/// @desc Follow player

x = objPlayer.x;
y = objPlayer.y;

var _width = camera_get_view_width(view_camera[0]);
var _height = camera_get_view_height(view_camera[0]);
var _displayX = x - (_width / 2);
var _displayY = y - (_height / 2);
_displayX = clamp(_displayX, 0, room_width - _width);
_displayY = clamp(_displayY, 0, room_height - _height);
camera_set_view_pos(view_camera[0], _displayX, _displayY);