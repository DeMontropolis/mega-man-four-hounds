/// @desc Set up variables

// State Machine
enum PlayerState
{
	Idle, 
	Moving,
	InAir,
	TippyToe,
	Sliding,
	ShootingIdle,
	ShootingMoving,
	ShootingInAir,
	Count,
	
}
state = PlayerState.Idle;

// Physics
xfrac = 0;
yfrac = 0;
tipToeTimer = 0;
tipToeTimerMax = 7;
slideTimer = 0;
slideTimerMax = 20;
xspeed = 0.0; // current x speed
yspeed = 0.0; // current y speed
accel = 0.1; // how fast we go from standing to top spd
spd = 1.596875; // top movement speed
slideSpeed = 3; // how fast mega man slides
jumpPower = 4.9; // how high megaman can jump
gravAccel = 0.24; // how quickly we fall
grav = 5; // the max speed we can fall
mask_index = mskMegaman;

// Personal properties
hp = 28;


// Combat
busterSpeedNoCharge = 4;
busterSpeedHalfCharge = 6;
busterSpeedFullCharge = 8;
busterSpriteNoCharge = sprBusterShot;
busterSpriteHalfCharge = sprBusterShotHalfCharged;
busterSpriteFullCharge = sprBusterShotCharged;
busterDamageNoCharge = 1;
busterDamageHalfCharge = 2;
busterDamageFullCharge = 3;
shotCooldown = 0;
shotCooldownMax = 20;
shotCharging = false;
shotCharge = 0;
shotChargeStart = 50;
shotChargeMax = 87;
shootResetTimer = 0;
shootResetTimerMax = 30;

