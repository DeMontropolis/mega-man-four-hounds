/// @desc Draw

// Draw the player
draw_sprite_ext(sprite_index, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, image_blend, image_alpha);

#region Set the correct whitemasks 

var _primary, _secondary, _outline;

switch (sprite_index)
{
  case sprMegamanStand:
    _primary =sprMegamanStandPrimary;
    _secondary = sprMegamanStandSecondary;
    _outline = sprMegamanStandOutline;
		break;
    
  case sprMegamanTippyToe:
    _primary =sprMegamanStepPrimary;
    _secondary = sprMegamanStepSecondary;
    _outline = sprMegamanStepOutline;
		break;
    
  case sprMegamanWalk:
    _primary =sprMegamanWalkPrimary;
    _secondary = sprMegamanWalkSecondary;
    _outline = sprMegamanWalkOutline;
		break;
    
  case sprMegamanJump:
    _primary =sprMegamanJumpPrimary;
    _secondary = sprMegamanJumpSecondary;
    _outline = sprMegamanJumpOutline;
		break;
    
  case sprMegamanStandShoot:
    _primary =sprMegamanStandShootPrimary;
    _secondary = sprMegamanStandShootSecondary;
    _outline = sprMegamanStandShootOutline;
		break;
    
  case sprMegamanWalkShoot:
    _primary =sprMegamanWalkShootPrimary;
    _secondary = sprMegamanWalkShootSecondary;
    _outline = sprMegamanWalkShootOutline;
		break;
    
  case sprMegamanJumpShoot:
    _primary =sprMegamanJumpShootPrimary;
    _secondary = sprMegamanJumpShootSecondary;
    _outline = sprMegamanJumpShootOutline;
		break;
    
  case sprMegamanStandThrow:
    _primary =sprMegamanStandThrowPrimary;
    _secondary = sprMegamanStandThrowSecondary;
    _outline = sprMegamanStandThrowOutline;
		break;
    
  case sprMegamanWalkThrow:
	  _primary =sprMegamanWalkThrowPrimary;
	  _secondary = sprMegamanWalkThrowSecondary;
	  _outline = sprMegamanWalkThrowOutline;
		break;
    
  case sprMegamanJumpThrow:
    _primary =sprMegamanJumpThrowPrimary;
    _secondary = sprMegamanJumpThrowSecondary;
    _outline = sprMegamanJumpThrowOutline;
		break;
    
  case sprMegamanSlide:
    _primary =sprMegamanSlidePrimary;
    _secondary = sprMegamanSlideSecondary;
    _outline = sprMegamanSlideOutline;
		break;
    
  case sprMegamanClimb:
    _primary =sprMegamanClimbPrimary;
    _secondary = sprMegamanClimbSecondary;
    _outline = sprMegamanClimbOutline;
		break;
    
  case sprMegamanClimbShoot:
    _primary =sprMegamanClimbShootPrimary;
    _secondary = sprMegamanClimbShootSecondary;
    _outline = sprMegamanClimbShootOutline;
	 break;
    
  case sprMegamanClimbThrow:
    _primary =sprMegamanClimbThrowPrimary;
    _secondary = sprMegamanClimbThrowSecondary;
    _outline = sprMegamanClimbThrowOutline;
	 break;
    
  case sprMegamanClimbGetup:
    _primary =sprMegamanClimbGetupPrimary;
    _secondary = sprMegamanClimbGetupSecondary;
    _outline = sprMegamanClimbGetupOutline;
		break;
    
  case sprMegamanHit:
    _primary =sprMegamanHitPrimary;
    _secondary = sprMegamanHitSecondary;
    _outline = sprMegamanHitOutline;
		break;
    
  case sprMegamanTeleport:
    _primary =sprMegamanTeleportPrimary;
    _secondary = sprMegamanTeleportSecondary;
    _outline = sprMegamanTeleportOutline;
		break;
}

#endregion

#region Draw the effects

if (shotCharging && shotCharge > shotChargeStart) {
	var _normalizedCharge = shotCharge - shotChargeStart;
	var _primaryColor = c_white;
	var _primaryAlpha = 0;
	var _secondaryColor = c_white;
	var _secondaryAlpha = 0;
	var _outlineColor = c_white;
	var _outlineAlpha = 0;
	if (shotCharge < shotChargeMax)
	{
			_outlineAlpha = image_alpha;
			var _oneThirdFirstStage = round(_normalizedCharge / 3);
      if (_normalizedCharge < _oneThirdFirstStage)
			{
          _outlineColor = make_color_rgb(168, 0, 32);     //Dark red
			}
      else if (_normalizedCharge < _oneThirdFirstStage * 2)
			{
          _outlineColor = make_color_rgb(228, 0, 88);     //Red (dark pink)
			}
      else
			{
          _outlineColor = make_color_rgb(248, 88, 152);   //Light red (pink)
			}
      if (_normalizedCharge mod 4 == 0 || _normalizedCharge mod 4 == 1)
			{
          _outlineColor = _outlineColor;
			}
      else
			{
          _outlineColor = c_black;
			}
	}
	else
	{
		_primaryAlpha = image_alpha;
		_secondaryAlpha = image_alpha;
		_outlineAlpha = image_alpha;
		switch (round((shotCharge - shootResetTimerMax) / 2 mod 3))
		{
		    case 0: //Light blue helmet, black shirt, blue outline
		      _primaryColor = make_color_rgb(0, 232, 216);
		      _secondaryColor = c_black;
		      _outlineColor = make_color_rgb(0, 120, 248);
					break;
                        
				case 1: //Black helmet, blue shirt, light blue outline
		      _primaryColor = c_black;
		      _secondaryColor = make_color_rgb(0, 120, 248);
		      _outlineColor = make_color_rgb(0, 232, 216);
					break;
                        
				case 2: //Blue helmet, light blue shirt, blue outline
		      _primaryColor = make_color_rgb(0, 120, 248);
		      _secondaryColor = make_color_rgb(0, 232, 216);
		      _outlineColor = c_black;
					break;
		}
	}
	
	// Draw these effects
	draw_sprite_ext(_primary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, _primaryColor, _primaryAlpha);
	draw_sprite_ext(_secondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, _secondaryColor, _secondaryAlpha);
	draw_sprite_ext(_outline, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, _outlineColor, _outlineAlpha);
}

#endregion