/// @desc Movement

// Inputs
var _leftDown = keyboard_check(global.leftKey);
var _rightDown = keyboard_check(global.rightKey);
var _upDown = keyboard_check(global.upKey);
var _downDown = keyboard_check(global.downKey);
var _jumpDown = keyboard_check(global.jumpKey);
var _attackPressed = keyboard_check_pressed(global.attackKey);
var _attackDown = keyboard_check(global.attackKey);
var _attackReleased = keyboard_check_released(global.attackKey);
var _slideDown = keyboard_check(global.slideKey);
var _RushSummon = keyboard_check(global.rushKey);

#region State Machine

switch (state)
{
	#region Idle
	case PlayerState.Idle:
	
		// Scripts 
		scrPlayerApplyGravity(_jumpDown);
		var _break = scrPlayerProcessSlide();
		if (_break) break;
		scrPlayerProcessJump();
		scrPlayerCheckInAir();
		scrPlayerBusterStart(_attackPressed);
		scrPlayerBusterCharging(_attackReleased);
		//scrPlayerRushSummon(_RushSummon);
	
		// Switch to moving
		if (_leftDown || _rightDown)
		{
			state = PlayerState.TippyToe;
			tipToeTimer = 0;
		}
		
		// Physics
		xspeed = 0;
		
		// Animation
		mask_index = mskMegaman;
		sprite_index = sprMegamanStand;
		image_index = 0;
		image_speed = 0;
		break;
	#endregion
	
	#region TippyToe
	case PlayerState.TippyToe:
	
		// Scripts 
		scrPlayerApplyGravity(_jumpDown);
		scrPlayerProcessJump();
		var _break = scrPlayerProcessSlide();
		if (_break) break;
		scrPlayerCheckInAir();
		scrPlayerBusterStart(_attackPressed);
		scrPlayerBusterCharging(_attackReleased);
		
		mask_index = mskMegaman;
		xspeed = 0;
		if (_leftDown || _rightDown)
		{
			if (_leftDown) xspeed -= 0.3;
			else xspeed += 0.3;
			sprite_index = sprMegamanTippyToe;
			image_xscale = sign(xspeed);
			if (tipToeTimer++ >= tipToeTimerMax)
			{
				tipToeTimer = 0;
				state = PlayerState.Moving;
				break;
			}
		} else {
			state = PlayerState.Idle;
		}
		break;
	#endregion
	
	#region Moving
	case PlayerState.Moving:
	
		// Scripts 
		scrPlayerApplyGravity(_jumpDown);
		var _break = scrPlayerProcessSlide();
		if (_break) break;
		scrPlayerProcessJump();
		scrPlayerCheckInAir();
		scrPlayerProcessMovement(_leftDown, _rightDown);
		scrPlayerBusterStart(_attackPressed);
		scrPlayerBusterCharging(_attackReleased);
		
		// Switch to idle
		if not (_leftDown || _rightDown)
		{
			state = PlayerState.Idle;
			readyToWalk = 0;
			break;
		}
		
		draw_text(x, y + 30, xspeed);
		

		// Process inputs
		if (_leftDown) xspeed = -1.4;
		else if (_rightDown) xspeed = 1.4;
		

		// Animations
		mask_index = mskMegaman;
		sprite_index = sprMegamanWalk;	
		image_xscale = sign(xspeed);
		image_speed = 1;
		break;
	#endregion
	
	#region InAir
	case PlayerState.InAir:
	
		// Scripts 
		scrPlayerApplyGravity(_jumpDown);
		scrPlayerProcessMovement(_leftDown, _rightDown);
		scrPlayerBusterStart(_attackPressed);
		scrPlayerCheckLanding(_leftDown, _rightDown);
		scrPlayerBusterCharging(_attackReleased);
		
		// Animation
		mask_index = mskMegaman;
		sprite_index = sprMegamanJump;
		if (xspeed != 0) image_xscale = sign(xspeed);
		break;
	#endregion
	
	#region Sliding
	case PlayerState.Sliding:
	
		// Scripts 
		mask_index = mskMegamanSlide;
		scrPlayerApplyGravity(_jumpDown);
		scrPlayerProcessJump();
		scrPlayerCheckInAir();
		
		// Leaving the slide
		++slideTimer;
		if (not tileCollision(x, y - 5)) {
			if (slideTimer >= slideTimerMax)
			{
				if (_leftDown || _rightDown)
				{
					state = PlayerState.Moving;
				}
				else
				{
					state = PlayerState.Idle;
				}
				slideTimer = 0;
				break;
			}
			else
			{
				if (sign(xspeed) == 1)
				{
					if (_leftDown)
					{
						state = PlayerState.Moving;
						slideTimer = 0;
						break;
					}
				}
				else if (sign(xspeed) == -1)
				{
					if (_rightDown) 
					{
						state = PlayerState.Moving;
						slideTimer = 0;
						break;
					}
				}
			}
		}
		
		// Animation
		sprite_index = sprMegamanSlide;
		
		// Physics
		xspeed = slideSpeed * image_xscale;
		break;
	#endregion
	
	#region ShootingIdle
	case PlayerState.ShootingIdle:
	
		// Scripts 
		scrPlayerApplyGravity(_jumpDown);
		scrPlayerProcessJump();
		var _break = scrPlayerProcessSlide();
		if (_break) break;
		scrPlayerBusterStart(_attackPressed);
		scrPlayerCheckInAir();
		scrPlayerBusterProcess(_leftDown, _rightDown);
		scrPlayerBusterCharging(_attackReleased);
		
		// Animation
		mask_index = mskMegaman;
		sprite_index = sprMegamanStandShoot;
		
		break;
	#endregion
	
	#region ShootingMoving
	case PlayerState.ShootingMoving:
	
		// Scripts 
		scrPlayerApplyGravity(_jumpDown);
		scrPlayerProcessJump();
		var _break = scrPlayerProcessSlide();
		if (_break) break;
		scrPlayerCheckInAir();
		scrPlayerBusterStart(_attackPressed);
		scrPlayerBusterProcess(_leftDown, _rightDown, _attackDown, _attackReleased);
		scrPlayerBusterCharging(_attackReleased);
		scrPlayerProcessMovement(_leftDown, _rightDown);
		
		// Animation
		mask_index = mskMegaman;
		sprite_index = sprMegamanWalkShoot;
		image_speed = 1;
		if (xspeed != 0) image_xscale = sign(xspeed);
		break;
	#endregion
	
	#region ShootingInAir
	case PlayerState.ShootingInAir:
	
		// Scripts 
		scrPlayerApplyGravity(_jumpDown);
		scrPlayerBusterProcess(_leftDown, _rightDown, _attackDown, _attackReleased);
		scrPlayerProcessMovement(_leftDown, _rightDown);
		scrPlayerBusterStart(_attackPressed);
		scrPlayerCheckLanding(_leftDown, _rightDown);
		scrPlayerBusterCharging(_attackReleased);
		
		// Animation
		mask_index = mskMegaman;
		sprite_index = sprMegamanJumpShoot;
		if (xspeed != 0) image_xscale = sign(xspeed);
		break;
	#endregion
}

#endregion

var _xTarget = x + xspeed;
var _yTarget = y + yspeed;

// take the fraction of my current xspeed.
// this is continued to be stored in a varaible and eventually will become
// a whole number 
xfrac += frac(xspeed);
// remove the fraction entirely from xspeed. We're only ever going to move 
// by one pixel. 
xspeed -= frac(xspeed);
// Assume _xfrac is something like 3.4. 
//WHILE _xfrac is atleast 1 or -1, continue to remove whole numbers from it
// at the end of this while loop speed would have increased by 3 and xfrac would end up as 0.4
// effectively applying all whole numbers from storage, keeping the remainder for the next go round 
while(abs(xfrac) >= 1)
{
    // remove unit from xfrac
    xfrac -= sign(xfrac);
    // add unit to speed 
    xspeed += sign(xfrac);
}

// speed is now a whole number. If it is not we've done something wrong.
if(frac(xspeed) != 0){
    show_error("you goofed", true);
}

//xspeed is now ready to be applied like normal. 

// Horizontal collisions
for (var i = 0; i < abs(xspeed); i++) 
{
	if not (tileCollision(x + sign(xspeed), y))
	{
		x += sign(xspeed);	
	}
	else
	{
		xspeed = 0;
		break;
	}
}

// Vertical collisions
for (var i = 0; i < abs(yspeed); i++) 
{
	if not (tileCollision(x , y + sign(yspeed)))
	{
		y += sign(yspeed);	
	}
	else
	{
		yspeed = 0;
		break;
	}
}