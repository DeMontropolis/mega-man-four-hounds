{
    "id": "d72685df-7078-4466-a526-5771cedb3c70",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objRush",
    "eventList": [
        {
            "id": "d0711ea8-582e-4df6-816a-96918d7df7d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d72685df-7078-4466-a526-5771cedb3c70"
        },
        {
            "id": "4a864afd-cfbe-4327-989d-cdd8aa5efaa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d72685df-7078-4466-a526-5771cedb3c70"
        },
        {
            "id": "be8b74e5-7203-4156-8288-2ff01d4136de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d72685df-7078-4466-a526-5771cedb3c70"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8419026f-97f9-4750-aa3a-9216f7bb5ab2",
    "visible": true
}