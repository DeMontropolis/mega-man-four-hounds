/// @desc 

// Draw Health
var _healthX = 20;
var _healthY = 17;
draw_sprite(sprHealthbarBackground, 0, _healthX, _healthY);
for (var i = 1; i <= objPlayer.hp; i++)
{
    draw_sprite_ext(sprHealthbarPrimary, 0, 21, 17 + (sprite_get_height(sprHealthbarBackground) - i * 2), 1, 1, 0, make_color_rgb(252, 228, 160), 1);
    draw_sprite_ext(sprHealthbarSecondary, 0, 21, 17 + (sprite_get_height(sprHealthbarBackground) - i * 2), 1, 1, 0, make_color_rgb(248, 248, 248), 1);
}

