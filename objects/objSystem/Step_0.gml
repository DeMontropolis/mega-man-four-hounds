/// @desc Restart

if (DEVELOPMENT && keyboard_check(vk_control) && keyboard_check_pressed(ord("R"))) {
	game_restart();
}
