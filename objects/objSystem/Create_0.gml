/// @desc Game Setup

#macro DEVELOPMENT false
#macro development:DEVELOPMENT true

// Inputs
global.leftKey = vk_left;
global.rightKey = vk_right;
global.upKey = vk_up;
global.downKey = vk_down;
global.jumpKey = ord("Z");
global.attackKey = ord("X");
global.slideKey = ord("C");
global.rushKey = ord("V");

// Create other objects
instance_create_depth(0, 0, 0, objUI);
instance_create_depth(0, 0, 0, objCamera);