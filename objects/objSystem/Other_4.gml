/// @desc Get our tile layer IDs

var _layerId = layer_get_id("Colliders");
global.collisionTiles = layer_tilemap_get_id(_layerId);