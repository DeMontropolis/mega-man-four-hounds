/// @desc Move

xspeed = lengthdir_x(spd, dir);
yspeed = lengthdir_y(spd, dir);

// Horizontal collisions
for (var i = 0; i < abs(xspeed); i++) 
{
	var _enemy = instance_place(x + sign(xspeed), y, parEnemy);
	if (_enemy == noone)
	{
		x += sign(xspeed);	
	}
	else
	{
		// Damage the enemy
		with (_enemy) {
			event_user(0);
		}
		break;
	}
}

// Vertical collisions
for (var i = 0; i < abs(yspeed); i++) 
{
	var _enemy = instance_place(x, y + sign(yspeed), parEnemy);
	if (_enemy == noone)
	{
		y += sign(yspeed);	
	}
	else
	{
		// Damage the enemy
		with (_enemy) {
			event_user(0);
		}
		break;
	}
}