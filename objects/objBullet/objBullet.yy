{
    "id": "79f6c60a-a058-4c2c-8d34-5f3d00e441f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBullet",
    "eventList": [
        {
            "id": "5e2f81d5-bc56-40ae-bb99-f8a68c0e710a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "79f6c60a-a058-4c2c-8d34-5f3d00e441f5"
        },
        {
            "id": "31758086-981a-4440-aae9-9db02c54526c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "79f6c60a-a058-4c2c-8d34-5f3d00e441f5"
        },
        {
            "id": "17149315-abea-4242-be47-6f547b34e81e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "79f6c60a-a058-4c2c-8d34-5f3d00e441f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}