{
    "id": "08980303-8247-4f0d-83a3-4ee360e94ead",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkThrowSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 21,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54b19a61-656d-49f3-9906-fb7d89d8cbdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08980303-8247-4f0d-83a3-4ee360e94ead",
            "compositeImage": {
                "id": "38732e9a-323b-4e0d-9cd8-4135ac458956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b19a61-656d-49f3-9906-fb7d89d8cbdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c705b91-d395-4107-8856-ba0c38bd447f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b19a61-656d-49f3-9906-fb7d89d8cbdc",
                    "LayerId": "a131cb0e-5d31-42ba-8119-2192b6e32ed4"
                }
            ]
        },
        {
            "id": "5ee7bd9a-76cb-472b-b361-59393ef49505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08980303-8247-4f0d-83a3-4ee360e94ead",
            "compositeImage": {
                "id": "1e1c3c31-1d6f-403c-976d-24a4b4d34bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee7bd9a-76cb-472b-b361-59393ef49505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4c443d7-a776-4250-838d-2fd2d2fc393c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee7bd9a-76cb-472b-b361-59393ef49505",
                    "LayerId": "a131cb0e-5d31-42ba-8119-2192b6e32ed4"
                }
            ]
        },
        {
            "id": "d079a646-d003-43b5-a77a-62ca2f4472f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08980303-8247-4f0d-83a3-4ee360e94ead",
            "compositeImage": {
                "id": "bd8e8bf3-a0a4-40e9-bec4-e29fa2f1f3fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d079a646-d003-43b5-a77a-62ca2f4472f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e37254f5-2a8b-4a38-9eee-20e0016d5289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d079a646-d003-43b5-a77a-62ca2f4472f2",
                    "LayerId": "a131cb0e-5d31-42ba-8119-2192b6e32ed4"
                }
            ]
        },
        {
            "id": "266ac43d-c3b2-4772-a8a4-cb6c4adcbec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08980303-8247-4f0d-83a3-4ee360e94ead",
            "compositeImage": {
                "id": "6ba2d470-3662-4fa2-8c5a-eac8ceea1a17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "266ac43d-c3b2-4772-a8a4-cb6c4adcbec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80a3a0a2-88d3-42d5-8e96-6baa0c67ce7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "266ac43d-c3b2-4772-a8a4-cb6c4adcbec9",
                    "LayerId": "a131cb0e-5d31-42ba-8119-2192b6e32ed4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a131cb0e-5d31-42ba-8119-2192b6e32ed4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08980303-8247-4f0d-83a3-4ee360e94ead",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 11,
    "yorig": 7
}