{
    "id": "8914f8fd-771e-419e-87f1-8668dd5dfc31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanSlideOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a329bca3-9d86-4556-9f79-7f00b3ca4eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8914f8fd-771e-419e-87f1-8668dd5dfc31",
            "compositeImage": {
                "id": "1af38b81-bf3e-4bed-bf47-7263843036ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a329bca3-9d86-4556-9f79-7f00b3ca4eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1ecfcee-fca6-402e-b57e-926ecff98f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a329bca3-9d86-4556-9f79-7f00b3ca4eeb",
                    "LayerId": "18bc6891-5d25-4304-9a27-b868e0d33148"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "18bc6891-5d25-4304-9a27-b868e0d33148",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8914f8fd-771e-419e-87f1-8668dd5dfc31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 11,
    "yorig": 2
}