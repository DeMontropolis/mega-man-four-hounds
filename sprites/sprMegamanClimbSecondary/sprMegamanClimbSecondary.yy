{
    "id": "6f27b62e-83e5-4af0-a9ca-eac3063be079",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acab065e-3385-47e4-8360-a1a5b28aa644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f27b62e-83e5-4af0-a9ca-eac3063be079",
            "compositeImage": {
                "id": "5539b808-8f71-44ba-9c8f-1dff1c286538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acab065e-3385-47e4-8360-a1a5b28aa644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72f2b6c3-f139-476c-bdbf-0572224a8a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acab065e-3385-47e4-8360-a1a5b28aa644",
                    "LayerId": "3e2391bf-f2c5-4183-859b-3a6101018b6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "3e2391bf-f2c5-4183-859b-3a6101018b6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f27b62e-83e5-4af0-a9ca-eac3063be079",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 7
}