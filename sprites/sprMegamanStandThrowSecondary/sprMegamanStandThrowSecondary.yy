{
    "id": "6349dd6c-15d9-4873-ba0e-fb78716f6229",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandThrowSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62a001b8-6be3-4ffd-8c48-c5ab81072326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6349dd6c-15d9-4873-ba0e-fb78716f6229",
            "compositeImage": {
                "id": "3f7e7c23-f85c-4f45-87f1-9b3f8c1b75be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a001b8-6be3-4ffd-8c48-c5ab81072326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb1a216-23e9-44d8-8dea-57513f49f843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a001b8-6be3-4ffd-8c48-c5ab81072326",
                    "LayerId": "4df7ba60-e690-4e41-b734-067cf41a40d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4df7ba60-e690-4e41-b734-067cf41a40d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6349dd6c-15d9-4873-ba0e-fb78716f6229",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 10,
    "yorig": 7
}