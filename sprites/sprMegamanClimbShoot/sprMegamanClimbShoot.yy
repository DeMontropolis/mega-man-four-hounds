{
    "id": "56f28b48-8814-44b1-aa6e-517a1a7946bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f7c7195-2f9e-4ede-b2b0-1b2f643a9a1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f28b48-8814-44b1-aa6e-517a1a7946bf",
            "compositeImage": {
                "id": "9a187bd0-0433-415c-9613-3b7ff96f34df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7c7195-2f9e-4ede-b2b0-1b2f643a9a1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b70073f0-693a-472c-a2b5-80ce077c080a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7c7195-2f9e-4ede-b2b0-1b2f643a9a1b",
                    "LayerId": "c3dd023f-70b4-4e29-ac27-2b7de60e83ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "c3dd023f-70b4-4e29-ac27-2b7de60e83ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56f28b48-8814-44b1-aa6e-517a1a7946bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 8,
    "yorig": 7
}