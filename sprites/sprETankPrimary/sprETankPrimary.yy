{
    "id": "bb695ac9-b6c8-4b84-a7cb-92ec0f4e3555",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprETankPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7dda322-110e-45f6-a340-a2d75d305f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb695ac9-b6c8-4b84-a7cb-92ec0f4e3555",
            "compositeImage": {
                "id": "a3f5c8fa-5f2e-4b71-8174-4d09ea1ab452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7dda322-110e-45f6-a340-a2d75d305f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b2b64d2-ca24-4859-9c04-d28acc4af494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7dda322-110e-45f6-a340-a2d75d305f75",
                    "LayerId": "912905fc-5878-4330-817b-c2495e210c3e"
                }
            ]
        },
        {
            "id": "afb5df3a-443a-4814-8db9-e9c683d44ff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb695ac9-b6c8-4b84-a7cb-92ec0f4e3555",
            "compositeImage": {
                "id": "583acf51-a1b7-4954-9b91-0ef46ad6d277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb5df3a-443a-4814-8db9-e9c683d44ff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a5c089-ebe7-485a-913a-47e2f8023822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb5df3a-443a-4814-8db9-e9c683d44ff0",
                    "LayerId": "912905fc-5878-4330-817b-c2495e210c3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "912905fc-5878-4330-817b-c2495e210c3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb695ac9-b6c8-4b84-a7cb-92ec0f4e3555",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}