{
    "id": "22af417e-d797-4166-8d25-8e6e773afdd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStageSelectIcons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb4b0d79-6c84-4f8a-9e3a-eafbeda9afc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "compositeImage": {
                "id": "c04c8e0a-e0bd-4fd8-8ef0-bbcc592680f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb4b0d79-6c84-4f8a-9e3a-eafbeda9afc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7bffdcc-2d21-4736-a506-06637821733c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb4b0d79-6c84-4f8a-9e3a-eafbeda9afc9",
                    "LayerId": "624eb712-3ac4-4c89-8ed7-c711dd0408e6"
                }
            ]
        },
        {
            "id": "d49f7bfb-ab36-4b12-ba02-97f40e17ed8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "compositeImage": {
                "id": "5a330c5e-ad3f-4697-9d9d-aca081191508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49f7bfb-ab36-4b12-ba02-97f40e17ed8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece4276f-8b5f-4331-a6ad-9282b88997d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49f7bfb-ab36-4b12-ba02-97f40e17ed8e",
                    "LayerId": "624eb712-3ac4-4c89-8ed7-c711dd0408e6"
                }
            ]
        },
        {
            "id": "2180cdc0-9f93-4e09-893e-5de27f515f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "compositeImage": {
                "id": "4f67fd56-611a-4e43-98be-55a976621264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2180cdc0-9f93-4e09-893e-5de27f515f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0962f1f0-776a-4a1e-bdd2-f8a7b462986e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2180cdc0-9f93-4e09-893e-5de27f515f66",
                    "LayerId": "624eb712-3ac4-4c89-8ed7-c711dd0408e6"
                }
            ]
        },
        {
            "id": "dbd5cd7b-b01a-44e0-b568-7cca68dbc8c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "compositeImage": {
                "id": "ea480deb-3475-41b3-8d5f-cb321c93116f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbd5cd7b-b01a-44e0-b568-7cca68dbc8c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dfaf1d4-6309-4ca5-a336-25cdbaa6436f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbd5cd7b-b01a-44e0-b568-7cca68dbc8c6",
                    "LayerId": "624eb712-3ac4-4c89-8ed7-c711dd0408e6"
                }
            ]
        },
        {
            "id": "8a49b2d1-94a7-4b2b-a10a-9b28ba80329e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "compositeImage": {
                "id": "ee5bd507-7714-42ae-8ac6-580fc09b70d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a49b2d1-94a7-4b2b-a10a-9b28ba80329e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71982488-b99f-4d1b-bd39-3024b1b19f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a49b2d1-94a7-4b2b-a10a-9b28ba80329e",
                    "LayerId": "624eb712-3ac4-4c89-8ed7-c711dd0408e6"
                }
            ]
        },
        {
            "id": "06362c45-5136-4ae7-b464-204221d10833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "compositeImage": {
                "id": "5277483f-b9ce-4330-b0bf-75c85809f6f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06362c45-5136-4ae7-b464-204221d10833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad4bf7ca-fa8b-4108-b4e9-906f5a61871a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06362c45-5136-4ae7-b464-204221d10833",
                    "LayerId": "624eb712-3ac4-4c89-8ed7-c711dd0408e6"
                }
            ]
        },
        {
            "id": "7fbbc5ec-9bf9-46b2-9008-14ab5e4954b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "compositeImage": {
                "id": "c57dde9b-3e9d-4a85-982d-dffa113cd756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fbbc5ec-9bf9-46b2-9008-14ab5e4954b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3818420d-1c9c-409e-b617-ed6be063894f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fbbc5ec-9bf9-46b2-9008-14ab5e4954b5",
                    "LayerId": "624eb712-3ac4-4c89-8ed7-c711dd0408e6"
                }
            ]
        },
        {
            "id": "41a83b79-4a09-44ea-b957-e6c8de9c932d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "compositeImage": {
                "id": "d87d85d2-b1c3-4961-af0e-11c700f12b02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a83b79-4a09-44ea-b957-e6c8de9c932d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a4f872f-58ac-4ac5-9b78-df3ec4ef8aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a83b79-4a09-44ea-b957-e6c8de9c932d",
                    "LayerId": "624eb712-3ac4-4c89-8ed7-c711dd0408e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "624eb712-3ac4-4c89-8ed7-c711dd0408e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22af417e-d797-4166-8d25-8e6e773afdd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}