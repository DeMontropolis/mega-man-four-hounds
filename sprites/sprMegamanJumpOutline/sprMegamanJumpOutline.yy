{
    "id": "6f8dcfd0-0523-490d-9b49-64f177959663",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f260fe5d-a509-4a20-b8ca-1b1045a09da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f8dcfd0-0523-490d-9b49-64f177959663",
            "compositeImage": {
                "id": "131da6d4-175f-4c16-bcee-6a8b7ce6fa2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f260fe5d-a509-4a20-b8ca-1b1045a09da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "370a60af-c1a5-4ef4-b9f5-a6ec970cc0da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f260fe5d-a509-4a20-b8ca-1b1045a09da7",
                    "LayerId": "23dd31e3-e18b-49e7-9619-1e67374b8618"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "23dd31e3-e18b-49e7-9619-1e67374b8618",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f8dcfd0-0523-490d-9b49-64f177959663",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 12,
    "yorig": 7
}