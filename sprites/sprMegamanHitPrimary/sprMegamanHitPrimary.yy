{
    "id": "9e318705-4e8a-4f28-8964-04868f1a06fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanHitPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 1,
    "bbox_right": 24,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bacc67af-f366-4595-b9e6-5cdfbb34f5a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e318705-4e8a-4f28-8964-04868f1a06fc",
            "compositeImage": {
                "id": "a06a9ebb-fafa-477e-b550-895a0b7d1b6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bacc67af-f366-4595-b9e6-5cdfbb34f5a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d19b4f72-51e9-40fd-916e-1f9832b5ac34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bacc67af-f366-4595-b9e6-5cdfbb34f5a5",
                    "LayerId": "fa238d35-7e5c-498d-976c-270c80ef5e78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "fa238d35-7e5c-498d-976c-270c80ef5e78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e318705-4e8a-4f28-8964-04868f1a06fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 7
}