{
    "id": "6d223753-df34-43bf-95c5-d76e4f17ebf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpThrowSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 6,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e1787b2-8476-4aed-aa3b-c503aec25e86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d223753-df34-43bf-95c5-d76e4f17ebf4",
            "compositeImage": {
                "id": "88877b58-6911-49b1-ac7b-801a5a107751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e1787b2-8476-4aed-aa3b-c503aec25e86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e3fdc47-671d-4753-9139-c84209cc7de2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e1787b2-8476-4aed-aa3b-c503aec25e86",
                    "LayerId": "822ae3dd-b75e-45c5-a5fd-5c39861e30f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "822ae3dd-b75e-45c5-a5fd-5c39861e30f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d223753-df34-43bf-95c5-d76e4f17ebf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 12,
    "yorig": 7
}