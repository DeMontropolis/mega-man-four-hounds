{
    "id": "4091bada-864e-4e1f-8430-16033680612c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkShootPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "635e89f0-d5d5-4c49-9a78-91ae9e496ae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4091bada-864e-4e1f-8430-16033680612c",
            "compositeImage": {
                "id": "4bea3b83-beb8-4c3f-a200-057e28a0432d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "635e89f0-d5d5-4c49-9a78-91ae9e496ae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e3f0f1-fcb5-4ee1-8724-01b63a8737be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "635e89f0-d5d5-4c49-9a78-91ae9e496ae5",
                    "LayerId": "384b03fe-ba9f-4f90-9cfa-b53e0bc17ae0"
                }
            ]
        },
        {
            "id": "09cebf75-9cae-474f-b4f2-8b4d52ba8604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4091bada-864e-4e1f-8430-16033680612c",
            "compositeImage": {
                "id": "4069ac09-e07f-4858-b992-07fd028841a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09cebf75-9cae-474f-b4f2-8b4d52ba8604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "997f11fa-fe16-4550-8bec-f9ec08236930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09cebf75-9cae-474f-b4f2-8b4d52ba8604",
                    "LayerId": "384b03fe-ba9f-4f90-9cfa-b53e0bc17ae0"
                }
            ]
        },
        {
            "id": "0dd74809-9416-49de-9ffa-fc235d18cca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4091bada-864e-4e1f-8430-16033680612c",
            "compositeImage": {
                "id": "4b400d55-bfd7-45de-8fd3-8c7c47c3c8e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd74809-9416-49de-9ffa-fc235d18cca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011e30b1-6d9f-4ace-806d-9a9631616280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd74809-9416-49de-9ffa-fc235d18cca9",
                    "LayerId": "384b03fe-ba9f-4f90-9cfa-b53e0bc17ae0"
                }
            ]
        },
        {
            "id": "1d06cd36-811b-4f28-906b-d908943021e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4091bada-864e-4e1f-8430-16033680612c",
            "compositeImage": {
                "id": "8950d44a-834e-47df-b82e-5c37ae728c4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d06cd36-811b-4f28-906b-d908943021e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee112630-8882-4ef0-b110-5074256abcbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d06cd36-811b-4f28-906b-d908943021e2",
                    "LayerId": "384b03fe-ba9f-4f90-9cfa-b53e0bc17ae0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "384b03fe-ba9f-4f90-9cfa-b53e0bc17ae0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4091bada-864e-4e1f-8430-16033680612c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 11,
    "yorig": 7
}