{
    "id": "cd1d81a8-38ee-4aec-911c-73809270b283",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanHitOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e0d00dc-47f5-4b46-a761-b972e464f179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd1d81a8-38ee-4aec-911c-73809270b283",
            "compositeImage": {
                "id": "fbf714f6-1dd1-4f08-873b-2f1c981f0995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e0d00dc-47f5-4b46-a761-b972e464f179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77fcbe22-cd41-479e-9713-9111696274c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e0d00dc-47f5-4b46-a761-b972e464f179",
                    "LayerId": "0ffa4e50-0708-4fd0-af39-76332a46251e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "0ffa4e50-0708-4fd0-af39-76332a46251e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd1d81a8-38ee-4aec-911c-73809270b283",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 7
}