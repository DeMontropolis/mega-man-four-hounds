{
    "id": "eb9c2ec5-2006-430f-b10d-1253ac9b05e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRushJet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 4,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60b9fecd-c389-4409-93bd-335534f71a1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb9c2ec5-2006-430f-b10d-1253ac9b05e2",
            "compositeImage": {
                "id": "67ecd044-e498-434c-9531-d8b69fdd7a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b9fecd-c389-4409-93bd-335534f71a1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2588cb1-ef43-4052-9e96-f598a0f5a922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b9fecd-c389-4409-93bd-335534f71a1d",
                    "LayerId": "502e47d4-c96a-4971-8ab3-7d189b730019"
                }
            ]
        },
        {
            "id": "9a591bee-c2fa-4719-8fca-d2380fb6943f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb9c2ec5-2006-430f-b10d-1253ac9b05e2",
            "compositeImage": {
                "id": "04317abc-c99b-45b5-9d1b-59d37d82ab31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a591bee-c2fa-4719-8fca-d2380fb6943f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c83080-f500-4424-addc-2cc26f80da93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a591bee-c2fa-4719-8fca-d2380fb6943f",
                    "LayerId": "502e47d4-c96a-4971-8ab3-7d189b730019"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "502e47d4-c96a-4971-8ab3-7d189b730019",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb9c2ec5-2006-430f-b10d-1253ac9b05e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 6
}