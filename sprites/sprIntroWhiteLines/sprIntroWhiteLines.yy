{
    "id": "d1f96547-0457-48d0-85e7-426864063a1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroWhiteLines",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eff3e8bd-6e67-4975-a103-f7ee5e218d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f96547-0457-48d0-85e7-426864063a1d",
            "compositeImage": {
                "id": "a7c0d416-9b37-4787-ac5f-ff7fd28d7b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eff3e8bd-6e67-4975-a103-f7ee5e218d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8011b67e-40d3-4ac9-a018-13ed2ee0eb96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eff3e8bd-6e67-4975-a103-f7ee5e218d2e",
                    "LayerId": "b1e60e83-a025-45af-9b1b-4c0b2e50c787"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "b1e60e83-a025-45af-9b1b-4c0b2e50c787",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1f96547-0457-48d0-85e7-426864063a1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}