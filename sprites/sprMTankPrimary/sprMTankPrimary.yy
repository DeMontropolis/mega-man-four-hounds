{
    "id": "db1f2d8e-ddc2-4500-bdf5-849d223ceebd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMTankPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5944878-0530-4743-b21f-abf07b28e104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db1f2d8e-ddc2-4500-bdf5-849d223ceebd",
            "compositeImage": {
                "id": "1aaf5f53-b80f-461a-a1b1-ca602825e590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5944878-0530-4743-b21f-abf07b28e104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51f46ce5-ab58-4112-ba4a-0292b2a7a951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5944878-0530-4743-b21f-abf07b28e104",
                    "LayerId": "de0ccb3c-9edf-4bfa-b158-ea046694f548"
                }
            ]
        },
        {
            "id": "2455277c-8bc0-4068-9349-0f4e0f171685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db1f2d8e-ddc2-4500-bdf5-849d223ceebd",
            "compositeImage": {
                "id": "be88a7a0-2849-4e1c-a3d1-3c9737dd907e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2455277c-8bc0-4068-9349-0f4e0f171685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73724347-b844-4c98-953f-286068d25b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2455277c-8bc0-4068-9349-0f4e0f171685",
                    "LayerId": "de0ccb3c-9edf-4bfa-b158-ea046694f548"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "de0ccb3c-9edf-4bfa-b158-ea046694f548",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db1f2d8e-ddc2-4500-bdf5-849d223ceebd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}