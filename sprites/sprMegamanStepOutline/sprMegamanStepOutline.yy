{
    "id": "e5eb8e38-bcec-4666-a2e4-50754f223e7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStepOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abf9505d-edc0-4b78-b2e3-49793576ba4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5eb8e38-bcec-4666-a2e4-50754f223e7c",
            "compositeImage": {
                "id": "25b573f2-3bc4-4e70-9d4c-e2c4b5a20817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf9505d-edc0-4b78-b2e3-49793576ba4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca307a7c-4769-4b74-9acd-3dd0d024c548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf9505d-edc0-4b78-b2e3-49793576ba4c",
                    "LayerId": "3d4e1b5e-0a96-43a0-bc24-e111cacbefe5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3d4e1b5e-0a96-43a0-bc24-e111cacbefe5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5eb8e38-bcec-4666-a2e4-50754f223e7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 7
}