{
    "id": "ad8f38f3-ab0f-4813-9115-5c5ff71f3fcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionArrowRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a76b961c-506f-43af-90b3-d1e15ea3f4be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8f38f3-ab0f-4813-9115-5c5ff71f3fcb",
            "compositeImage": {
                "id": "5878a29f-5ca0-46e7-94db-bb7347f7098f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a76b961c-506f-43af-90b3-d1e15ea3f4be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a8849ab-03ed-4938-99b7-14763d1ad161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a76b961c-506f-43af-90b3-d1e15ea3f4be",
                    "LayerId": "13bc66f2-edee-4e28-ad6f-141a3488c95a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "13bc66f2-edee-4e28-ad6f-141a3488c95a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad8f38f3-ab0f-4813-9115-5c5ff71f3fcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}