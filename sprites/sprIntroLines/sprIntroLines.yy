{
    "id": "9003991f-5f80-4305-acc7-80b6f212e676",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroLines",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "312ba4e4-8dcb-4ffe-b5c0-b980c60a4afe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9003991f-5f80-4305-acc7-80b6f212e676",
            "compositeImage": {
                "id": "ab52597a-ff7e-44b1-81df-2fdbdbc0ca2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "312ba4e4-8dcb-4ffe-b5c0-b980c60a4afe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97c5d63a-8dbb-4cd4-bc4f-1599c9eda49b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "312ba4e4-8dcb-4ffe-b5c0-b980c60a4afe",
                    "LayerId": "e91fd3a1-03a7-4292-a1c3-c803923f10e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "e91fd3a1-03a7-4292-a1c3-c803923f10e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9003991f-5f80-4305-acc7-80b6f212e676",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}