{
    "id": "f367b9c6-20ad-41e0-8bc7-6441d6dda66b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "IntroStageTiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 305,
    "bbox_left": 0,
    "bbox_right": 815,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5852bdef-a29b-4713-9253-0cec27d864cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f367b9c6-20ad-41e0-8bc7-6441d6dda66b",
            "compositeImage": {
                "id": "3a2008ae-6605-445a-ae8a-595b11f991ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5852bdef-a29b-4713-9253-0cec27d864cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94b54ed3-4821-41de-a188-eef70e9c85c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5852bdef-a29b-4713-9253-0cec27d864cf",
                    "LayerId": "5acecaef-94ad-4df3-9b91-91e7721f8045"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 306,
    "layers": [
        {
            "id": "5acecaef-94ad-4df3-9b91-91e7721f8045",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f367b9c6-20ad-41e0-8bc7-6441d6dda66b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 816,
    "xorig": 0,
    "yorig": 0
}