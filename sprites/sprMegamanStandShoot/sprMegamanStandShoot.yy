{
    "id": "b56d62a2-4e92-4056-925c-202ef098db09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46e7b4c2-a809-4fb6-9650-13d6a2d7f4b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b56d62a2-4e92-4056-925c-202ef098db09",
            "compositeImage": {
                "id": "c50da272-2f80-4cbd-9c03-343ff37f53e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46e7b4c2-a809-4fb6-9650-13d6a2d7f4b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "799b5b1f-d4ec-4102-82e8-467accbaf59d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46e7b4c2-a809-4fb6-9650-13d6a2d7f4b9",
                    "LayerId": "5e39c692-1038-4bad-a732-c6e56dfd5860"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "5e39c692-1038-4bad-a732-c6e56dfd5860",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b56d62a2-4e92-4056-925c-202ef098db09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 10,
    "yorig": 7
}