{
    "id": "8a713e6f-e969-49d0-952f-90deec50bde9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGambettal5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82bdd3f0-3c1c-4eaa-b304-9f8976dcd260",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a713e6f-e969-49d0-952f-90deec50bde9",
            "compositeImage": {
                "id": "690c2db9-a861-4edc-bece-1f7ed59edfcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82bdd3f0-3c1c-4eaa-b304-9f8976dcd260",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05765614-87df-4293-a681-98cdd17f5096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82bdd3f0-3c1c-4eaa-b304-9f8976dcd260",
                    "LayerId": "ce2f15fd-ff82-4555-bb23-6b38d0cf007d"
                }
            ]
        },
        {
            "id": "06d60c82-b070-4e02-a5ee-1b1210e68dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a713e6f-e969-49d0-952f-90deec50bde9",
            "compositeImage": {
                "id": "c48e7f51-0462-486b-8d5d-7958761a1004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d60c82-b070-4e02-a5ee-1b1210e68dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9662b070-60b2-4a65-8a25-95c517a5f9b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d60c82-b070-4e02-a5ee-1b1210e68dac",
                    "LayerId": "ce2f15fd-ff82-4555-bb23-6b38d0cf007d"
                }
            ]
        },
        {
            "id": "81843012-5978-4792-82c6-585562e359aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a713e6f-e969-49d0-952f-90deec50bde9",
            "compositeImage": {
                "id": "8f5074c5-e04c-4836-8ced-a1732d4236f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81843012-5978-4792-82c6-585562e359aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4704d99-4e55-4fc2-8025-991d3d2d7c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81843012-5978-4792-82c6-585562e359aa",
                    "LayerId": "ce2f15fd-ff82-4555-bb23-6b38d0cf007d"
                }
            ]
        },
        {
            "id": "bce6ddd3-cec3-416e-baa1-fc9655beba59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a713e6f-e969-49d0-952f-90deec50bde9",
            "compositeImage": {
                "id": "015fd17d-7e22-4dfd-b034-c7faca3a6c8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bce6ddd3-cec3-416e-baa1-fc9655beba59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b93419-da3c-4d0f-a4ea-7c903e792715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bce6ddd3-cec3-416e-baa1-fc9655beba59",
                    "LayerId": "ce2f15fd-ff82-4555-bb23-6b38d0cf007d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "ce2f15fd-ff82-4555-bb23-6b38d0cf007d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a713e6f-e969-49d0-952f-90deec50bde9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 8,
    "yorig": 11
}