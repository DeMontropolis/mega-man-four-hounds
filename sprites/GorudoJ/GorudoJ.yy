{
    "id": "dea4b042-efe7-4225-887c-d1ce5850a9d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "GorudoJ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ab7261b-6a21-4c7b-9794-faebc978622b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea4b042-efe7-4225-887c-d1ce5850a9d3",
            "compositeImage": {
                "id": "77fd03fb-6a4c-40c8-895d-238669be57bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab7261b-6a21-4c7b-9794-faebc978622b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3fdafd1-b06e-4f9a-8322-ea1f607dba99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab7261b-6a21-4c7b-9794-faebc978622b",
                    "LayerId": "b6b5ee7e-c09d-4765-b402-af1a51876a77"
                }
            ]
        },
        {
            "id": "1f063a8e-4c5b-4676-a65d-63359c7433e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea4b042-efe7-4225-887c-d1ce5850a9d3",
            "compositeImage": {
                "id": "0df76b96-8a97-41fe-8d66-d9fe1b0e911f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f063a8e-4c5b-4676-a65d-63359c7433e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37f15d93-ad0a-47d2-9e1b-0128982bec2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f063a8e-4c5b-4676-a65d-63359c7433e3",
                    "LayerId": "b6b5ee7e-c09d-4765-b402-af1a51876a77"
                }
            ]
        },
        {
            "id": "9647db60-2055-4dcc-bf30-7e2fb9dcf843",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea4b042-efe7-4225-887c-d1ce5850a9d3",
            "compositeImage": {
                "id": "891fd300-4962-423a-b039-b209db8c6ab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9647db60-2055-4dcc-bf30-7e2fb9dcf843",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d8a838-5db5-4825-b5fb-d5fe358a6177",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9647db60-2055-4dcc-bf30-7e2fb9dcf843",
                    "LayerId": "b6b5ee7e-c09d-4765-b402-af1a51876a77"
                }
            ]
        },
        {
            "id": "ce3e4be9-9c21-4a7f-8269-aac9b959fe24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea4b042-efe7-4225-887c-d1ce5850a9d3",
            "compositeImage": {
                "id": "0a78f71d-0037-4156-8109-6fde1b4af4cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce3e4be9-9c21-4a7f-8269-aac9b959fe24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c3737d9-45d0-4a15-b6ae-b2c2cc7b96a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce3e4be9-9c21-4a7f-8269-aac9b959fe24",
                    "LayerId": "b6b5ee7e-c09d-4765-b402-af1a51876a77"
                }
            ]
        },
        {
            "id": "c2b0e81d-f46b-4702-91da-9a3fb63cdbbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea4b042-efe7-4225-887c-d1ce5850a9d3",
            "compositeImage": {
                "id": "6a9a89e2-17f7-4fbb-ae19-14ba2be0ab2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b0e81d-f46b-4702-91da-9a3fb63cdbbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "124e1dc6-0077-4102-85a6-7564f7ec7139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b0e81d-f46b-4702-91da-9a3fb63cdbbd",
                    "LayerId": "b6b5ee7e-c09d-4765-b402-af1a51876a77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "b6b5ee7e-c09d-4765-b402-af1a51876a77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dea4b042-efe7-4225-887c-d1ce5850a9d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0.3,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 18
}