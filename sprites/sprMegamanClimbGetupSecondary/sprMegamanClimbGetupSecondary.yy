{
    "id": "d846145b-3739-4728-a11b-8d19e5fe39f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbGetupSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15b5e14f-bcaa-4315-a26d-e9ca738851ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d846145b-3739-4728-a11b-8d19e5fe39f0",
            "compositeImage": {
                "id": "f7b6ba8f-6a2d-49c0-b875-192622343350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b5e14f-bcaa-4315-a26d-e9ca738851ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cff385c-de2b-4f9c-b026-481d59adfa8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b5e14f-bcaa-4315-a26d-e9ca738851ca",
                    "LayerId": "da123271-4595-4c41-b344-beda3a6a9d2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "da123271-4595-4c41-b344-beda3a6a9d2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d846145b-3739-4728-a11b-8d19e5fe39f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 5
}