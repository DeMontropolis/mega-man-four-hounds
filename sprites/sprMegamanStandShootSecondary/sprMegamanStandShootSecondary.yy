{
    "id": "f6eba650-7928-4d93-8b2b-6f11ddf219fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandShootSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9aa1729d-8614-4319-bc6c-679ccdf1560d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6eba650-7928-4d93-8b2b-6f11ddf219fd",
            "compositeImage": {
                "id": "72d7fa3f-6307-4a47-9160-b025fdb1d649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa1729d-8614-4319-bc6c-679ccdf1560d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32a9cda7-532e-41fb-ae73-70f95362dbc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa1729d-8614-4319-bc6c-679ccdf1560d",
                    "LayerId": "ac59c409-abe1-449d-9b63-781ad357e831"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ac59c409-abe1-449d-9b63-781ad357e831",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6eba650-7928-4d93-8b2b-6f11ddf219fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 10,
    "yorig": 7
}