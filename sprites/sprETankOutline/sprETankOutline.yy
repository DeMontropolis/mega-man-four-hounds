{
    "id": "85085360-90d4-438c-914b-a44a3d346fa7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprETankOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8831dd8e-6855-4fef-8521-42f5e0aac553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85085360-90d4-438c-914b-a44a3d346fa7",
            "compositeImage": {
                "id": "bd9e2be7-d0e5-4ea5-9143-dc95a4ede77a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8831dd8e-6855-4fef-8521-42f5e0aac553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c62ea47c-545d-4d17-b27d-94c8c4624c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8831dd8e-6855-4fef-8521-42f5e0aac553",
                    "LayerId": "342013a2-5db5-4764-abe2-e22fe5a99ca7"
                }
            ]
        },
        {
            "id": "25106d48-5ced-47fa-9712-6dcbb1be767d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85085360-90d4-438c-914b-a44a3d346fa7",
            "compositeImage": {
                "id": "eea0d117-ce30-4771-88db-28eeeeb5d20c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25106d48-5ced-47fa-9712-6dcbb1be767d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf73b0ba-9765-4cdc-9fe7-c43ad7e41e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25106d48-5ced-47fa-9712-6dcbb1be767d",
                    "LayerId": "342013a2-5db5-4764-abe2-e22fe5a99ca7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "342013a2-5db5-4764-abe2-e22fe5a99ca7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85085360-90d4-438c-914b-a44a3d346fa7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}