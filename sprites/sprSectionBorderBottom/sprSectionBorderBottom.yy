{
    "id": "ce81f532-f7bc-41bb-8bb1-e7b00f1eb853",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionBorderBottom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6c76021-3901-4b84-b46b-76f9ad0020ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce81f532-f7bc-41bb-8bb1-e7b00f1eb853",
            "compositeImage": {
                "id": "2d0016a5-f800-4d52-acbc-e3bb3af0fd83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c76021-3901-4b84-b46b-76f9ad0020ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474ee8a5-03c8-4d1a-a254-e4320dcfb46f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c76021-3901-4b84-b46b-76f9ad0020ce",
                    "LayerId": "454bfa31-6f49-4070-a076-12006dc6f4ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "454bfa31-6f49-4070-a076-12006dc6f4ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce81f532-f7bc-41bb-8bb1-e7b00f1eb853",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}