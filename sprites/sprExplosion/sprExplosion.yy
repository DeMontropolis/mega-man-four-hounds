{
    "id": "6040f0ee-d72e-430b-8f7e-b186682f6a41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprExplosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8eb49d5c-fbeb-4fa3-b04b-45348c2c035d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6040f0ee-d72e-430b-8f7e-b186682f6a41",
            "compositeImage": {
                "id": "4aea6c0d-da61-4641-a8a4-bc4b820b26f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb49d5c-fbeb-4fa3-b04b-45348c2c035d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52b2388d-08a2-4d04-8ffc-b9fe50f0549e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb49d5c-fbeb-4fa3-b04b-45348c2c035d",
                    "LayerId": "f8dba0d8-8752-4319-b82f-8b4095511bb5"
                }
            ]
        },
        {
            "id": "fdab9942-2693-42a6-89ba-8c2359f38431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6040f0ee-d72e-430b-8f7e-b186682f6a41",
            "compositeImage": {
                "id": "e6040f8a-95bf-4386-8483-50014f180c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdab9942-2693-42a6-89ba-8c2359f38431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "663fb572-a8d7-4fe0-b471-9db3ed6dde42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdab9942-2693-42a6-89ba-8c2359f38431",
                    "LayerId": "f8dba0d8-8752-4319-b82f-8b4095511bb5"
                }
            ]
        },
        {
            "id": "5bf3ba01-a52b-43e2-8760-c32407c92c74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6040f0ee-d72e-430b-8f7e-b186682f6a41",
            "compositeImage": {
                "id": "d8f4fe03-2948-4983-9c48-d278f3849c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bf3ba01-a52b-43e2-8760-c32407c92c74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff9b703-3139-40e9-9555-94019a8275ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bf3ba01-a52b-43e2-8760-c32407c92c74",
                    "LayerId": "f8dba0d8-8752-4319-b82f-8b4095511bb5"
                }
            ]
        },
        {
            "id": "e6ffbf40-f520-4d20-8dde-78b038013951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6040f0ee-d72e-430b-8f7e-b186682f6a41",
            "compositeImage": {
                "id": "13925f67-71a4-4878-a8d7-61f12a955473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6ffbf40-f520-4d20-8dde-78b038013951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a5aaf14-d5eb-40e4-b9b9-1071afda4c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6ffbf40-f520-4d20-8dde-78b038013951",
                    "LayerId": "f8dba0d8-8752-4319-b82f-8b4095511bb5"
                }
            ]
        },
        {
            "id": "0bdfa042-a142-4dbc-b863-0ad4e10016f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6040f0ee-d72e-430b-8f7e-b186682f6a41",
            "compositeImage": {
                "id": "2bfb4631-2402-497a-b9bb-8eabdf8e7eb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bdfa042-a142-4dbc-b863-0ad4e10016f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10fa10f7-5052-4aeb-8e20-ab4db85efaf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bdfa042-a142-4dbc-b863-0ad4e10016f3",
                    "LayerId": "f8dba0d8-8752-4319-b82f-8b4095511bb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "f8dba0d8-8752-4319-b82f-8b4095511bb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6040f0ee-d72e-430b-8f7e-b186682f6a41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}