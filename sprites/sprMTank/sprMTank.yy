{
    "id": "1f12b455-600a-4211-8088-c65cdb757421",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMTank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51095b49-ff16-4ddb-8c54-a4d479bea74e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f12b455-600a-4211-8088-c65cdb757421",
            "compositeImage": {
                "id": "20dff5e0-266b-4727-bc31-83331424afc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51095b49-ff16-4ddb-8c54-a4d479bea74e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd52d507-bd0b-44ee-97e9-071b35acb908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51095b49-ff16-4ddb-8c54-a4d479bea74e",
                    "LayerId": "02e0a9f6-41fd-47ed-959d-4c43f08bbbc0"
                }
            ]
        },
        {
            "id": "7122ae64-4a5b-4b68-8841-203da6a4de7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f12b455-600a-4211-8088-c65cdb757421",
            "compositeImage": {
                "id": "4c1c72ea-7ed4-4dfb-b829-e574ecbbdeea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7122ae64-4a5b-4b68-8841-203da6a4de7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10421698-339c-474c-a988-beeeecf1ed91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7122ae64-4a5b-4b68-8841-203da6a4de7c",
                    "LayerId": "02e0a9f6-41fd-47ed-959d-4c43f08bbbc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "02e0a9f6-41fd-47ed-959d-4c43f08bbbc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f12b455-600a-4211-8088-c65cdb757421",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}