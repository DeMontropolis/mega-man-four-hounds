{
    "id": "935e0bc4-a8a1-4717-b22e-71beaad8691b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWeaponGet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "565a687c-e268-45a2-9d56-64d9f7d6b2cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935e0bc4-a8a1-4717-b22e-71beaad8691b",
            "compositeImage": {
                "id": "a4f9f8e1-e602-4edc-bc43-e78386afda71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565a687c-e268-45a2-9d56-64d9f7d6b2cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec18a2a7-92c8-44af-8811-4a568185cd5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565a687c-e268-45a2-9d56-64d9f7d6b2cd",
                    "LayerId": "2a4ac5ee-4ef4-4cac-942b-a783491455e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 93,
    "layers": [
        {
            "id": "2a4ac5ee-4ef4-4cac-942b-a783491455e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "935e0bc4-a8a1-4717-b22e-71beaad8691b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 46
}