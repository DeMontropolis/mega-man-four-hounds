{
    "id": "5426e92e-b881-4d8e-9870-367a8641b831",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionBorderTopScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b815065-f4c1-4835-a812-df4b89df1c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5426e92e-b881-4d8e-9870-367a8641b831",
            "compositeImage": {
                "id": "d9e57c02-eea2-401e-8126-3a67cf091c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b815065-f4c1-4835-a812-df4b89df1c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6c205a1-ec59-4495-882f-1d54e072220d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b815065-f4c1-4835-a812-df4b89df1c6a",
                    "LayerId": "aa5832ab-fa75-4925-93a5-b675a67734ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "aa5832ab-fa75-4925-93a5-b675a67734ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5426e92e-b881-4d8e-9870-367a8641b831",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}