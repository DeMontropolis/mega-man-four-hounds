{
    "id": "17d4c710-10ac-4c2d-a41a-031f6d2eac5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSilverTomahawk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f68fe78a-17ee-4df9-a299-30c866b1759f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17d4c710-10ac-4c2d-a41a-031f6d2eac5c",
            "compositeImage": {
                "id": "c8eb39c4-d0d9-4acf-864d-783d494b5a33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f68fe78a-17ee-4df9-a299-30c866b1759f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "980e6adc-e339-4011-869d-1964763301d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f68fe78a-17ee-4df9-a299-30c866b1759f",
                    "LayerId": "0155125c-29b7-43e0-abc3-7c918382b9ba"
                }
            ]
        },
        {
            "id": "07c7ce95-cf6a-4a69-b004-de3657970e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17d4c710-10ac-4c2d-a41a-031f6d2eac5c",
            "compositeImage": {
                "id": "71558949-1481-4818-af46-ba7a5807bd09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07c7ce95-cf6a-4a69-b004-de3657970e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "905208f8-ad5b-4712-bd26-de908aad8001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07c7ce95-cf6a-4a69-b004-de3657970e77",
                    "LayerId": "0155125c-29b7-43e0-abc3-7c918382b9ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "0155125c-29b7-43e0-abc3-7c918382b9ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17d4c710-10ac-4c2d-a41a-031f6d2eac5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 7
}