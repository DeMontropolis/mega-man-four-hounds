{
    "id": "c18c3c45-fcee-4a47-8df4-38c960c03300",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRushMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e9cc93e-7b6f-4755-9b97-614b8f55daf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c18c3c45-fcee-4a47-8df4-38c960c03300",
            "compositeImage": {
                "id": "92ac6756-1a4b-46eb-a9c2-a985f29e1e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e9cc93e-7b6f-4755-9b97-614b8f55daf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2347586d-38ed-4944-92e6-ef3ce1ee0e9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e9cc93e-7b6f-4755-9b97-614b8f55daf6",
                    "LayerId": "89a99fe2-5558-496d-9477-e61292f93c01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "89a99fe2-5558-496d-9477-e61292f93c01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c18c3c45-fcee-4a47-8df4-38c960c03300",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 8
}