{
    "id": "48e55b02-8077-4422-844a-cbd0f75c5f3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpShootOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "430e706e-c9f6-4d9f-a21d-df4fb443c124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48e55b02-8077-4422-844a-cbd0f75c5f3c",
            "compositeImage": {
                "id": "4ea983bc-116e-49c6-bbbf-b06ac053cbc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430e706e-c9f6-4d9f-a21d-df4fb443c124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c99f49-3b8f-45b3-bd74-049b63d002ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430e706e-c9f6-4d9f-a21d-df4fb443c124",
                    "LayerId": "ed657bcf-4e89-4a6e-8fa2-72a374b08201"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "ed657bcf-4e89-4a6e-8fa2-72a374b08201",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48e55b02-8077-4422-844a-cbd0f75c5f3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 12,
    "yorig": 7
}