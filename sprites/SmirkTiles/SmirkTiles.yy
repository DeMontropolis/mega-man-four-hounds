{
    "id": "95551575-8557-4c8a-a520-ceacc7ba6f28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SmirkTiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e1ee9ae-d48f-4ad5-9a07-21d3e3a4fa22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95551575-8557-4c8a-a520-ceacc7ba6f28",
            "compositeImage": {
                "id": "bde8d2fa-9659-4e93-a249-653a0ae719c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e1ee9ae-d48f-4ad5-9a07-21d3e3a4fa22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e83a7b26-de89-44be-bd25-7ee2b1a0493f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e1ee9ae-d48f-4ad5-9a07-21d3e3a4fa22",
                    "LayerId": "3b870e55-bd68-42b1-bc03-2439c9606d1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "3b870e55-bd68-42b1-bc03-2439c9606d1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95551575-8557-4c8a-a520-ceacc7ba6f28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 171,
    "xorig": 120,
    "yorig": 87
}