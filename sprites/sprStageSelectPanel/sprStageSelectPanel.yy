{
    "id": "487a45a1-b10e-475c-bfd2-b624a7199a1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStageSelectPanel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bf0aebb-9ddf-4217-ab0a-8e9dd2b6e5d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "487a45a1-b10e-475c-bfd2-b624a7199a1a",
            "compositeImage": {
                "id": "bdd9c08c-2284-4bf2-9445-68a6f7fb6456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bf0aebb-9ddf-4217-ab0a-8e9dd2b6e5d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b05bffdd-c6bc-4429-94de-7436f5961bd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bf0aebb-9ddf-4217-ab0a-8e9dd2b6e5d7",
                    "LayerId": "2aee7df3-98c4-4703-8434-5e0a7445faff"
                }
            ]
        },
        {
            "id": "52e5f57a-f56a-449f-a882-0c795e9adee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "487a45a1-b10e-475c-bfd2-b624a7199a1a",
            "compositeImage": {
                "id": "39620b69-e980-46bd-bab8-24879adc8a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e5f57a-f56a-449f-a882-0c795e9adee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73d263d5-2fb6-4cb7-bc0b-579d80ff95dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e5f57a-f56a-449f-a882-0c795e9adee4",
                    "LayerId": "2aee7df3-98c4-4703-8434-5e0a7445faff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "2aee7df3-98c4-4703-8434-5e0a7445faff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "487a45a1-b10e-475c-bfd2-b624a7199a1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}