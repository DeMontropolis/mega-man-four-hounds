{
    "id": "dcae2e48-1f6f-48cb-baf5-a49f514360f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionBorderBottomScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54f99384-0b79-4b9d-b96e-4b257e802415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcae2e48-1f6f-48cb-baf5-a49f514360f2",
            "compositeImage": {
                "id": "e1455bde-ca25-4298-8f7d-fbc06abd315b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54f99384-0b79-4b9d-b96e-4b257e802415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea24f235-a66c-4da9-9c29-fa8d0ec1c449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54f99384-0b79-4b9d-b96e-4b257e802415",
                    "LayerId": "dc599b04-1d7f-4344-91c1-b1529c75bdd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dc599b04-1d7f-4344-91c1-b1529c75bdd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcae2e48-1f6f-48cb-baf5-a49f514360f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}