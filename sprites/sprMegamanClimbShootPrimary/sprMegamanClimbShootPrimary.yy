{
    "id": "a2d54159-3d5f-4e6c-a832-01b244e4b4d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbShootPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0cf31d8-0c15-4f39-b51c-f3844dde9583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2d54159-3d5f-4e6c-a832-01b244e4b4d9",
            "compositeImage": {
                "id": "6eb1d54a-98ae-47b6-bdd1-9a9568b45225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0cf31d8-0c15-4f39-b51c-f3844dde9583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e0b105-e855-4253-aadd-7ff7c80a82bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0cf31d8-0c15-4f39-b51c-f3844dde9583",
                    "LayerId": "0637ab79-bafb-4472-87b0-4d072594beab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "0637ab79-bafb-4472-87b0-4d072594beab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2d54159-3d5f-4e6c-a832-01b244e4b4d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 8,
    "yorig": 7
}