{
    "id": "2ad6baea-4170-4d58-8ab5-08217bfbc156",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBluePokerTellyDispenser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db30af37-6478-4f5f-b56e-d2f93312e796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ad6baea-4170-4d58-8ab5-08217bfbc156",
            "compositeImage": {
                "id": "4dda3c97-879b-4040-82f9-053db20ff399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db30af37-6478-4f5f-b56e-d2f93312e796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d4a4300-6a15-4b42-ac7f-2738a08e8e7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db30af37-6478-4f5f-b56e-d2f93312e796",
                    "LayerId": "d720c756-eb0f-4ccd-b761-8104684938ee"
                }
            ]
        },
        {
            "id": "a3a21a2f-506a-4b23-84f6-8f75422e339b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ad6baea-4170-4d58-8ab5-08217bfbc156",
            "compositeImage": {
                "id": "c261390c-3e3c-44f2-9ba6-209261ca1eee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a21a2f-506a-4b23-84f6-8f75422e339b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98c7e244-ee76-4f04-bc50-4ef58add852a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a21a2f-506a-4b23-84f6-8f75422e339b",
                    "LayerId": "d720c756-eb0f-4ccd-b761-8104684938ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d720c756-eb0f-4ccd-b761-8104684938ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ad6baea-4170-4d58-8ab5-08217bfbc156",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0.01,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}