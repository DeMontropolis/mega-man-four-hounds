{
    "id": "febedaf0-8904-45a6-a27a-92ad4d6cca9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecf96b75-8598-4693-b71d-2b77436428f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "febedaf0-8904-45a6-a27a-92ad4d6cca9d",
            "compositeImage": {
                "id": "9e1a3f4b-ea98-4ecc-b009-15b6426b6457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf96b75-8598-4693-b71d-2b77436428f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02556c65-137a-4420-bddc-4b2fb6e2ca04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf96b75-8598-4693-b71d-2b77436428f6",
                    "LayerId": "7c8d4629-bb2f-48f3-839f-af99f2d35e3a"
                }
            ]
        },
        {
            "id": "4a9211cd-5b79-47d3-99c4-e62d9e585dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "febedaf0-8904-45a6-a27a-92ad4d6cca9d",
            "compositeImage": {
                "id": "e41347dd-d522-422f-90e1-b889cdca3d57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a9211cd-5b79-47d3-99c4-e62d9e585dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e615a8e4-b723-442c-9ea8-500b114fdeb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a9211cd-5b79-47d3-99c4-e62d9e585dfc",
                    "LayerId": "7c8d4629-bb2f-48f3-839f-af99f2d35e3a"
                }
            ]
        },
        {
            "id": "c89b1e8f-af5b-4a4f-ae52-7f53d4c15700",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "febedaf0-8904-45a6-a27a-92ad4d6cca9d",
            "compositeImage": {
                "id": "a834057d-2eb1-4f83-ac4a-34e8c1d0e522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c89b1e8f-af5b-4a4f-ae52-7f53d4c15700",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dedfdc8a-ea57-41bc-9a8d-2cd47857a34e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c89b1e8f-af5b-4a4f-ae52-7f53d4c15700",
                    "LayerId": "7c8d4629-bb2f-48f3-839f-af99f2d35e3a"
                }
            ]
        },
        {
            "id": "9861a768-54f8-4fb3-a237-ac77362ad891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "febedaf0-8904-45a6-a27a-92ad4d6cca9d",
            "compositeImage": {
                "id": "02070a80-4c39-4cec-829d-0507db01c302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9861a768-54f8-4fb3-a237-ac77362ad891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "315cee0e-8f8d-4453-a61e-1191aa154196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9861a768-54f8-4fb3-a237-ac77362ad891",
                    "LayerId": "7c8d4629-bb2f-48f3-839f-af99f2d35e3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "7c8d4629-bb2f-48f3-839f-af99f2d35e3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "febedaf0-8904-45a6-a27a-92ad4d6cca9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 11,
    "yorig": 7
}