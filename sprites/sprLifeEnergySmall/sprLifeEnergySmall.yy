{
    "id": "3440093d-87b6-40c3-a639-53cc42cf3c45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLifeEnergySmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b6727b3-56ad-4b7e-83b8-158718f69d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3440093d-87b6-40c3-a639-53cc42cf3c45",
            "compositeImage": {
                "id": "e3fd3303-3d52-4eeb-8280-0ce1e29172cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b6727b3-56ad-4b7e-83b8-158718f69d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f29f20-4fb8-4f0a-9f9e-0d8ec22f42c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b6727b3-56ad-4b7e-83b8-158718f69d95",
                    "LayerId": "5c39e622-4ade-4693-9002-41aa0a29f265"
                }
            ]
        },
        {
            "id": "895e59ac-37f9-4406-a321-a9faceec7b2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3440093d-87b6-40c3-a639-53cc42cf3c45",
            "compositeImage": {
                "id": "5c60c6b0-bef9-4c61-8fbd-62dafff479ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895e59ac-37f9-4406-a321-a9faceec7b2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "437e275b-ffd9-4e36-bab5-c7ffbc712c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895e59ac-37f9-4406-a321-a9faceec7b2d",
                    "LayerId": "5c39e622-4ade-4693-9002-41aa0a29f265"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "5c39e622-4ade-4693-9002-41aa0a29f265",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3440093d-87b6-40c3-a639-53cc42cf3c45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": -4,
    "yorig": -8
}