{
    "id": "fd64bb7e-117d-453b-a830-36bda32a9f11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SmirkTiles_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "642591af-da44-45bb-9f78-75ec5cf9b3ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd64bb7e-117d-453b-a830-36bda32a9f11",
            "compositeImage": {
                "id": "d3719022-7d0c-4891-919a-3dd79508e8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "642591af-da44-45bb-9f78-75ec5cf9b3ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebaef2d8-ff59-46e4-8b58-033e23efa43a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "642591af-da44-45bb-9f78-75ec5cf9b3ac",
                    "LayerId": "005f1aa7-c202-4a0d-b4f3-59ba25318ebf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "005f1aa7-c202-4a0d-b4f3-59ba25318ebf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd64bb7e-117d-453b-a830-36bda32a9f11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 24
}