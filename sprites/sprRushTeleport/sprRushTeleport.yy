{
    "id": "8419026f-97f9-4750-aa3a-9216f7bb5ab2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRushTeleport",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d65cfc74-8e31-4555-830f-cef23e439bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8419026f-97f9-4750-aa3a-9216f7bb5ab2",
            "compositeImage": {
                "id": "55fa4e81-3a6a-4031-ba03-c6b255f73311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d65cfc74-8e31-4555-830f-cef23e439bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccdbf9ad-51d5-4253-81bc-9db3b5db3add",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d65cfc74-8e31-4555-830f-cef23e439bd8",
                    "LayerId": "ece1a665-42a0-4d8a-b7e2-6974d3ca4bad"
                }
            ]
        },
        {
            "id": "a20d41da-85ca-48c0-ac04-09f029bbcc2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8419026f-97f9-4750-aa3a-9216f7bb5ab2",
            "compositeImage": {
                "id": "9ad1498e-8e64-439d-8808-299a25851784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a20d41da-85ca-48c0-ac04-09f029bbcc2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86e1c82d-9314-4939-9b8e-39ef07ee8641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a20d41da-85ca-48c0-ac04-09f029bbcc2a",
                    "LayerId": "ece1a665-42a0-4d8a-b7e2-6974d3ca4bad"
                }
            ]
        },
        {
            "id": "2798373d-137f-499d-8cab-49675e13d371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8419026f-97f9-4750-aa3a-9216f7bb5ab2",
            "compositeImage": {
                "id": "63596a65-187e-456a-8d8c-1536ce06b3a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2798373d-137f-499d-8cab-49675e13d371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1d2fbe3-947c-4378-84a4-df5d4d482617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2798373d-137f-499d-8cab-49675e13d371",
                    "LayerId": "ece1a665-42a0-4d8a-b7e2-6974d3ca4bad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ece1a665-42a0-4d8a-b7e2-6974d3ca4bad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8419026f-97f9-4750-aa3a-9216f7bb5ab2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 16
}