{
    "id": "c35c2509-96be-4c19-a827-c911b826b221",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBossDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1166e9f7-8d9f-442a-9477-0dc944f7f97d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "a42485cb-1c55-4241-aaec-5ac625ac87d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1166e9f7-8d9f-442a-9477-0dc944f7f97d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "161fcde6-ff79-4e1f-a1a0-531ff036c969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1166e9f7-8d9f-442a-9477-0dc944f7f97d",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        },
        {
            "id": "302fa443-8d4d-40b4-844a-371d536800dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "66150099-7b1f-4c61-8998-bc0182a3b751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302fa443-8d4d-40b4-844a-371d536800dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb8c518-9b30-4ed3-a230-77643076c8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302fa443-8d4d-40b4-844a-371d536800dc",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        },
        {
            "id": "81ffb941-344a-4ad4-a3ce-e30c05d0162b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "ab660e7f-3105-41f2-9ea3-521d6dc1b130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ffb941-344a-4ad4-a3ce-e30c05d0162b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d221bf8-4e86-4ba1-ba10-8b47cf15a703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ffb941-344a-4ad4-a3ce-e30c05d0162b",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        },
        {
            "id": "3cd61cd1-1901-4b77-9406-05c5ed08d4e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "272333c7-4551-4a90-9037-e39f6e23be53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd61cd1-1901-4b77-9406-05c5ed08d4e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11b7cdfe-577c-4980-a8f1-45b08cc66f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd61cd1-1901-4b77-9406-05c5ed08d4e1",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        },
        {
            "id": "a732ea33-d6a5-4787-8982-9592f4af3a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "da42cf68-a4d4-4ec0-a8d0-62af46031802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a732ea33-d6a5-4787-8982-9592f4af3a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c00b3a33-8fd7-4d1d-b526-cba8a6f72e42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a732ea33-d6a5-4787-8982-9592f4af3a0c",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        },
        {
            "id": "b3b01a19-31bf-4672-8a4e-defbad977999",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "5c4dd011-06af-4861-ac3c-8a94835a154f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b01a19-31bf-4672-8a4e-defbad977999",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdafd1c2-76f7-4d8a-add2-94b939eb1fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b01a19-31bf-4672-8a4e-defbad977999",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        },
        {
            "id": "f8b8ac8a-a356-4112-91a7-8e3a66acf5f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "c32a393f-0c28-4536-b3ba-c3e1fb4bdcba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8b8ac8a-a356-4112-91a7-8e3a66acf5f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca85e81d-0845-4eb4-be40-72d2af67346f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8b8ac8a-a356-4112-91a7-8e3a66acf5f5",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        },
        {
            "id": "8daf085f-5c61-4e00-b974-709e5d6b4558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "524bdd7e-b1c9-48bc-b77c-71e51aed3756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8daf085f-5c61-4e00-b974-709e5d6b4558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b8a5aa-f125-4103-a414-7672d2a17d87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8daf085f-5c61-4e00-b974-709e5d6b4558",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        },
        {
            "id": "58253edc-8a11-4eec-973f-6703d0794b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "compositeImage": {
                "id": "9cec5bb6-49af-4cf7-b812-94d740eb0c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58253edc-8a11-4eec-973f-6703d0794b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba0e31f0-eee6-43ff-95c6-f32b1d6efd9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58253edc-8a11-4eec-973f-6703d0794b3b",
                    "LayerId": "4943de1a-e8de-40ed-89e7-9b980d33b341"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4943de1a-e8de-40ed-89e7-9b980d33b341",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c35c2509-96be-4c19-a827-c911b826b221",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 1,
    "yorig": 0
}