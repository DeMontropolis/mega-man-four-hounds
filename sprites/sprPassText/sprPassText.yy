{
    "id": "2e4b8721-ef0c-4572-a809-a6f5f5dbccf8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPassText",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 93,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a020c75-cebe-43f8-bd23-407714ab0b8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e4b8721-ef0c-4572-a809-a6f5f5dbccf8",
            "compositeImage": {
                "id": "452ccddd-86ca-4afd-a12a-bb9690c402ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a020c75-cebe-43f8-bd23-407714ab0b8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a627adf5-6bb1-48ed-808f-1c423144245d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a020c75-cebe-43f8-bd23-407714ab0b8d",
                    "LayerId": "02a81d1e-14e3-4b9c-b79f-28fb9f76a7c3"
                }
            ]
        },
        {
            "id": "353eb8de-8a0a-40da-bc33-419231db8f2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e4b8721-ef0c-4572-a809-a6f5f5dbccf8",
            "compositeImage": {
                "id": "5e368b12-6be8-48e1-a7b6-a7d0a9579073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "353eb8de-8a0a-40da-bc33-419231db8f2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9a31855-9a97-46ee-9276-2dd899421b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "353eb8de-8a0a-40da-bc33-419231db8f2e",
                    "LayerId": "02a81d1e-14e3-4b9c-b79f-28fb9f76a7c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "02a81d1e-14e3-4b9c-b79f-28fb9f76a7c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e4b8721-ef0c-4572-a809-a6f5f5dbccf8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 0,
    "yorig": 0
}