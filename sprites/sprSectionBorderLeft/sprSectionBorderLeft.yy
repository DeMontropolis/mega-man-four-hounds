{
    "id": "b87a8cee-1fae-4676-8e94-b45999ba1e4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionBorderLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d13b059-069d-41c1-b990-380c9fcdce5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b87a8cee-1fae-4676-8e94-b45999ba1e4c",
            "compositeImage": {
                "id": "60feee18-8480-475c-af4b-aa8c4b1a9e64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d13b059-069d-41c1-b990-380c9fcdce5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "100a361e-8663-45fd-989f-76d6d9311cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d13b059-069d-41c1-b990-380c9fcdce5b",
                    "LayerId": "9f483070-6aed-470f-a02d-d1d359178b50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9f483070-6aed-470f-a02d-d1d359178b50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b87a8cee-1fae-4676-8e94-b45999ba1e4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}