{
    "id": "5c9d7560-8e80-42c4-a733-cc99a31a2278",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "445fa520-0cea-476c-a083-03d2f6003401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c9d7560-8e80-42c4-a733-cc99a31a2278",
            "compositeImage": {
                "id": "cb26de67-d280-4cde-8de4-943fb2c9ab66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "445fa520-0cea-476c-a083-03d2f6003401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d269eec7-3c9a-4221-b64f-aac36f51814b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "445fa520-0cea-476c-a083-03d2f6003401",
                    "LayerId": "7deaf266-d78d-4c01-bb66-e73addbc7985"
                }
            ]
        },
        {
            "id": "c72bff8d-6901-449d-b17f-6dda55d669a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c9d7560-8e80-42c4-a733-cc99a31a2278",
            "compositeImage": {
                "id": "7c074ea3-766b-4229-bd72-825ea1648b40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c72bff8d-6901-449d-b17f-6dda55d669a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98f1c240-cb8d-407b-9122-6e4432559e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c72bff8d-6901-449d-b17f-6dda55d669a3",
                    "LayerId": "7deaf266-d78d-4c01-bb66-e73addbc7985"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "7deaf266-d78d-4c01-bb66-e73addbc7985",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c9d7560-8e80-42c4-a733-cc99a31a2278",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 7
}