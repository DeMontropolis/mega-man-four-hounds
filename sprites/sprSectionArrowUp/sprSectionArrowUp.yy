{
    "id": "358951ae-76ce-4ef3-bb0e-0ecc9508e606",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionArrowUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bafa2c88-44c8-466f-af03-78ba53f4f741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "358951ae-76ce-4ef3-bb0e-0ecc9508e606",
            "compositeImage": {
                "id": "e44bb20e-62d2-4353-b11c-c6e3e099085c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bafa2c88-44c8-466f-af03-78ba53f4f741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ab63b40-8899-4d9d-8cb5-c033cfe989f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bafa2c88-44c8-466f-af03-78ba53f4f741",
                    "LayerId": "367a0a1e-7090-4e7f-930e-7f87ae4bdf0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "367a0a1e-7090-4e7f-930e-7f87ae4bdf0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "358951ae-76ce-4ef3-bb0e-0ecc9508e606",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}