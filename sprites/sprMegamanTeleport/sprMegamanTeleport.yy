{
    "id": "20834028-7a18-4bf4-8e69-484fe7297468",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanTeleport",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f1f06b1-5192-41d5-b036-2f6f90dd64d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20834028-7a18-4bf4-8e69-484fe7297468",
            "compositeImage": {
                "id": "703c5e22-555c-495b-9827-fcd09b4f5ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f1f06b1-5192-41d5-b036-2f6f90dd64d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "077b3c29-a363-4ecb-b8cf-1bc03fd2a08c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f1f06b1-5192-41d5-b036-2f6f90dd64d1",
                    "LayerId": "9faab3e2-c517-4619-be38-34f787f12310"
                }
            ]
        },
        {
            "id": "3913299d-c922-44b9-8fdc-f3b2001801c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20834028-7a18-4bf4-8e69-484fe7297468",
            "compositeImage": {
                "id": "9239a76f-0a6d-4630-8908-52b1c04b6a05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3913299d-c922-44b9-8fdc-f3b2001801c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69452484-00bc-4228-9f11-0470419598b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3913299d-c922-44b9-8fdc-f3b2001801c0",
                    "LayerId": "9faab3e2-c517-4619-be38-34f787f12310"
                }
            ]
        },
        {
            "id": "5a14451a-6d3c-4b17-8c32-877cb1d66719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20834028-7a18-4bf4-8e69-484fe7297468",
            "compositeImage": {
                "id": "60f6cff0-99ee-46c3-8028-496c6f426270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a14451a-6d3c-4b17-8c32-877cb1d66719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2269c80d-a6d1-47bd-8de9-a8568b5448a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a14451a-6d3c-4b17-8c32-877cb1d66719",
                    "LayerId": "9faab3e2-c517-4619-be38-34f787f12310"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9faab3e2-c517-4619-be38-34f787f12310",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20834028-7a18-4bf4-8e69-484fe7297468",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 15
}