{
    "id": "13552aea-949e-43aa-9646-5a2f5e4bfd28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRushCoil",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f59e7b30-2ea2-4401-a97f-216822980160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13552aea-949e-43aa-9646-5a2f5e4bfd28",
            "compositeImage": {
                "id": "ffb330db-740e-499a-8c23-383f0ce3e209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f59e7b30-2ea2-4401-a97f-216822980160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e258fb-3d9e-4f2c-9a83-d91d83e7cf86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f59e7b30-2ea2-4401-a97f-216822980160",
                    "LayerId": "c8932697-7e9e-4ae7-ba14-fce4b75ce54f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c8932697-7e9e-4ae7-ba14-fce4b75ce54f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13552aea-949e-43aa-9646-5a2f5e4bfd28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 7
}