{
    "id": "b69c4b57-7a0c-4545-ac6a-49c1048353fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponEnergyBigSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0de439e4-7609-47ea-ada2-d3a1baca53ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b69c4b57-7a0c-4545-ac6a-49c1048353fb",
            "compositeImage": {
                "id": "50379c75-6ccc-4c9f-9120-ecf006e64291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de439e4-7609-47ea-ada2-d3a1baca53ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86882136-bb3d-4f65-96f6-85ada9c806ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de439e4-7609-47ea-ada2-d3a1baca53ec",
                    "LayerId": "8d109812-0743-4f6f-850c-75b8c11679b9"
                }
            ]
        },
        {
            "id": "cf287dd6-94ae-47e2-bc2b-bb55180b2e92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b69c4b57-7a0c-4545-ac6a-49c1048353fb",
            "compositeImage": {
                "id": "122f1015-dfe2-4baa-8f25-0d8752a5129e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf287dd6-94ae-47e2-bc2b-bb55180b2e92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "296474ec-d67e-4715-867f-d42e22779b36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf287dd6-94ae-47e2-bc2b-bb55180b2e92",
                    "LayerId": "8d109812-0743-4f6f-850c-75b8c11679b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "8d109812-0743-4f6f-850c-75b8c11679b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b69c4b57-7a0c-4545-ac6a-49c1048353fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": -4
}