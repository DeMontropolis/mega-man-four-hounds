{
    "id": "54e02096-920d-4e18-87ef-535c367a0c63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohJumpBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be84abb5-f123-428a-ba17-bcb515eddbe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54e02096-920d-4e18-87ef-535c367a0c63",
            "compositeImage": {
                "id": "419fe1b8-03b4-41d3-b113-09536fb77884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be84abb5-f123-428a-ba17-bcb515eddbe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1ce563-03cf-421a-85c0-96d97ad94be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be84abb5-f123-428a-ba17-bcb515eddbe7",
                    "LayerId": "a59606c0-9cfb-41c5-8372-b757112a899f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a59606c0-9cfb-41c5-8372-b757112a899f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54e02096-920d-4e18-87ef-535c367a0c63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 16,
    "yorig": 16
}