{
    "id": "29df36d8-b5db-4fe0-9423-87199f4c7325",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanTeleportOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc015681-a40d-4300-af4a-71e29c6ba93e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29df36d8-b5db-4fe0-9423-87199f4c7325",
            "compositeImage": {
                "id": "9e430277-ad2e-4904-aa7f-f5e1f2844a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc015681-a40d-4300-af4a-71e29c6ba93e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d5cc054-116a-440f-abf0-8a277a1ce775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc015681-a40d-4300-af4a-71e29c6ba93e",
                    "LayerId": "afe46d02-fb9e-4632-91e4-94aa3abd06d7"
                }
            ]
        },
        {
            "id": "f7f75e03-3bc6-43fc-a36f-dc394a3b6954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29df36d8-b5db-4fe0-9423-87199f4c7325",
            "compositeImage": {
                "id": "c8a87410-c4b1-4628-ad43-260b2bea0ef3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f75e03-3bc6-43fc-a36f-dc394a3b6954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2447594d-744d-428a-83db-2eac2d62fb03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f75e03-3bc6-43fc-a36f-dc394a3b6954",
                    "LayerId": "afe46d02-fb9e-4632-91e4-94aa3abd06d7"
                }
            ]
        },
        {
            "id": "01fe00da-0387-492d-a8cf-dcc4d1efddbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29df36d8-b5db-4fe0-9423-87199f4c7325",
            "compositeImage": {
                "id": "ab375cae-2ad9-4c99-ac7b-f47d7ad43158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fe00da-0387-492d-a8cf-dcc4d1efddbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e447b0a4-3d68-4454-a977-3af3530ddcc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fe00da-0387-492d-a8cf-dcc4d1efddbc",
                    "LayerId": "afe46d02-fb9e-4632-91e4-94aa3abd06d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "afe46d02-fb9e-4632-91e4-94aa3abd06d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29df36d8-b5db-4fe0-9423-87199f4c7325",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 15
}