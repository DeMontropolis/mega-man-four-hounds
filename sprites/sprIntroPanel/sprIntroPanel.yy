{
    "id": "27131126-d7ee-43a1-a7b3-58ee40e50f97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroPanel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 151,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69edc782-e518-473b-81f3-af3c7a3ccc86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27131126-d7ee-43a1-a7b3-58ee40e50f97",
            "compositeImage": {
                "id": "f0bd6b68-ef65-4bd7-9a28-284586bddbed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69edc782-e518-473b-81f3-af3c7a3ccc86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89ed411b-3a14-40c4-ac79-e4bccfeab818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69edc782-e518-473b-81f3-af3c7a3ccc86",
                    "LayerId": "403d1b2e-5367-400d-af7b-f372c69926d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "403d1b2e-5367-400d-af7b-f372c69926d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27131126-d7ee-43a1-a7b3-58ee40e50f97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}