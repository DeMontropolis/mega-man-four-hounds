{
    "id": "cb02ede3-a41b-4ec5-b885-1f793352afba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMMStageSelect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b270af0-085c-40b1-a21a-ba8c24461cb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "92ac8883-b245-4105-9041-178a124c53b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b270af0-085c-40b1-a21a-ba8c24461cb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73aa7a8c-e5c6-43da-866f-46834b62b6f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b270af0-085c-40b1-a21a-ba8c24461cb8",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        },
        {
            "id": "b43f2043-08f9-4391-8b50-b592afbe6d0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "62d76e44-6a32-4fb7-afee-ef7008b9a2c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b43f2043-08f9-4391-8b50-b592afbe6d0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "117dba14-0fe0-4e19-83f9-72eba631800b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b43f2043-08f9-4391-8b50-b592afbe6d0e",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        },
        {
            "id": "0cf459b7-6baa-4fa6-b39a-bfe8f5c666de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "02127882-a26d-4187-962e-47b3f99b421d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cf459b7-6baa-4fa6-b39a-bfe8f5c666de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc55d6d4-3723-4a60-b895-cd946040aa77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cf459b7-6baa-4fa6-b39a-bfe8f5c666de",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        },
        {
            "id": "c31f0dda-8f43-46f7-af37-104d046a7db4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "10dc5dd6-61e2-4977-81d9-43a38118b8e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c31f0dda-8f43-46f7-af37-104d046a7db4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3452a16-ae64-4e55-8098-eebe543afb34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c31f0dda-8f43-46f7-af37-104d046a7db4",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        },
        {
            "id": "8da59d1d-7c32-478b-8a44-d79339e5dcca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "78bac3c7-6c94-4266-8795-6cb2747a94b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da59d1d-7c32-478b-8a44-d79339e5dcca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a4d3e3-5ab7-4099-ac2c-95964f5188cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da59d1d-7c32-478b-8a44-d79339e5dcca",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        },
        {
            "id": "456d8452-f6e1-4462-b04e-a4e22e608269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "9f767739-77f9-47e8-8178-0f2c0a94d74b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "456d8452-f6e1-4462-b04e-a4e22e608269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fde7039-6da7-4983-bd2e-7217f3942273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "456d8452-f6e1-4462-b04e-a4e22e608269",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        },
        {
            "id": "6a105a82-02b9-43d7-a38f-1ae54afb5154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "9b2a85c2-99e4-428d-affd-272dc9c32d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a105a82-02b9-43d7-a38f-1ae54afb5154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85899e2f-aece-43cf-a35d-df432ac78cf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a105a82-02b9-43d7-a38f-1ae54afb5154",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        },
        {
            "id": "e34983b0-5e82-4b99-bca4-ca92d6105bee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "b56fe9e8-0f66-473f-b158-c6625682c425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e34983b0-5e82-4b99-bca4-ca92d6105bee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0efc85a-4616-4792-84f0-2c52151d66fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e34983b0-5e82-4b99-bca4-ca92d6105bee",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        },
        {
            "id": "0a0433b6-ee80-4bcc-8a08-072cc1430c78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "compositeImage": {
                "id": "57f3f346-fbb5-41d0-8746-f1a29d756470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a0433b6-ee80-4bcc-8a08-072cc1430c78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc8b9ae5-4585-441c-a1e3-99bb854bd609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a0433b6-ee80-4bcc-8a08-072cc1430c78",
                    "LayerId": "436b9b14-da23-4138-94fa-603e057d7b58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "436b9b14-da23-4138-94fa-603e057d7b58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb02ede3-a41b-4ec5-b885-1f793352afba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}