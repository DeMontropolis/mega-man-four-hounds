{
    "id": "698c73ca-e6c1-4d95-a28d-1bbabda04cbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStarCrashInit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7d82872-f9ef-41ba-936e-c9c311ffbb45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "698c73ca-e6c1-4d95-a28d-1bbabda04cbf",
            "compositeImage": {
                "id": "c48a1f4d-094f-4448-bd68-ac68403d972f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7d82872-f9ef-41ba-936e-c9c311ffbb45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ae8c3a-94f1-4246-8b31-1a4fba6da2cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d82872-f9ef-41ba-936e-c9c311ffbb45",
                    "LayerId": "09f1fde0-773d-4a7a-8a47-f470899a4dc1"
                }
            ]
        },
        {
            "id": "0d21ddc3-4648-4908-8e43-d651e00c0e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "698c73ca-e6c1-4d95-a28d-1bbabda04cbf",
            "compositeImage": {
                "id": "001ef09a-b60c-4b8b-bb61-a993fac0d6a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d21ddc3-4648-4908-8e43-d651e00c0e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4128311-449f-4ad2-a91c-996c72c053a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d21ddc3-4648-4908-8e43-d651e00c0e3e",
                    "LayerId": "09f1fde0-773d-4a7a-8a47-f470899a4dc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "09f1fde0-773d-4a7a-8a47-f470899a4dc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "698c73ca-e6c1-4d95-a28d-1bbabda04cbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 17,
    "yorig": 15
}