{
    "id": "57ec4ebd-1160-41c6-a800-c84272250c26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgBeta",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fb4694f-a7b2-4bb1-bf9e-f5477fae1039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ec4ebd-1160-41c6-a800-c84272250c26",
            "compositeImage": {
                "id": "1359c64b-a1ff-4882-9a5c-38006bba17d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fb4694f-a7b2-4bb1-bf9e-f5477fae1039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2ba9ee-02d8-4fe3-a0d5-db621a67ae6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fb4694f-a7b2-4bb1-bf9e-f5477fae1039",
                    "LayerId": "6a805538-85ee-4f75-afae-40e71401d155"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6a805538-85ee-4f75-afae-40e71401d155",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57ec4ebd-1160-41c6-a800-c84272250c26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}