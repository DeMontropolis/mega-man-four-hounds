{
    "id": "cb9b69eb-72ec-4050-90e2-7e49a4128f46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohShotCharged",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bad4795b-109b-4c59-a423-86d79f73e99b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb9b69eb-72ec-4050-90e2-7e49a4128f46",
            "compositeImage": {
                "id": "629d2712-bb37-481b-bd1b-539c901fd3e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bad4795b-109b-4c59-a423-86d79f73e99b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3090a12a-9c15-4f30-b756-a9bc4bc9c245",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bad4795b-109b-4c59-a423-86d79f73e99b",
                    "LayerId": "5326d11d-42f1-47b0-8ad1-67b0110ece51"
                }
            ]
        },
        {
            "id": "3ba349de-d17c-415e-8936-0590862fec5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb9b69eb-72ec-4050-90e2-7e49a4128f46",
            "compositeImage": {
                "id": "f9c951db-a318-4854-869a-ed9d53f23da8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ba349de-d17c-415e-8936-0590862fec5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa4f7e7-e498-48cd-a940-8cdc7926f0f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ba349de-d17c-415e-8936-0590862fec5e",
                    "LayerId": "5326d11d-42f1-47b0-8ad1-67b0110ece51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "5326d11d-42f1-47b0-8ad1-67b0110ece51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb9b69eb-72ec-4050-90e2-7e49a4128f46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}