{
    "id": "0d009768-0503-48c4-8346-b9739fc46823",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mskMegamanSlide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 2,
    "bbox_right": 19,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e8376bc-f544-4a41-8efe-360fe1117433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d009768-0503-48c4-8346-b9739fc46823",
            "compositeImage": {
                "id": "c34953e4-6cd6-4bc3-8f32-83ac28a0ce44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e8376bc-f544-4a41-8efe-360fe1117433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e85acb59-3b08-4830-a32f-20d93b4cb6cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e8376bc-f544-4a41-8efe-360fe1117433",
                    "LayerId": "09aa1215-0fab-4ca2-b12c-84a4d60fdbc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "09aa1215-0fab-4ca2-b12c-84a4d60fdbc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d009768-0503-48c4-8346-b9739fc46823",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}