{
    "id": "b4393b1d-b72a-4003-ac68-a1c81fb29c60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6912e6dd-9d8c-49f2-86a0-ff25205b3b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4393b1d-b72a-4003-ac68-a1c81fb29c60",
            "compositeImage": {
                "id": "dd100e6f-b42c-4375-8e79-75a578017574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6912e6dd-9d8c-49f2-86a0-ff25205b3b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00b84de3-d5a0-43d4-aded-bc14de5bad3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6912e6dd-9d8c-49f2-86a0-ff25205b3b27",
                    "LayerId": "547ade51-9dac-47a0-b2f6-62269a548e24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "547ade51-9dac-47a0-b2f6-62269a548e24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4393b1d-b72a-4003-ac68-a1c81fb29c60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 7
}