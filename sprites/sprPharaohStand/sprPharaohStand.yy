{
    "id": "7b2b0b2c-5ebb-42b8-8b9b-1b1f1a646e32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohStand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb2cdf91-abe9-41c6-81c6-e4581f0ef0df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b2b0b2c-5ebb-42b8-8b9b-1b1f1a646e32",
            "compositeImage": {
                "id": "549fee99-0723-41c0-aade-2a15afd732d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb2cdf91-abe9-41c6-81c6-e4581f0ef0df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc3413a5-0d54-4f7e-ba64-7a789201f2c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb2cdf91-abe9-41c6-81c6-e4581f0ef0df",
                    "LayerId": "73b16cbb-734d-4b72-8100-2a900cb2b1ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "73b16cbb-734d-4b72-8100-2a900cb2b1ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b2b0b2c-5ebb-42b8-8b9b-1b1f1a646e32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 13
}