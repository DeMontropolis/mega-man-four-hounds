{
    "id": "cd3e3d65-8856-4ad4-a97b-4dde1e8b4eea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fec3f59-99ca-436b-87c8-ccd7c86a46a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd3e3d65-8856-4ad4-a97b-4dde1e8b4eea",
            "compositeImage": {
                "id": "7abcb714-165c-4391-8bf0-a8bcd2c045f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fec3f59-99ca-436b-87c8-ccd7c86a46a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a85795e-626b-40aa-8b30-0f5acd266855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fec3f59-99ca-436b-87c8-ccd7c86a46a3",
                    "LayerId": "61fd80d8-6be7-41b0-b94a-0d97705e3092"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "61fd80d8-6be7-41b0-b94a-0d97705e3092",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd3e3d65-8856-4ad4-a97b-4dde1e8b4eea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}