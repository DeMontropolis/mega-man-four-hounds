{
    "id": "2ed84345-469a-47e2-b2e9-6ecd43dbc97b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohJumpShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acf38a41-a370-4bce-af62-3eefebc454f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ed84345-469a-47e2-b2e9-6ecd43dbc97b",
            "compositeImage": {
                "id": "2846e9c4-f9d8-45ed-8062-56824a3c5abf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acf38a41-a370-4bce-af62-3eefebc454f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0782df0-e99c-4965-a063-5d51785f88e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acf38a41-a370-4bce-af62-3eefebc454f8",
                    "LayerId": "bdcd552a-99f7-4328-b37e-1fe8a9219ea9"
                }
            ]
        },
        {
            "id": "e4f03246-a8fb-4e4a-814e-ab5f1cdc5a97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ed84345-469a-47e2-b2e9-6ecd43dbc97b",
            "compositeImage": {
                "id": "bbac9b48-dcb1-4f86-969e-1fd05e22c536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4f03246-a8fb-4e4a-814e-ab5f1cdc5a97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8a3c9d4-edda-47c8-a7b5-73b1220bc7a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f03246-a8fb-4e4a-814e-ab5f1cdc5a97",
                    "LayerId": "bdcd552a-99f7-4328-b37e-1fe8a9219ea9"
                }
            ]
        },
        {
            "id": "af0a09c8-3494-47ed-852c-d3b1cd11aa8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ed84345-469a-47e2-b2e9-6ecd43dbc97b",
            "compositeImage": {
                "id": "bbfe7fca-3c14-4d89-8ad8-a4e7804cb83b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af0a09c8-3494-47ed-852c-d3b1cd11aa8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4371999-9f0d-4503-a3fb-b71100813647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af0a09c8-3494-47ed-852c-d3b1cd11aa8d",
                    "LayerId": "bdcd552a-99f7-4328-b37e-1fe8a9219ea9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "bdcd552a-99f7-4328-b37e-1fe8a9219ea9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ed84345-469a-47e2-b2e9-6ecd43dbc97b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 16,
    "yorig": 24
}