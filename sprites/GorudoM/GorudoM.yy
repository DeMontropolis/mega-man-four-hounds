{
    "id": "5538eb49-1851-4f68-b600-6a6640d6822a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "GorudoM",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "794f900e-9562-4086-a607-7b15b8ffa0d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5538eb49-1851-4f68-b600-6a6640d6822a",
            "compositeImage": {
                "id": "d792d2a8-07c6-49fc-bc3e-7fb6b8749006",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "794f900e-9562-4086-a607-7b15b8ffa0d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4245c9e4-9b9e-47b3-b412-692993be04ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "794f900e-9562-4086-a607-7b15b8ffa0d8",
                    "LayerId": "4e8bc115-7f51-49f2-8c5a-2174fb6505e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e8bc115-7f51-49f2-8c5a-2174fb6505e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5538eb49-1851-4f68-b600-6a6640d6822a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 16
}