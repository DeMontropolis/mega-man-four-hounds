{
    "id": "3aa826da-e22e-4561-8654-61a340dbc20f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBusterShot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a109f5d-2bf9-4eb3-b860-d72b0faae7af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aa826da-e22e-4561-8654-61a340dbc20f",
            "compositeImage": {
                "id": "b0e7b860-8ea6-4a36-b429-49b558c2245e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a109f5d-2bf9-4eb3-b860-d72b0faae7af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "325f7606-6dc2-453a-9097-99e5a68671e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a109f5d-2bf9-4eb3-b860-d72b0faae7af",
                    "LayerId": "d0bf71b8-f43d-46e2-9614-2049a6b4c236"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "d0bf71b8-f43d-46e2-9614-2049a6b4c236",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3aa826da-e22e-4561-8654-61a340dbc20f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 3
}