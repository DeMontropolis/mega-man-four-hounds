{
    "id": "cc6f81db-ea13-46ce-9b37-875c430a9800",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPauseMenuBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 224,
    "bbox_left": 0,
    "bbox_right": 256,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6de0ccf6-41ff-407e-9e7e-3d44fddd38a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f81db-ea13-46ce-9b37-875c430a9800",
            "compositeImage": {
                "id": "5c568c9d-c0a5-48ea-b9f7-3e63d444a578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de0ccf6-41ff-407e-9e7e-3d44fddd38a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91cfacf7-6a90-4372-97de-d27789913a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de0ccf6-41ff-407e-9e7e-3d44fddd38a0",
                    "LayerId": "7376aaf4-45f4-4f7e-b4d2-60cbb88b4fa5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 225,
    "layers": [
        {
            "id": "7376aaf4-45f4-4f7e-b4d2-60cbb88b4fa5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc6f81db-ea13-46ce-9b37-875c430a9800",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 257,
    "xorig": 0,
    "yorig": 0
}