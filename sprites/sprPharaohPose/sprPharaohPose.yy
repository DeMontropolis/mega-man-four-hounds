{
    "id": "ad3d4825-eebb-4b32-8563-d187227bbc47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohPose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5dcec07a-7e96-4029-a006-7fb56226241a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3d4825-eebb-4b32-8563-d187227bbc47",
            "compositeImage": {
                "id": "e9e7c1f7-7aa6-49cb-917e-e1c141bfe1f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dcec07a-7e96-4029-a006-7fb56226241a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c27c45a-9888-4c2f-bd16-582e9402b051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dcec07a-7e96-4029-a006-7fb56226241a",
                    "LayerId": "9bdb6081-5f5c-4798-9309-2c4f5e3869e4"
                }
            ]
        },
        {
            "id": "3e93d566-f372-4a9e-84ba-91fe5a96eb76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad3d4825-eebb-4b32-8563-d187227bbc47",
            "compositeImage": {
                "id": "459ad723-5931-49c3-ac3f-ed86530a0768",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e93d566-f372-4a9e-84ba-91fe5a96eb76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50f1a87c-9404-4458-b041-b5f89e82c0a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e93d566-f372-4a9e-84ba-91fe5a96eb76",
                    "LayerId": "9bdb6081-5f5c-4798-9309-2c4f5e3869e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9bdb6081-5f5c-4798-9309-2c4f5e3869e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad3d4825-eebb-4b32-8563-d187227bbc47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 16,
    "yorig": 16
}