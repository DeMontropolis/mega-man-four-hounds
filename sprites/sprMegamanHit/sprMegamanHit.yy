{
    "id": "896601ea-338d-47a6-b393-16a51479a4db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanHit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "054b449e-cb7f-4cef-a8fa-186eb3d79033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "896601ea-338d-47a6-b393-16a51479a4db",
            "compositeImage": {
                "id": "be5fe5f1-c680-4d5a-b042-181356786b85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "054b449e-cb7f-4cef-a8fa-186eb3d79033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "134c0fb0-f8e1-4f2f-8be9-6be115e0277f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "054b449e-cb7f-4cef-a8fa-186eb3d79033",
                    "LayerId": "b2d92969-0bb4-4f43-ae0a-958d7f1a4809"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "b2d92969-0bb4-4f43-ae0a-958d7f1a4809",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "896601ea-338d-47a6-b393-16a51479a4db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 7
}