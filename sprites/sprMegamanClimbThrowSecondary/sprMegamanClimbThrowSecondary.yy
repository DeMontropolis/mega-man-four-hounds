{
    "id": "c304597d-24d5-45ec-a06d-271af9ab59ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbThrowSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e483219d-9888-42ff-94a4-6476707ec442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c304597d-24d5-45ec-a06d-271af9ab59ae",
            "compositeImage": {
                "id": "c6bd4b9b-932f-4455-baa2-3db335e697db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e483219d-9888-42ff-94a4-6476707ec442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "accb7d14-0dc1-4771-9012-89d0a7fb0ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e483219d-9888-42ff-94a4-6476707ec442",
                    "LayerId": "511e750e-3f4c-4737-a1a0-7aed755db2d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "511e750e-3f4c-4737-a1a0-7aed755db2d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c304597d-24d5-45ec-a06d-271af9ab59ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 8,
    "yorig": 7
}