{
    "id": "12acec0e-7ea6-4804-aca4-ce7218e91c65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 19,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7aa70fc-61de-4707-847d-f0dc2dd1bf7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12acec0e-7ea6-4804-aca4-ce7218e91c65",
            "compositeImage": {
                "id": "d59bf22e-08cb-40a1-9615-2c7ab6d90c02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7aa70fc-61de-4707-847d-f0dc2dd1bf7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9402812c-d604-4534-a648-600d5cbc6cbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7aa70fc-61de-4707-847d-f0dc2dd1bf7f",
                    "LayerId": "2cce4e4f-be81-40e0-87fe-bb573b4ed12f"
                }
            ]
        },
        {
            "id": "581964d1-96bc-41dc-a5ed-adfd2d96084f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12acec0e-7ea6-4804-aca4-ce7218e91c65",
            "compositeImage": {
                "id": "4db634f3-0f22-42b9-8600-8b3a00912d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "581964d1-96bc-41dc-a5ed-adfd2d96084f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bcd5653-8aaa-4b8a-bacc-7493eb6dbc36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "581964d1-96bc-41dc-a5ed-adfd2d96084f",
                    "LayerId": "2cce4e4f-be81-40e0-87fe-bb573b4ed12f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "2cce4e4f-be81-40e0-87fe-bb573b4ed12f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12acec0e-7ea6-4804-aca4-ce7218e91c65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 7
}