{
    "id": "724582ff-4f39-4753-b726-f2941476a218",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c468425d-cba9-4204-a973-329017da4533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "724582ff-4f39-4753-b726-f2941476a218",
            "compositeImage": {
                "id": "58af8538-2782-4cb4-96ce-c28548e06723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c468425d-cba9-4204-a973-329017da4533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f035d238-4d6c-4c1d-baa3-bce4f0899d0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c468425d-cba9-4204-a973-329017da4533",
                    "LayerId": "572d8f06-0a39-4561-a626-63fb00cf7b7d"
                }
            ]
        },
        {
            "id": "beebefb9-b945-4f87-9a99-ce2b031dac5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "724582ff-4f39-4753-b726-f2941476a218",
            "compositeImage": {
                "id": "e67e6dc3-681a-4335-914f-4cac562bc587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beebefb9-b945-4f87-9a99-ce2b031dac5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3709d843-decf-49de-bfb3-f96b82e4f5b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beebefb9-b945-4f87-9a99-ce2b031dac5c",
                    "LayerId": "572d8f06-0a39-4561-a626-63fb00cf7b7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "572d8f06-0a39-4561-a626-63fb00cf7b7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "724582ff-4f39-4753-b726-f2941476a218",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 7
}