{
    "id": "e68f0393-be6a-4e03-8cf1-27d90a9bfdd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPokerChip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77d2bfb5-f292-41a6-ba27-a314048957e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e68f0393-be6a-4e03-8cf1-27d90a9bfdd5",
            "compositeImage": {
                "id": "8cfbd67d-4b56-41ef-8cd7-8eea37ef001e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77d2bfb5-f292-41a6-ba27-a314048957e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a492697e-42d3-48d6-a60e-66715fc75aca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77d2bfb5-f292-41a6-ba27-a314048957e5",
                    "LayerId": "4d43e1e3-bfe4-4678-9bc6-09eb62b81c6d"
                }
            ]
        },
        {
            "id": "19ff5a03-3b98-4257-be31-5f21374726f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e68f0393-be6a-4e03-8cf1-27d90a9bfdd5",
            "compositeImage": {
                "id": "1923ee16-53cf-4178-81b4-3bd5baaee381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ff5a03-3b98-4257-be31-5f21374726f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42175b95-f4ed-4908-ac35-c8d63e3d9566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ff5a03-3b98-4257-be31-5f21374726f9",
                    "LayerId": "4d43e1e3-bfe4-4678-9bc6-09eb62b81c6d"
                }
            ]
        },
        {
            "id": "a660b927-798b-4917-be46-adfb12a83dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e68f0393-be6a-4e03-8cf1-27d90a9bfdd5",
            "compositeImage": {
                "id": "a5bc4f3a-1742-4d9a-802a-3e717de6d29d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a660b927-798b-4917-be46-adfb12a83dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243dd55e-96b8-48b2-9f4b-006c61a8ef68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a660b927-798b-4917-be46-adfb12a83dfc",
                    "LayerId": "4d43e1e3-bfe4-4678-9bc6-09eb62b81c6d"
                }
            ]
        },
        {
            "id": "dd2a908e-3f7d-44f9-abae-e33a9f96458a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e68f0393-be6a-4e03-8cf1-27d90a9bfdd5",
            "compositeImage": {
                "id": "b7f09973-135b-4527-92a7-5844e5f93633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd2a908e-3f7d-44f9-abae-e33a9f96458a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eedd80a2-8ed4-4661-b4da-c8cee8d69e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd2a908e-3f7d-44f9-abae-e33a9f96458a",
                    "LayerId": "4d43e1e3-bfe4-4678-9bc6-09eb62b81c6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4d43e1e3-bfe4-4678-9bc6-09eb62b81c6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e68f0393-be6a-4e03-8cf1-27d90a9bfdd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0.2,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}