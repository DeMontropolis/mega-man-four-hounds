{
    "id": "dea5eee8-10b3-430f-90cd-25beac3fd4aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLifeOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef111bd3-6eec-4c18-a68f-577252c79cbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea5eee8-10b3-430f-90cd-25beac3fd4aa",
            "compositeImage": {
                "id": "244e72ba-8190-4cbc-ae59-61f716f72562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef111bd3-6eec-4c18-a68f-577252c79cbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "607b24bd-c393-40fb-98f8-6a80a5f8e885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef111bd3-6eec-4c18-a68f-577252c79cbb",
                    "LayerId": "665f4c70-1873-4415-bfb7-49615fd0c0a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "665f4c70-1873-4415-bfb7-49615fd0c0a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dea5eee8-10b3-430f-90cd-25beac3fd4aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": -1
}