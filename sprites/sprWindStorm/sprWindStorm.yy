{
    "id": "b3139184-12ec-4e5a-82f9-014f355e7202",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWindStorm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cc60ee8-19b4-409b-8200-24d217c5643c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3139184-12ec-4e5a-82f9-014f355e7202",
            "compositeImage": {
                "id": "e421ecbb-f5cd-41df-82aa-8eab9433a037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc60ee8-19b4-409b-8200-24d217c5643c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1153ed-75c4-436c-bd86-2b11f55f4c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc60ee8-19b4-409b-8200-24d217c5643c",
                    "LayerId": "01f73f1c-8181-4cc5-aefa-47ffdce4b03b"
                }
            ]
        },
        {
            "id": "fd2e2bd2-41f4-449e-beb3-4ceb346aa894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3139184-12ec-4e5a-82f9-014f355e7202",
            "compositeImage": {
                "id": "ee302d92-f964-4232-97f5-d54d62402133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2e2bd2-41f4-449e-beb3-4ceb346aa894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "284c7278-1cb0-4380-805a-c408c7f0922e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2e2bd2-41f4-449e-beb3-4ceb346aa894",
                    "LayerId": "01f73f1c-8181-4cc5-aefa-47ffdce4b03b"
                }
            ]
        },
        {
            "id": "5707beba-13df-497d-9c76-00fe694351bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3139184-12ec-4e5a-82f9-014f355e7202",
            "compositeImage": {
                "id": "5e2d7914-cf3a-4e18-88b2-cd14841df1bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5707beba-13df-497d-9c76-00fe694351bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1864506e-8d76-4ff1-a20f-8142a1d94e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5707beba-13df-497d-9c76-00fe694351bf",
                    "LayerId": "01f73f1c-8181-4cc5-aefa-47ffdce4b03b"
                }
            ]
        },
        {
            "id": "09080837-a1ca-4c2b-bdfc-fb1683674aa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3139184-12ec-4e5a-82f9-014f355e7202",
            "compositeImage": {
                "id": "117d4ada-8b9b-4ab3-bacb-b7a30de19f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09080837-a1ca-4c2b-bdfc-fb1683674aa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4810e297-8158-4991-bb64-46f86a31b8a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09080837-a1ca-4c2b-bdfc-fb1683674aa7",
                    "LayerId": "01f73f1c-8181-4cc5-aefa-47ffdce4b03b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "01f73f1c-8181-4cc5-aefa-47ffdce4b03b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3139184-12ec-4e5a-82f9-014f355e7202",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 8
}