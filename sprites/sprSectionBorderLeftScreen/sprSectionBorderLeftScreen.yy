{
    "id": "2adaa9d0-af90-4733-99fd-87967d243c34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionBorderLeftScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d8a1c4b-ecaf-4e84-baaa-5b8c4b3cdfa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2adaa9d0-af90-4733-99fd-87967d243c34",
            "compositeImage": {
                "id": "6401d2e3-f192-480a-87c5-381c73b6e070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8a1c4b-ecaf-4e84-baaa-5b8c4b3cdfa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee7503a8-1334-4cfd-8945-9e6e95b66902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8a1c4b-ecaf-4e84-baaa-5b8c4b3cdfa3",
                    "LayerId": "8f3ad154-99fb-4c47-9dbb-190ed80d27e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "8f3ad154-99fb-4c47-9dbb-190ed80d27e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2adaa9d0-af90-4733-99fd-87967d243c34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}