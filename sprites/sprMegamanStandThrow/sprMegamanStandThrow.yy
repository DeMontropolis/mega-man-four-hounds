{
    "id": "407a9485-ac19-48b8-82dd-5012ef137511",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandThrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fec9e0de-c4c6-42a9-9494-6339fc7a474f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407a9485-ac19-48b8-82dd-5012ef137511",
            "compositeImage": {
                "id": "9b926255-51e1-42b0-adca-e7be723fbb8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fec9e0de-c4c6-42a9-9494-6339fc7a474f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb6763b-708b-4c33-b3e1-0fa409a1e665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fec9e0de-c4c6-42a9-9494-6339fc7a474f",
                    "LayerId": "858663f3-4f03-459e-a79d-ee7b7052e02c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "858663f3-4f03-459e-a79d-ee7b7052e02c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "407a9485-ac19-48b8-82dd-5012ef137511",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 10,
    "yorig": 7
}