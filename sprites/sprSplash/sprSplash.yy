{
    "id": "7caf42a7-cb6a-4d98-aee4-51e66f41e375",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSplash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e26821b-cb55-4af6-a48f-2619a0ea085b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7caf42a7-cb6a-4d98-aee4-51e66f41e375",
            "compositeImage": {
                "id": "b6e49471-fc05-4363-ba2c-36c525cb921a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e26821b-cb55-4af6-a48f-2619a0ea085b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8580f5ee-67cc-4fb2-8209-fd79c9aadefd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e26821b-cb55-4af6-a48f-2619a0ea085b",
                    "LayerId": "2e91700f-4fb1-4c58-b4a6-e62e8dc003ee"
                }
            ]
        },
        {
            "id": "f199cf78-7331-4e07-b649-ad7bf2383ae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7caf42a7-cb6a-4d98-aee4-51e66f41e375",
            "compositeImage": {
                "id": "1561e40f-b6a0-4b73-8ce9-d5f667fd8e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f199cf78-7331-4e07-b649-ad7bf2383ae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95853a0b-ef3d-4ea2-b679-38b6ff6fc3f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f199cf78-7331-4e07-b649-ad7bf2383ae1",
                    "LayerId": "2e91700f-4fb1-4c58-b4a6-e62e8dc003ee"
                }
            ]
        },
        {
            "id": "6f8dbaeb-4482-4a56-b2df-a59bfb710f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7caf42a7-cb6a-4d98-aee4-51e66f41e375",
            "compositeImage": {
                "id": "864cdcfd-dc0e-4a08-b3f2-69150b9e41ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8dbaeb-4482-4a56-b2df-a59bfb710f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02fdc64d-d97a-4ded-8b78-a89004317704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8dbaeb-4482-4a56-b2df-a59bfb710f5d",
                    "LayerId": "2e91700f-4fb1-4c58-b4a6-e62e8dc003ee"
                }
            ]
        },
        {
            "id": "71b8ed92-7084-4460-899a-fea6ec76403b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7caf42a7-cb6a-4d98-aee4-51e66f41e375",
            "compositeImage": {
                "id": "33d1e9aa-0aff-4874-8025-82a522ce0b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b8ed92-7084-4460-899a-fea6ec76403b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d926599-c645-4ae4-9a23-41f34973a513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b8ed92-7084-4460-899a-fea6ec76403b",
                    "LayerId": "2e91700f-4fb1-4c58-b4a6-e62e8dc003ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2e91700f-4fb1-4c58-b4a6-e62e8dc003ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7caf42a7-cb6a-4d98-aee4-51e66f41e375",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}