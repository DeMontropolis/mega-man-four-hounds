{
    "id": "89b1a241-eea0-48e3-bb3b-efde8b59c94a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11edc988-aeaf-4e26-8dba-2348de62d443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89b1a241-eea0-48e3-bb3b-efde8b59c94a",
            "compositeImage": {
                "id": "b3430ad1-b5dc-4822-91d4-8b20ae1cdcd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11edc988-aeaf-4e26-8dba-2348de62d443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad3d80c-fc4f-4089-90b4-f1c075da6d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11edc988-aeaf-4e26-8dba-2348de62d443",
                    "LayerId": "d0d9a499-8827-49a8-bd27-b4a190956b44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "d0d9a499-8827-49a8-bd27-b4a190956b44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89b1a241-eea0-48e3-bb3b-efde8b59c94a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 12,
    "yorig": 7
}