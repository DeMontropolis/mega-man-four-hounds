{
    "id": "f32d687b-9c50-41f6-beaa-34e00adb1ed7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSlideDust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9c39a7d-1b65-45e7-a1a4-5fb8b4dcfe9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f32d687b-9c50-41f6-beaa-34e00adb1ed7",
            "compositeImage": {
                "id": "d32acb92-1671-430d-82ab-29aae8e58eb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9c39a7d-1b65-45e7-a1a4-5fb8b4dcfe9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9705da06-2d70-4995-9af4-c776afb63e00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9c39a7d-1b65-45e7-a1a4-5fb8b4dcfe9e",
                    "LayerId": "b59276f1-fbd6-469e-8f3b-f5a5be23daf1"
                }
            ]
        },
        {
            "id": "f0708684-b2d8-4285-b678-283e1a8a73b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f32d687b-9c50-41f6-beaa-34e00adb1ed7",
            "compositeImage": {
                "id": "df1d4fd7-57a7-40fc-917d-3a88c18b6988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0708684-b2d8-4285-b678-283e1a8a73b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35450e0d-9c77-44cb-af46-b4bbd8a00798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0708684-b2d8-4285-b678-283e1a8a73b7",
                    "LayerId": "b59276f1-fbd6-469e-8f3b-f5a5be23daf1"
                }
            ]
        },
        {
            "id": "bff09ced-98b1-4308-9495-6bfcb141dfca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f32d687b-9c50-41f6-beaa-34e00adb1ed7",
            "compositeImage": {
                "id": "913475de-2ceb-424a-9e50-f7fbd67e521f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bff09ced-98b1-4308-9495-6bfcb141dfca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f981d8a-6889-4408-a766-12fe0a17f4a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bff09ced-98b1-4308-9495-6bfcb141dfca",
                    "LayerId": "b59276f1-fbd6-469e-8f3b-f5a5be23daf1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b59276f1-fbd6-469e-8f3b-f5a5be23daf1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f32d687b-9c50-41f6-beaa-34e00adb1ed7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}