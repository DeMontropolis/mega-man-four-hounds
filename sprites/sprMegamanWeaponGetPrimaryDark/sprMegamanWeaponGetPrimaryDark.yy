{
    "id": "9f68a6b6-01c1-4b3d-a7aa-10e64f55bfbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWeaponGetPrimaryDark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 7,
    "bbox_right": 54,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a7c3102-8580-4ba4-8b69-1846f99136a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f68a6b6-01c1-4b3d-a7aa-10e64f55bfbd",
            "compositeImage": {
                "id": "58fc3c06-2c51-404a-9cfa-82dcdc6171bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a7c3102-8580-4ba4-8b69-1846f99136a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8116273b-41cb-4e0a-b02c-192afeea5ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a7c3102-8580-4ba4-8b69-1846f99136a6",
                    "LayerId": "276bc0d9-118d-463e-9251-cbd3a61da4e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 93,
    "layers": [
        {
            "id": "276bc0d9-118d-463e-9251-cbd3a61da4e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f68a6b6-01c1-4b3d-a7aa-10e64f55bfbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 46
}