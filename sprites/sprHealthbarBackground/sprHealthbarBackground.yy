{
    "id": "9f5c6331-43b2-4f2f-bb25-1c52d05f2197",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHealthbarBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c87d1ad-0dab-4342-a082-9761c83f89d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5c6331-43b2-4f2f-bb25-1c52d05f2197",
            "compositeImage": {
                "id": "60f4e6c9-3abc-49b3-908e-69123b72704c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c87d1ad-0dab-4342-a082-9761c83f89d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59a5b1e1-be1d-45d5-9f82-8ca3309c8e60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c87d1ad-0dab-4342-a082-9761c83f89d5",
                    "LayerId": "29a72e68-909a-479c-9d72-193ea7266786"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "29a72e68-909a-479c-9d72-193ea7266786",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f5c6331-43b2-4f2f-bb25-1c52d05f2197",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}