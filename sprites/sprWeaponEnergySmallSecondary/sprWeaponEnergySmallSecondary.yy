{
    "id": "4ed209ce-e5aa-48d7-ae40-a3f11f98d6f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponEnergySmallSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66873cf9-fd3d-47cc-bc75-40540eac871d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ed209ce-e5aa-48d7-ae40-a3f11f98d6f2",
            "compositeImage": {
                "id": "be37cf9f-d5e7-45b9-ae54-e94bbd411744",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66873cf9-fd3d-47cc-bc75-40540eac871d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a19b9b9e-85ba-4a7d-900f-2528055d013e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66873cf9-fd3d-47cc-bc75-40540eac871d",
                    "LayerId": "ee3170be-f2a1-4605-b81b-56712bfb7be8"
                }
            ]
        },
        {
            "id": "887d15b1-c1d2-4963-a419-9105abe5e255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ed209ce-e5aa-48d7-ae40-a3f11f98d6f2",
            "compositeImage": {
                "id": "a1103ade-7c4b-4026-9659-d502a1efd941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "887d15b1-c1d2-4963-a419-9105abe5e255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b96e07a-798f-4b35-84f2-72ff087393f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "887d15b1-c1d2-4963-a419-9105abe5e255",
                    "LayerId": "ee3170be-f2a1-4605-b81b-56712bfb7be8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ee3170be-f2a1-4605-b81b-56712bfb7be8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ed209ce-e5aa-48d7-ae40-a3f11f98d6f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": -4,
    "yorig": -8
}