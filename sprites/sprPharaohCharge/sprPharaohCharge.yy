{
    "id": "1fa7d5cc-67e9-4f24-a406-c74e565871c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohCharge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e41f610-6895-4bff-9fe5-3f1969955323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fa7d5cc-67e9-4f24-a406-c74e565871c1",
            "compositeImage": {
                "id": "fbb4dcdf-7c2b-48e8-87fa-3d1423ec9e61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e41f610-6895-4bff-9fe5-3f1969955323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd971966-4770-4506-ab1e-e90245e3ff61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e41f610-6895-4bff-9fe5-3f1969955323",
                    "LayerId": "c563e394-ea74-4f59-99d2-b7ede43e7a0e"
                }
            ]
        },
        {
            "id": "de8f37db-343c-49fe-8add-51a4b115781c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fa7d5cc-67e9-4f24-a406-c74e565871c1",
            "compositeImage": {
                "id": "2bfd8dce-7fc4-4d7a-81fe-ae6f3a81c8d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de8f37db-343c-49fe-8add-51a4b115781c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ece13be-2a59-4da3-9016-5d676f3a85ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de8f37db-343c-49fe-8add-51a4b115781c",
                    "LayerId": "c563e394-ea74-4f59-99d2-b7ede43e7a0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "c563e394-ea74-4f59-99d2-b7ede43e7a0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fa7d5cc-67e9-4f24-a406-c74e565871c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 17,
    "yorig": 22
}