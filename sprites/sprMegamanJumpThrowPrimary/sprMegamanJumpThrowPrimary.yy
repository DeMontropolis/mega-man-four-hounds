{
    "id": "9a70fe95-d3ca-4d6b-bdd5-4442aa23cd87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpThrowPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 26,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75eed2cc-46a9-4eb2-be40-b79cbbc729ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a70fe95-d3ca-4d6b-bdd5-4442aa23cd87",
            "compositeImage": {
                "id": "5e34677e-d505-41e0-a9a7-2400440e58d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75eed2cc-46a9-4eb2-be40-b79cbbc729ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ec3b08e-4155-4c49-92d1-ad263ed330f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75eed2cc-46a9-4eb2-be40-b79cbbc729ef",
                    "LayerId": "943967e9-febf-4e53-beba-6bcad0b5b121"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "943967e9-febf-4e53-beba-6bcad0b5b121",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a70fe95-d3ca-4d6b-bdd5-4442aa23cd87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 12,
    "yorig": 7
}