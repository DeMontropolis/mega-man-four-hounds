{
    "id": "c4da5f2e-c203-4262-8d7b-dc7ca4eb4ab2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohManShotBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5be6009f-2d83-4898-9185-6c63c45805dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4da5f2e-c203-4262-8d7b-dc7ca4eb4ab2",
            "compositeImage": {
                "id": "06867828-7c3e-4312-b8b1-3b199fbd8ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be6009f-2d83-4898-9185-6c63c45805dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bce39f0c-0a40-420f-b1b1-13b3976ab069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be6009f-2d83-4898-9185-6c63c45805dc",
                    "LayerId": "e2e565f0-abe4-429c-bd94-92f08c265af8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e2e565f0-abe4-429c-bd94-92f08c265af8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4da5f2e-c203-4262-8d7b-dc7ca4eb4ab2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 16
}