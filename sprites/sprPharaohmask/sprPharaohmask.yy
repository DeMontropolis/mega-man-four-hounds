{
    "id": "b2a7497c-2c87-4f22-9086-cbfa72e1714b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohmask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 4,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c48087d-81a7-474e-b229-e6b74c6aa470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2a7497c-2c87-4f22-9086-cbfa72e1714b",
            "compositeImage": {
                "id": "6ea314ce-652a-43a8-9509-3b2a57d17302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c48087d-81a7-474e-b229-e6b74c6aa470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "949f0e99-fe44-45d7-93dc-73ac05ff5cb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c48087d-81a7-474e-b229-e6b74c6aa470",
                    "LayerId": "96cd60d3-57fd-402f-b767-f8d3652fc0ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "96cd60d3-57fd-402f-b767-f8d3652fc0ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2a7497c-2c87-4f22-9086-cbfa72e1714b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 13
}