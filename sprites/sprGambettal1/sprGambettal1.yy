{
    "id": "eeb03595-fd3b-477f-8945-2f60906fc8d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGambettal1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f86109b-199a-4e11-8914-9d9bd57b8135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eeb03595-fd3b-477f-8945-2f60906fc8d6",
            "compositeImage": {
                "id": "2b09086e-dab2-4ced-9d4a-aff73ec4628b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f86109b-199a-4e11-8914-9d9bd57b8135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be45bc92-60a3-4332-8cb5-422d189dcdd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f86109b-199a-4e11-8914-9d9bd57b8135",
                    "LayerId": "5f26c7c3-815c-4803-af23-b604b9862eaf"
                }
            ]
        },
        {
            "id": "3b491626-b102-4d0c-b9ce-45c58824ac4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eeb03595-fd3b-477f-8945-2f60906fc8d6",
            "compositeImage": {
                "id": "2110a308-e39c-4b69-83dd-74d93b9ee7f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b491626-b102-4d0c-b9ce-45c58824ac4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cb39401-cf79-434f-a63f-b630dcb8fb2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b491626-b102-4d0c-b9ce-45c58824ac4b",
                    "LayerId": "5f26c7c3-815c-4803-af23-b604b9862eaf"
                }
            ]
        },
        {
            "id": "b80a6104-0733-4529-9af4-81484dd91955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eeb03595-fd3b-477f-8945-2f60906fc8d6",
            "compositeImage": {
                "id": "9ebb92d3-c5f5-47e4-9484-8b343ba4ae5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b80a6104-0733-4529-9af4-81484dd91955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edbc652b-529e-43df-bcce-50afa012d879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80a6104-0733-4529-9af4-81484dd91955",
                    "LayerId": "5f26c7c3-815c-4803-af23-b604b9862eaf"
                }
            ]
        },
        {
            "id": "00994687-5a75-4822-a96c-8a54f1aeeb15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eeb03595-fd3b-477f-8945-2f60906fc8d6",
            "compositeImage": {
                "id": "1d586958-54ef-42fa-8b9f-7cbc5042c904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00994687-5a75-4822-a96c-8a54f1aeeb15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bcdb530-6da1-4eb4-a27e-aa7ef7e21022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00994687-5a75-4822-a96c-8a54f1aeeb15",
                    "LayerId": "5f26c7c3-815c-4803-af23-b604b9862eaf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "5f26c7c3-815c-4803-af23-b604b9862eaf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eeb03595-fd3b-477f-8945-2f60906fc8d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 8,
    "yorig": 11
}