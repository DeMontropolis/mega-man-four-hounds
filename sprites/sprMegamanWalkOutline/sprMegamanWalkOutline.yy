{
    "id": "f422c2a9-eaff-4d25-8f45-1f77e9bca48f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6efdcab-89ba-4f90-91d0-2eb6c1d31139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f422c2a9-eaff-4d25-8f45-1f77e9bca48f",
            "compositeImage": {
                "id": "4fb22e14-1e76-4834-b166-9478e3e5ed58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6efdcab-89ba-4f90-91d0-2eb6c1d31139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e488023f-30ac-4336-90f0-5ab3bb8adfc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6efdcab-89ba-4f90-91d0-2eb6c1d31139",
                    "LayerId": "80fd4ea6-7213-471e-bc14-a5a1d54a6166"
                }
            ]
        },
        {
            "id": "698fcdae-5ddc-444e-9286-34d35912fcdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f422c2a9-eaff-4d25-8f45-1f77e9bca48f",
            "compositeImage": {
                "id": "32279edd-9d7f-444b-8893-1d8d85821c01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698fcdae-5ddc-444e-9286-34d35912fcdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72eb454d-4f34-41b7-a473-bbb1201bd9ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698fcdae-5ddc-444e-9286-34d35912fcdb",
                    "LayerId": "80fd4ea6-7213-471e-bc14-a5a1d54a6166"
                }
            ]
        },
        {
            "id": "f8411c13-ad7c-4d6d-af61-3d8c81a82323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f422c2a9-eaff-4d25-8f45-1f77e9bca48f",
            "compositeImage": {
                "id": "84e59880-f5f3-4c66-b35b-34da04e7182b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8411c13-ad7c-4d6d-af61-3d8c81a82323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "054597e8-4937-4bc4-be67-25d33f053ed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8411c13-ad7c-4d6d-af61-3d8c81a82323",
                    "LayerId": "80fd4ea6-7213-471e-bc14-a5a1d54a6166"
                }
            ]
        },
        {
            "id": "b3e55265-6c2b-4d35-bf9b-cf3fd5371553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f422c2a9-eaff-4d25-8f45-1f77e9bca48f",
            "compositeImage": {
                "id": "7070cf5d-840c-4a2c-8179-f05f0362deb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3e55265-6c2b-4d35-bf9b-cf3fd5371553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ae651e7-6e12-4d73-ac70-db92b1034183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3e55265-6c2b-4d35-bf9b-cf3fd5371553",
                    "LayerId": "80fd4ea6-7213-471e-bc14-a5a1d54a6166"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "80fd4ea6-7213-471e-bc14-a5a1d54a6166",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f422c2a9-eaff-4d25-8f45-1f77e9bca48f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 11,
    "yorig": 7
}