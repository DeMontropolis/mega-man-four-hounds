{
    "id": "839f7485-334b-468b-aa2e-396084a9f86d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgStageSelect",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 224,
    "bbox_left": 0,
    "bbox_right": 256,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dfc1dac-85e9-4105-8587-09ea58730e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "839f7485-334b-468b-aa2e-396084a9f86d",
            "compositeImage": {
                "id": "dd9c9d6c-7010-4ac0-862a-878782edda61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dfc1dac-85e9-4105-8587-09ea58730e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa18bef1-8c37-43a3-b3d1-39a71b5f567b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dfc1dac-85e9-4105-8587-09ea58730e81",
                    "LayerId": "207b0d5e-4782-43a2-b031-ba021acbf2d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 225,
    "layers": [
        {
            "id": "207b0d5e-4782-43a2-b031-ba021acbf2d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "839f7485-334b-468b-aa2e-396084a9f86d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 257,
    "xorig": 0,
    "yorig": 0
}