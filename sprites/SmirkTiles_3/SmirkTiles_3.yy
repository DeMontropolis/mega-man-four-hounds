{
    "id": "75deb215-3397-43a2-b195-88c5551822db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SmirkTiles_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "848df041-6373-477e-8cb4-fc033c411097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75deb215-3397-43a2-b195-88c5551822db",
            "compositeImage": {
                "id": "ce84f475-04c4-4374-8f76-e45151a89f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "848df041-6373-477e-8cb4-fc033c411097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f8fda55-eeff-4441-811b-cab210a3bdaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "848df041-6373-477e-8cb4-fc033c411097",
                    "LayerId": "118053b9-a8cf-4b44-91b9-bdcd5ef41ee8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "118053b9-a8cf-4b44-91b9-bdcd5ef41ee8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75deb215-3397-43a2-b195-88c5551822db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}