{
    "id": "61a23cbd-13f2-4b0f-99f0-7180ecbfab69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite201",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f740e1ef-382f-4b91-b00d-cf0cf42ed38a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a23cbd-13f2-4b0f-99f0-7180ecbfab69",
            "compositeImage": {
                "id": "9bb5533d-63c9-4fee-8f75-e4146628e506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f740e1ef-382f-4b91-b00d-cf0cf42ed38a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049186d4-b262-49b6-bd4c-a7aaa055b872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f740e1ef-382f-4b91-b00d-cf0cf42ed38a",
                    "LayerId": "27da874b-16b2-44bf-bee8-e3d89dedd528"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "27da874b-16b2-44bf-bee8-e3d89dedd528",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61a23cbd-13f2-4b0f-99f0-7180ecbfab69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}