{
    "id": "6a0bf1d6-73a5-4782-aa46-767986b36d58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mskMegaman",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 16,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a634abef-9be7-4b20-acd1-fa3e96868913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a0bf1d6-73a5-4782-aa46-767986b36d58",
            "compositeImage": {
                "id": "4ac8c893-d84a-46e5-a6aa-c2db85b748ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a634abef-9be7-4b20-acd1-fa3e96868913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6263319e-7ec6-4750-967b-13bc9d9e166a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a634abef-9be7-4b20-acd1-fa3e96868913",
                    "LayerId": "a07380e9-6872-479a-baaf-22c596629889"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a07380e9-6872-479a-baaf-22c596629889",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a0bf1d6-73a5-4782-aa46-767986b36d58",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 0,
    "yorig": 0
}