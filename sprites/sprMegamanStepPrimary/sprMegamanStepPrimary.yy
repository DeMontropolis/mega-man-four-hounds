{
    "id": "6abbcf99-467a-47d6-9b8b-ad4d2ba99993",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStepPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 18,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9eedeaf4-2fc8-41d9-984b-6750d65d897e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6abbcf99-467a-47d6-9b8b-ad4d2ba99993",
            "compositeImage": {
                "id": "605376be-b661-4320-8f3e-5fca0ac178e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eedeaf4-2fc8-41d9-984b-6750d65d897e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a01f52-6d69-4be4-9a35-70ebbd887ca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eedeaf4-2fc8-41d9-984b-6750d65d897e",
                    "LayerId": "6dd43538-e999-4976-9174-d0a65b7cce93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6dd43538-e999-4976-9174-d0a65b7cce93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6abbcf99-467a-47d6-9b8b-ad4d2ba99993",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 7
}