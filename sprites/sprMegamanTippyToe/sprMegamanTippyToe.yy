{
    "id": "82f4160a-dffe-4a2a-9f14-d6729b6843e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanTippyToe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "582075f1-8169-492f-aaf3-7f56d52bbec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82f4160a-dffe-4a2a-9f14-d6729b6843e3",
            "compositeImage": {
                "id": "2a235076-4493-4a2d-a209-3987a85ba5b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582075f1-8169-492f-aaf3-7f56d52bbec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7969ab3-e91a-4a38-84c4-6f29d0101d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582075f1-8169-492f-aaf3-7f56d52bbec0",
                    "LayerId": "ba55a379-76d9-42ca-9ff5-57e61bc0283e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ba55a379-76d9-42ca-9ff5-57e61bc0283e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82f4160a-dffe-4a2a-9f14-d6729b6843e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 7
}