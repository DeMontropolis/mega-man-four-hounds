{
    "id": "ffdd260f-e6d1-4e53-ab4f-7fdd464dcb35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SmirkTiles_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 82,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "613f4c47-a203-4122-9fe6-9c06e376364b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffdd260f-e6d1-4e53-ab4f-7fdd464dcb35",
            "compositeImage": {
                "id": "da106d54-0385-4e8d-859b-aa6b5fbb1309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "613f4c47-a203-4122-9fe6-9c06e376364b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5339dd84-e482-45f8-b0d0-193350fd6466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "613f4c47-a203-4122-9fe6-9c06e376364b",
                    "LayerId": "076fd694-ca7f-4678-90d2-99378e3c6577"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "076fd694-ca7f-4678-90d2-99378e3c6577",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffdd260f-e6d1-4e53-ab4f-7fdd464dcb35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 0,
    "yorig": 0
}