{
    "id": "8e2d37ef-72ec-4303-a0cb-fc48684f6a4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGambettal6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56f81a1a-da4c-47ec-92be-10cf2d4a4cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e2d37ef-72ec-4303-a0cb-fc48684f6a4d",
            "compositeImage": {
                "id": "d85327a8-949d-4417-966d-86bd10594f85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f81a1a-da4c-47ec-92be-10cf2d4a4cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf4448d-8d92-4e9b-a3ec-0403fe57e544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f81a1a-da4c-47ec-92be-10cf2d4a4cd1",
                    "LayerId": "c34b8845-26a0-44b9-a223-bdf5cc9e85f5"
                }
            ]
        },
        {
            "id": "ba1c342f-0efe-4330-9a8e-c5342e896a33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e2d37ef-72ec-4303-a0cb-fc48684f6a4d",
            "compositeImage": {
                "id": "5b140205-76d2-4233-875e-37510e9b9c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba1c342f-0efe-4330-9a8e-c5342e896a33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17c4e2b4-8817-4574-981d-33ed5479c074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba1c342f-0efe-4330-9a8e-c5342e896a33",
                    "LayerId": "c34b8845-26a0-44b9-a223-bdf5cc9e85f5"
                }
            ]
        },
        {
            "id": "e0ddaac1-a432-49e4-a6a7-f251e3c8617c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e2d37ef-72ec-4303-a0cb-fc48684f6a4d",
            "compositeImage": {
                "id": "5560145c-55a9-424b-a604-6145a9808574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ddaac1-a432-49e4-a6a7-f251e3c8617c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d81fa20b-0dc9-42ac-a1b4-462b5cfa7987",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ddaac1-a432-49e4-a6a7-f251e3c8617c",
                    "LayerId": "c34b8845-26a0-44b9-a223-bdf5cc9e85f5"
                }
            ]
        },
        {
            "id": "51918b93-9e19-4803-b8c9-125440307858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e2d37ef-72ec-4303-a0cb-fc48684f6a4d",
            "compositeImage": {
                "id": "0b3b105b-2b90-4d81-a6fd-63234fa4b3b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51918b93-9e19-4803-b8c9-125440307858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e780a41e-6026-43f4-a39b-ebe7676c76aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51918b93-9e19-4803-b8c9-125440307858",
                    "LayerId": "c34b8845-26a0-44b9-a223-bdf5cc9e85f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "c34b8845-26a0-44b9-a223-bdf5cc9e85f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e2d37ef-72ec-4303-a0cb-fc48684f6a4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 8,
    "yorig": 11
}