{
    "id": "c3a18cf9-6138-4489-b9e5-41db0f9361aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpShootPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11507430-ecfa-4d50-967d-c6094458c66f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3a18cf9-6138-4489-b9e5-41db0f9361aa",
            "compositeImage": {
                "id": "00a8b3fc-410c-4730-92ea-c03defd12244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11507430-ecfa-4d50-967d-c6094458c66f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffe8d58e-c52f-4ba5-9938-e71fc880fa76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11507430-ecfa-4d50-967d-c6094458c66f",
                    "LayerId": "99258952-eaec-4e70-b1bc-5ba7dd1af76f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "99258952-eaec-4e70-b1bc-5ba7dd1af76f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3a18cf9-6138-4489-b9e5-41db0f9361aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 12,
    "yorig": 7
}