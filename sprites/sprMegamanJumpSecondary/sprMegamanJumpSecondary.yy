{
    "id": "e7c3589a-0853-476a-b8d4-be36428e79fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 6,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdaa0387-42a3-42d3-9c31-e7fc5976b91f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7c3589a-0853-476a-b8d4-be36428e79fe",
            "compositeImage": {
                "id": "a62fed27-9767-457b-ac64-e9c8d373385a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdaa0387-42a3-42d3-9c31-e7fc5976b91f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57614681-fc06-4472-990c-a175dabd7673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdaa0387-42a3-42d3-9c31-e7fc5976b91f",
                    "LayerId": "49201eb4-fede-4e24-8199-267897e4fc45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "49201eb4-fede-4e24-8199-267897e4fc45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7c3589a-0853-476a-b8d4-be36428e79fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 12,
    "yorig": 7
}