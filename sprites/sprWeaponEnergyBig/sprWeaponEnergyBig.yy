{
    "id": "e01f50b8-3ccc-4fff-b20c-5f993bd3cbb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponEnergyBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3cf4745-c50a-4c3a-83db-f270f1f067c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e01f50b8-3ccc-4fff-b20c-5f993bd3cbb5",
            "compositeImage": {
                "id": "130d1ca9-99f1-4011-a231-5442e8aefc16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3cf4745-c50a-4c3a-83db-f270f1f067c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17cd882e-0f86-4cf4-8656-1c5c2f246779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3cf4745-c50a-4c3a-83db-f270f1f067c9",
                    "LayerId": "996e3103-1c36-4c88-8733-fc5109cef49d"
                }
            ]
        },
        {
            "id": "598ac3b1-8bc3-46ce-bfc1-f20c500591f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e01f50b8-3ccc-4fff-b20c-5f993bd3cbb5",
            "compositeImage": {
                "id": "aa32a66a-be5b-49e1-8d5f-d75e9ace7d1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "598ac3b1-8bc3-46ce-bfc1-f20c500591f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1ad6e02-7369-4069-8dd1-b5d7c24835ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "598ac3b1-8bc3-46ce-bfc1-f20c500591f5",
                    "LayerId": "996e3103-1c36-4c88-8733-fc5109cef49d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "996e3103-1c36-4c88-8733-fc5109cef49d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e01f50b8-3ccc-4fff-b20c-5f993bd3cbb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": -4
}