{
    "id": "88364bbf-2585-49cd-a7d0-e059ad17f9aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprETank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dc04189-0ff0-47b6-8e10-3d09354ccfd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88364bbf-2585-49cd-a7d0-e059ad17f9aa",
            "compositeImage": {
                "id": "9d08b3cf-4012-446a-96f5-9553ef497eb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc04189-0ff0-47b6-8e10-3d09354ccfd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc04de3-3387-49f9-9210-bcd3b03a43d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc04189-0ff0-47b6-8e10-3d09354ccfd4",
                    "LayerId": "1b21c8d7-eff0-4d09-b1e0-18391bd50e4b"
                }
            ]
        },
        {
            "id": "102da0cc-3f44-48df-810a-c7b30addd30f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88364bbf-2585-49cd-a7d0-e059ad17f9aa",
            "compositeImage": {
                "id": "17f7e6e1-4a4c-4274-a34a-0d3512016782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "102da0cc-3f44-48df-810a-c7b30addd30f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5fcff2-d121-4f87-af75-a8d9f127f8be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "102da0cc-3f44-48df-810a-c7b30addd30f",
                    "LayerId": "1b21c8d7-eff0-4d09-b1e0-18391bd50e4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1b21c8d7-eff0-4d09-b1e0-18391bd50e4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88364bbf-2585-49cd-a7d0-e059ad17f9aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}