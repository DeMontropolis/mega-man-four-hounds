{
    "id": "b16b9adf-5713-4f96-947a-2aab7e8c3f8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponEnergyBigOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8755e286-f6be-4883-93eb-6c1d24691f64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b16b9adf-5713-4f96-947a-2aab7e8c3f8d",
            "compositeImage": {
                "id": "baf4248b-f3c4-4bc8-9409-621a708e2745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8755e286-f6be-4883-93eb-6c1d24691f64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e8e033d-8204-4e44-9f9e-d915934a8d6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8755e286-f6be-4883-93eb-6c1d24691f64",
                    "LayerId": "64c19ac2-90d7-4774-b1e2-d4f900bcf685"
                }
            ]
        },
        {
            "id": "2f7e7db9-2d8f-444b-b269-884bbe2f5d7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b16b9adf-5713-4f96-947a-2aab7e8c3f8d",
            "compositeImage": {
                "id": "1f77e306-3565-4569-a2fc-8639d14bf055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7e7db9-2d8f-444b-b269-884bbe2f5d7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c4d29c5-ffe7-4dde-ad51-8d44da2d3946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7e7db9-2d8f-444b-b269-884bbe2f5d7a",
                    "LayerId": "64c19ac2-90d7-4774-b1e2-d4f900bcf685"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "64c19ac2-90d7-4774-b1e2-d4f900bcf685",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b16b9adf-5713-4f96-947a-2aab7e8c3f8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": -4
}