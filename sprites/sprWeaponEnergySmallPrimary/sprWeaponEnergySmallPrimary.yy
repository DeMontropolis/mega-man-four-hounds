{
    "id": "8e83ca82-845d-46bb-ad24-56ca558f4774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponEnergySmallPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa6e34c4-0334-403e-b63a-2508f6145249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e83ca82-845d-46bb-ad24-56ca558f4774",
            "compositeImage": {
                "id": "6916600e-ef2f-4e5f-82a1-2048019af116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa6e34c4-0334-403e-b63a-2508f6145249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed0cdb7-dce0-46e1-8cd9-dcbf5a85f621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa6e34c4-0334-403e-b63a-2508f6145249",
                    "LayerId": "67244e95-d237-4cac-a80c-ae7eb75a8131"
                }
            ]
        },
        {
            "id": "11d50d26-762b-433e-b327-52d9e41d1db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e83ca82-845d-46bb-ad24-56ca558f4774",
            "compositeImage": {
                "id": "427fe1be-7274-4f23-8360-d9ab1a785daa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11d50d26-762b-433e-b327-52d9e41d1db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a3c6df-1fcb-4bc6-962f-8f4938490141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11d50d26-762b-433e-b327-52d9e41d1db6",
                    "LayerId": "67244e95-d237-4cac-a80c-ae7eb75a8131"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "67244e95-d237-4cac-a80c-ae7eb75a8131",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e83ca82-845d-46bb-ad24-56ca558f4774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": -4,
    "yorig": -8
}