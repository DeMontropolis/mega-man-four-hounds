{
    "id": "56e5dd73-006a-4a16-a235-52197bb3aa64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandShootPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e188311-8b48-4382-a8e4-f1228ef2353e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e5dd73-006a-4a16-a235-52197bb3aa64",
            "compositeImage": {
                "id": "0d490eac-d407-499f-968f-7cf4a1d6e9e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e188311-8b48-4382-a8e4-f1228ef2353e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc3974fa-bb74-449d-ac46-14953c8a965e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e188311-8b48-4382-a8e4-f1228ef2353e",
                    "LayerId": "2c06e5f8-ab99-4a2a-a1ef-9d6fb24ab460"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "2c06e5f8-ab99-4a2a-a1ef-9d6fb24ab460",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56e5dd73-006a-4a16-a235-52197bb3aa64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 10,
    "yorig": 7
}