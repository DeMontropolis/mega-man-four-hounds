{
    "id": "6c427a83-d2a2-422b-ab06-2b794fbc0c44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandThrowOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62aa60c9-2251-429b-9746-b18aa5096d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c427a83-d2a2-422b-ab06-2b794fbc0c44",
            "compositeImage": {
                "id": "d07ce77a-55ab-49f7-8f1a-9c300fb54fa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62aa60c9-2251-429b-9746-b18aa5096d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bfba45a-8cc5-4a03-9f22-f4939b51102a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62aa60c9-2251-429b-9746-b18aa5096d48",
                    "LayerId": "1072e0b4-a957-4d6c-a9e9-cbb1af243626"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "1072e0b4-a957-4d6c-a9e9-cbb1af243626",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c427a83-d2a2-422b-ab06-2b794fbc0c44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 10,
    "yorig": 7
}