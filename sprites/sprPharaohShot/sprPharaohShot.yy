{
    "id": "daf39dbc-d03b-48f8-a577-1c9e06366f54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohShot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7275ce0e-ef55-45dd-a6f0-867395c5f462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daf39dbc-d03b-48f8-a577-1c9e06366f54",
            "compositeImage": {
                "id": "fe39dbcc-f4d1-455f-ada1-ccd1fe68d56c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7275ce0e-ef55-45dd-a6f0-867395c5f462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdeb7c86-fea3-4af7-a5d7-1e09aa83db0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7275ce0e-ef55-45dd-a6f0-867395c5f462",
                    "LayerId": "dea059c9-000f-4c0a-ba40-aaddd66fd578"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "dea059c9-000f-4c0a-ba40-aaddd66fd578",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daf39dbc-d03b-48f8-a577-1c9e06366f54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}