{
    "id": "70ffda28-2ab2-4325-9422-b321fe30d638",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "GorudoW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52eaf95f-d83d-409a-8429-f523e125b004",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ffda28-2ab2-4325-9422-b321fe30d638",
            "compositeImage": {
                "id": "1531c7a2-2bb8-4b47-87ea-002fd9c4e0b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52eaf95f-d83d-409a-8429-f523e125b004",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d7a8d6-7412-4f40-a11e-de212e1e6c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52eaf95f-d83d-409a-8429-f523e125b004",
                    "LayerId": "4646e75c-01ac-4483-8111-1bd98d889ecb"
                }
            ]
        },
        {
            "id": "9152d79b-04b5-40a7-9f18-a0caa44d2dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ffda28-2ab2-4325-9422-b321fe30d638",
            "compositeImage": {
                "id": "c3e4b7b6-6b11-41bf-a371-fd9291e616c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9152d79b-04b5-40a7-9f18-a0caa44d2dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ae1824a-7012-4d7c-95e3-39abfcaaccc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9152d79b-04b5-40a7-9f18-a0caa44d2dd5",
                    "LayerId": "4646e75c-01ac-4483-8111-1bd98d889ecb"
                }
            ]
        },
        {
            "id": "feb71eca-ca84-4fe3-9caf-dbe2e5c2d9c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ffda28-2ab2-4325-9422-b321fe30d638",
            "compositeImage": {
                "id": "e419c17d-c577-427b-ba41-91fe715555c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feb71eca-ca84-4fe3-9caf-dbe2e5c2d9c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52313136-53d9-4602-8abf-1df74cb592d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feb71eca-ca84-4fe3-9caf-dbe2e5c2d9c4",
                    "LayerId": "4646e75c-01ac-4483-8111-1bd98d889ecb"
                }
            ]
        },
        {
            "id": "a6fddcf3-cc22-438e-8cdf-6a36cb7a5a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ffda28-2ab2-4325-9422-b321fe30d638",
            "compositeImage": {
                "id": "b62ad233-7f49-49af-ae44-a2c68124a234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6fddcf3-cc22-438e-8cdf-6a36cb7a5a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c0a3d4-6a48-4f1d-b7d4-051b108c7c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6fddcf3-cc22-438e-8cdf-6a36cb7a5a47",
                    "LayerId": "4646e75c-01ac-4483-8111-1bd98d889ecb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "4646e75c-01ac-4483-8111-1bd98d889ecb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70ffda28-2ab2-4325-9422-b321fe30d638",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0.1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 16
}