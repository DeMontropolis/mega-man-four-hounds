{
    "id": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFont",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ef8506a-0642-491d-910f-72e7df736694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "47401572-c9a5-493b-bf0e-b1dff7b31693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef8506a-0642-491d-910f-72e7df736694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b713779-486e-4302-8cc3-5d07a76b7a0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef8506a-0642-491d-910f-72e7df736694",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "93d98f9e-9c63-4775-98b1-565c0f7e7d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "695519b3-d273-45c6-9ba2-6e4cc668cc21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d98f9e-9c63-4775-98b1-565c0f7e7d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a588ec7b-4e2d-4db9-b624-29ce22bd2c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d98f9e-9c63-4775-98b1-565c0f7e7d83",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "0e60fa5f-8034-4bdc-8995-5ad0f2fec333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "4bfa0fe0-cccc-41e0-91b5-34717d748788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e60fa5f-8034-4bdc-8995-5ad0f2fec333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0d62a92-96a8-4297-a813-eb13e492bece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e60fa5f-8034-4bdc-8995-5ad0f2fec333",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "9dbdafa9-5180-4dc3-94c6-1bdaabbdc029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "bf096873-d41d-4b82-afcd-9b6f1ce9b9ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dbdafa9-5180-4dc3-94c6-1bdaabbdc029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c8eea37-4daa-461f-8589-13f2b0947d0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dbdafa9-5180-4dc3-94c6-1bdaabbdc029",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "10cd70c6-f050-40e7-b1a2-c5970f2e7da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "4ad4190c-acce-4ad0-bb71-993065abf720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10cd70c6-f050-40e7-b1a2-c5970f2e7da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "778bddbf-060b-428f-bde4-b6170800ff6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10cd70c6-f050-40e7-b1a2-c5970f2e7da3",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "29d6bec4-987a-4c99-bc3e-516fc1b0d9b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "860fe306-9cde-415d-9718-021494710288",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d6bec4-987a-4c99-bc3e-516fc1b0d9b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfa80c0f-0950-44b1-91a4-f6b9a00ed5fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d6bec4-987a-4c99-bc3e-516fc1b0d9b2",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "ded7294e-3442-4467-ac73-aac2fbaf1582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "44ade5dd-3e9f-4812-8198-bec23c115c08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded7294e-3442-4467-ac73-aac2fbaf1582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f1f6b84-2395-4438-bc51-41a7c6fdcf41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded7294e-3442-4467-ac73-aac2fbaf1582",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "1b024d6d-6638-498c-bd90-a19f13662dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "f7ba00b8-0e33-4890-8068-e2d5ee000a6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b024d6d-6638-498c-bd90-a19f13662dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8464e9b-1ef6-480f-b9f3-ac8428c3eb3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b024d6d-6638-498c-bd90-a19f13662dc2",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "0d88e893-5188-44af-8021-3f59678d496b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "69aa0cf4-b6dd-4a5f-ae9c-4d7dd46d8445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d88e893-5188-44af-8021-3f59678d496b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "684098e3-1806-440a-9599-35dd959b56d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d88e893-5188-44af-8021-3f59678d496b",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "33e92f16-1f42-4122-9604-fdc2fae463dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "0b120524-87d2-46ca-ab7e-c88157ddc430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e92f16-1f42-4122-9604-fdc2fae463dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc29866-f874-4249-8868-ffcc85764a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e92f16-1f42-4122-9604-fdc2fae463dc",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "a39baaef-fa24-43c0-9508-3ece78d047c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "aae7d9c2-7104-4e00-af7f-3380f6ea14f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a39baaef-fa24-43c0-9508-3ece78d047c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "616f5484-da66-4af9-908d-1eb75c832956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a39baaef-fa24-43c0-9508-3ece78d047c5",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "7bb6ecca-4895-4eb2-8ef7-c77c359400bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "ba6baf56-00a8-4f62-afb2-4fa499b4a531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bb6ecca-4895-4eb2-8ef7-c77c359400bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4acfc97-0f72-4e9c-9e9c-84cb7da79e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bb6ecca-4895-4eb2-8ef7-c77c359400bc",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "0152439f-069d-461e-b069-34dcf249a6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "b640c250-8145-4c84-9f16-14088ba4c889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0152439f-069d-461e-b069-34dcf249a6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8199dfd-da32-4610-8320-cb2df2326658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0152439f-069d-461e-b069-34dcf249a6b2",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "5acd6a12-802e-4bb2-b5a5-26e4db8561ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "c86c56a4-d793-4e71-9c57-2a218f665709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5acd6a12-802e-4bb2-b5a5-26e4db8561ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2ef4604-54cf-4bc8-bb0a-3b9b4a999d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5acd6a12-802e-4bb2-b5a5-26e4db8561ea",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "bbb9c193-6a5d-4b67-9703-f404ed08ded4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "bf3b0dea-7041-42a1-b264-1cc1476a585f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbb9c193-6a5d-4b67-9703-f404ed08ded4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f50c7335-12b8-4126-9bdd-255a6ba9ce0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbb9c193-6a5d-4b67-9703-f404ed08ded4",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "265978de-7a9a-4e47-b0a8-ee492ff3f37a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "93bff82f-20a5-4876-a514-26635275f8af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "265978de-7a9a-4e47-b0a8-ee492ff3f37a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c86d84f-41ef-41cf-97d0-aa66d0c50bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265978de-7a9a-4e47-b0a8-ee492ff3f37a",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "852e1854-696e-4d95-b9f0-4d58bc2db625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "10644356-321a-4278-afa9-9bde21202ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "852e1854-696e-4d95-b9f0-4d58bc2db625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a00b85d1-91f7-4799-b76b-e67a336cbc38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "852e1854-696e-4d95-b9f0-4d58bc2db625",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "533c2939-5b3a-4b1f-b6eb-5d2d35e87e83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "a3be140f-8835-455c-9f0e-9b18464d7191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "533c2939-5b3a-4b1f-b6eb-5d2d35e87e83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aea91d9-6688-4a47-b6fd-6aa1c833ab42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "533c2939-5b3a-4b1f-b6eb-5d2d35e87e83",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "0b827e43-efd8-4e5b-8df6-bfa22db25767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "09a73249-f36a-4bad-b31d-ccfdf6259ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b827e43-efd8-4e5b-8df6-bfa22db25767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb537497-f8f7-4ca8-bb1d-ace403af02fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b827e43-efd8-4e5b-8df6-bfa22db25767",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "a3882bd3-c14d-4ea8-a310-d4156526ec1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "8401e80f-f6dc-48a6-ab08-89997af05cce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3882bd3-c14d-4ea8-a310-d4156526ec1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0da7c74-e72b-4642-8483-a9548b56d2c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3882bd3-c14d-4ea8-a310-d4156526ec1c",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "bcc3b5b2-c0cd-4465-a209-2996a9ef1336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "87a91aa8-1d51-4f6a-8497-fd298ca3cc9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcc3b5b2-c0cd-4465-a209-2996a9ef1336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6957de9-38f7-4fa2-a2cc-2f1ea4ff110a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcc3b5b2-c0cd-4465-a209-2996a9ef1336",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "eea1c472-7750-472f-8e89-3d101f9b7730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "d22e6746-e2cd-42cb-b92d-2ecf8f448720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eea1c472-7750-472f-8e89-3d101f9b7730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c995db88-05af-446b-b0e8-5f5636b17d04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eea1c472-7750-472f-8e89-3d101f9b7730",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "63735ccd-0fec-489c-b731-300ad960986e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "81c30eb8-107a-464d-89f0-9c551d00be58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63735ccd-0fec-489c-b731-300ad960986e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bba9d83-355d-44a8-9cfa-c74f53187ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63735ccd-0fec-489c-b731-300ad960986e",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "dff0ca4d-16ba-4ff2-908a-12b9a5a69540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "010199e8-a775-48af-ab76-0c5ab199d02f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dff0ca4d-16ba-4ff2-908a-12b9a5a69540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5285ef2-1609-4d99-b505-2e2b4d32f49b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dff0ca4d-16ba-4ff2-908a-12b9a5a69540",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "8ffb4520-acb9-4f8f-9e70-e5565fa771ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "86115497-218f-4c8a-85d5-7a576e0eade2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ffb4520-acb9-4f8f-9e70-e5565fa771ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85791709-e05e-4c80-91c5-f64b1552590e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ffb4520-acb9-4f8f-9e70-e5565fa771ff",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "adef50fc-79d7-4bbd-a9bb-9ed0f102d01c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "faad57b9-b41e-4950-b8fd-e1775fd5659d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adef50fc-79d7-4bbd-a9bb-9ed0f102d01c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b019923-a5bf-4ed8-8586-0f1c550a0704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adef50fc-79d7-4bbd-a9bb-9ed0f102d01c",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "ada55876-ac58-41cd-a275-6653bbf5ec0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "c3dea577-556c-4cc8-b29e-2ae1ccd0c518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ada55876-ac58-41cd-a275-6653bbf5ec0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d267267-bd2b-43be-a824-64bbfa677bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ada55876-ac58-41cd-a275-6653bbf5ec0d",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "7edea0d6-50ce-4d85-8e9b-f2a39e3b2fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "5158e06b-891f-4319-ab7e-376dc71eac2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7edea0d6-50ce-4d85-8e9b-f2a39e3b2fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03e3c458-aeea-4d46-9211-5e42a06718eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7edea0d6-50ce-4d85-8e9b-f2a39e3b2fcf",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "2a9a0987-7e2c-4efd-a107-1595b63bbfff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "d6490e54-b983-4964-99ae-e5ffd56ecdf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a9a0987-7e2c-4efd-a107-1595b63bbfff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb7f94b-f592-4210-bb86-69b3ae4da2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a9a0987-7e2c-4efd-a107-1595b63bbfff",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "40435487-fd4b-4f5b-a299-276450f7b407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "f8f475ed-160f-4b6a-adbf-a0f86176ca8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40435487-fd4b-4f5b-a299-276450f7b407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c21e01b-2e79-4df7-a5a2-0ffdee342985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40435487-fd4b-4f5b-a299-276450f7b407",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "d162f71f-3811-4f31-b92e-fc606d4469dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "66301e0e-0333-4767-9be4-3111d1537df4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d162f71f-3811-4f31-b92e-fc606d4469dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4ebcba5-37a4-419e-b1db-c07d89eaaa5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d162f71f-3811-4f31-b92e-fc606d4469dc",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "60467123-daf6-41b0-91f6-775306fef540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "2bf1a172-2a96-4c61-a552-b9edc36b46cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60467123-daf6-41b0-91f6-775306fef540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74b5534a-9da8-4ca6-a9ac-9e6dc95c098b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60467123-daf6-41b0-91f6-775306fef540",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "ba0131b6-d870-4773-bc2a-1835c795dab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "95c35541-6724-4fd7-a2da-9aa00149f0cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0131b6-d870-4773-bc2a-1835c795dab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c473615-547b-4252-8b84-dc056a1e64c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0131b6-d870-4773-bc2a-1835c795dab9",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "05610fac-b07a-4193-8862-19235317f7ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "915610cd-6543-4648-8248-083bd1b48256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05610fac-b07a-4193-8862-19235317f7ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89dc3a90-16ff-418e-8745-d1139083ef10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05610fac-b07a-4193-8862-19235317f7ce",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "ba0c6840-30c5-43e1-bd7c-39ea9bbbec88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "2aafc162-aad8-4c21-967d-cddb084c1fcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0c6840-30c5-43e1-bd7c-39ea9bbbec88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8753e5fa-9963-4d68-ae46-0e5d9161d464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0c6840-30c5-43e1-bd7c-39ea9bbbec88",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "00342bee-34fd-4e02-b9e6-507120e3b10f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "c88ee5d0-181d-4b2c-80fc-37b34e958275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00342bee-34fd-4e02-b9e6-507120e3b10f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e62dcd-d865-4d01-8e4a-d4e0e63fb7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00342bee-34fd-4e02-b9e6-507120e3b10f",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "2e1e35db-1cf0-404e-9e51-8d691470bfa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "6dd32775-bb37-4db9-bafc-64d7fe5f4607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e1e35db-1cf0-404e-9e51-8d691470bfa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "424f2978-395e-4bd2-9da1-f0bd6606a0a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e1e35db-1cf0-404e-9e51-8d691470bfa4",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "098de324-3bdc-475c-b02e-8831444a5ae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "51917268-ce10-418c-93fc-959fa2fb6bda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "098de324-3bdc-475c-b02e-8831444a5ae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6bd5dc-44ac-4d1e-8f5e-965aa81f6b76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "098de324-3bdc-475c-b02e-8831444a5ae8",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "3f6173d1-57f4-4426-b8cc-8e376c094253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "f76427d9-9f4d-46d6-b9e7-db9072abb901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f6173d1-57f4-4426-b8cc-8e376c094253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c9dc7e4-6b52-467c-8127-ba18a72ad1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f6173d1-57f4-4426-b8cc-8e376c094253",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "4a857abd-8eb4-4c65-88f8-6c89e4606c12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "e14d7642-a6ed-4d3e-9896-5c3ec617b8aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a857abd-8eb4-4c65-88f8-6c89e4606c12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16514c19-7a55-434f-a6a6-884eed4b6a51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a857abd-8eb4-4c65-88f8-6c89e4606c12",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "8b37043f-f8c4-467a-9317-a27403af74e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "2d26ac6d-1886-4370-afca-08989e1c13b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b37043f-f8c4-467a-9317-a27403af74e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1579676f-416f-40ba-bc44-451e88bcb772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b37043f-f8c4-467a-9317-a27403af74e5",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "67d500ce-e6ac-435d-9727-889484515d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "01b1a74f-1ee3-4a1c-ac7c-f8a01902c00e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67d500ce-e6ac-435d-9727-889484515d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d9bfcbf-f1ec-4024-b1d2-d5aba1276b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67d500ce-e6ac-435d-9727-889484515d4f",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "acfdf7db-167e-4115-9bd4-5a66d8d9aa80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "a8c9903a-956d-41ba-9cdf-5a5c8225572c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acfdf7db-167e-4115-9bd4-5a66d8d9aa80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a32fcd2-4d59-470f-896b-9fb167fea3e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acfdf7db-167e-4115-9bd4-5a66d8d9aa80",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "6d3bdbe8-4e7d-4045-9230-1f9e0e624a41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "3ac8832b-9687-49d5-9cd0-dd2e834f0b95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d3bdbe8-4e7d-4045-9230-1f9e0e624a41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eaa7d9f-997e-45b7-a540-e97633ad52d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d3bdbe8-4e7d-4045-9230-1f9e0e624a41",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "3ea6d605-c24e-4912-ae89-9db09c27279c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "b726bf49-ab09-4161-b8d2-4b5519e09644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea6d605-c24e-4912-ae89-9db09c27279c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88405984-f93d-4258-acb7-97689c9d230a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea6d605-c24e-4912-ae89-9db09c27279c",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "431a9c82-ac9e-49c5-8c8a-21c79877dec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "3b491176-3be8-4a10-9fad-debb3c6f93e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "431a9c82-ac9e-49c5-8c8a-21c79877dec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96bb3a09-aa2f-4c0c-a138-2a8029c760c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "431a9c82-ac9e-49c5-8c8a-21c79877dec7",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "7e07aa3f-28c5-48f9-963e-4ea064a78373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "e6a6d786-7992-4342-a25a-824db89cf9c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e07aa3f-28c5-48f9-963e-4ea064a78373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ed2563-65b5-407e-bef3-b99b441fb0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e07aa3f-28c5-48f9-963e-4ea064a78373",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "b2b7cef3-d1b8-47fe-9bfe-46171faada1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "09df1ffc-7ad7-4c41-ad8e-b5b632abcf18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2b7cef3-d1b8-47fe-9bfe-46171faada1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acf42e0b-76cd-49c4-8a94-6fb070b25136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2b7cef3-d1b8-47fe-9bfe-46171faada1d",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "7f9954ee-48d8-405b-abca-10bd9779062b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "402974bd-b04e-4c48-b198-1abd4a348ec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f9954ee-48d8-405b-abca-10bd9779062b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d8b8a70-5a80-4f61-83b7-efb3984c52ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f9954ee-48d8-405b-abca-10bd9779062b",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "fef07028-4490-45c0-9e57-989d7c4a9c8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "1c13eeb9-d1c9-4e32-8d51-a8a8823dc6e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fef07028-4490-45c0-9e57-989d7c4a9c8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9f7ae24-fa06-4208-9545-c687e331ce66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fef07028-4490-45c0-9e57-989d7c4a9c8c",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "09cf01f8-d5d8-499d-a76c-ae75bbcc078e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "ef9ab76e-9235-4524-86fe-e4b8bb6017bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09cf01f8-d5d8-499d-a76c-ae75bbcc078e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c02cfd9-70ca-418f-a377-16ee3876a503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09cf01f8-d5d8-499d-a76c-ae75bbcc078e",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "0f39db9d-a0f5-4d62-a41a-7d05b1c3627e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "c811bdf5-cd31-4a9e-a35a-82d2d38a08a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f39db9d-a0f5-4d62-a41a-7d05b1c3627e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd9670f4-aefb-4735-859b-d68a3f6a06ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f39db9d-a0f5-4d62-a41a-7d05b1c3627e",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "45d27106-1c32-4196-8fec-5a89e18c650d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "2af19051-0460-413a-9bd5-35af2013bc30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d27106-1c32-4196-8fec-5a89e18c650d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c350e38d-c0e1-4ea4-a5b4-ac8f13daf424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d27106-1c32-4196-8fec-5a89e18c650d",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "6b8c8db0-6620-4224-ab77-0ca9b169da47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "a62f4f51-ce7c-4168-85c7-2c739f256d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b8c8db0-6620-4224-ab77-0ca9b169da47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6057dca-0659-4bd5-9964-193685222e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b8c8db0-6620-4224-ab77-0ca9b169da47",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "9db95491-313d-4cac-ba81-507ae76d087a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "a784fee3-ef35-4956-95f0-7cfeba55a56c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db95491-313d-4cac-ba81-507ae76d087a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae1f3e88-a9a7-4686-8eac-0673741f5afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db95491-313d-4cac-ba81-507ae76d087a",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "be6e2ef5-777e-43dd-9c11-82ecf92f674a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "236dca0c-621d-44d5-97f2-d5584f76721b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be6e2ef5-777e-43dd-9c11-82ecf92f674a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea06ded-abcf-4a31-a88c-1ea35a439dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be6e2ef5-777e-43dd-9c11-82ecf92f674a",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "4f2c2b52-bb58-40c9-85b0-9f85355dd67b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "c327d5e7-01d6-42db-a83c-9cbcafa9289e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2c2b52-bb58-40c9-85b0-9f85355dd67b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24686031-0b42-4925-8cdd-4899d429b559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2c2b52-bb58-40c9-85b0-9f85355dd67b",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        },
        {
            "id": "140f21a9-1e56-4e21-bd0c-0a9aae40e35a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "compositeImage": {
                "id": "cf478f4a-ca34-485b-b521-608e076f3b53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "140f21a9-1e56-4e21-bd0c-0a9aae40e35a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "143ad580-32de-4f21-b99c-d3323d9fac13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "140f21a9-1e56-4e21-bd0c-0a9aae40e35a",
                    "LayerId": "067ed34c-c03d-4e39-8644-5a0bed7fa842"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "067ed34c-c03d-4e39-8644-5a0bed7fa842",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd453c51-81f5-4c8d-b3f1-cca45b998fb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}