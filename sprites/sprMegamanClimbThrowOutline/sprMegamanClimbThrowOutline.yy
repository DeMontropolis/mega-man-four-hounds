{
    "id": "a27b862c-4452-4eb8-89db-9310f81f2013",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbThrowOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b809d8d9-72cb-42f4-b09c-68ca832cf4b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a27b862c-4452-4eb8-89db-9310f81f2013",
            "compositeImage": {
                "id": "efd697c4-2d7e-4c6d-8e31-3ac9ae56b8ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b809d8d9-72cb-42f4-b09c-68ca832cf4b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbfcc67a-a6dc-4c3a-b6aa-8f3d888c5677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b809d8d9-72cb-42f4-b09c-68ca832cf4b5",
                    "LayerId": "f018e35d-fcb8-48ab-b959-500774832a6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "f018e35d-fcb8-48ab-b959-500774832a6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a27b862c-4452-4eb8-89db-9310f81f2013",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 8,
    "yorig": 7
}