{
    "id": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponIcons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7adc8778-a5a4-489f-95e3-01f9327859dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "1e587a2c-1d35-489a-bbd2-0e474d5a922c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7adc8778-a5a4-489f-95e3-01f9327859dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c8fc61d-191d-46ba-b28d-fa28d36e1aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7adc8778-a5a4-489f-95e3-01f9327859dc",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        },
        {
            "id": "5dd72425-f800-4610-8b42-ecaf61a79fd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "e0811897-1d57-420e-8f79-fb77059caa79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd72425-f800-4610-8b42-ecaf61a79fd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b970fea-642d-4397-acde-e819dbca2132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd72425-f800-4610-8b42-ecaf61a79fd7",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        },
        {
            "id": "401afae6-5f79-4019-89f7-9a73f6836c26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "48ef46e1-7e2b-4f3b-bd16-fe79b3c0ee4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "401afae6-5f79-4019-89f7-9a73f6836c26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9511429e-4c6d-4d10-ae70-56af407cd08c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "401afae6-5f79-4019-89f7-9a73f6836c26",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        },
        {
            "id": "281d48d5-13ad-4f85-82e7-52f033f9f6c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "7a3e4f7a-4eca-4d1d-8562-da2b51987c80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "281d48d5-13ad-4f85-82e7-52f033f9f6c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9595de3-804f-4e7b-825f-60ea58aafa18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "281d48d5-13ad-4f85-82e7-52f033f9f6c7",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        },
        {
            "id": "2eabff21-5964-4f01-944a-ba3ab1d9ade7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "307966b7-e120-4b64-ae05-228df2dabeb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eabff21-5964-4f01-944a-ba3ab1d9ade7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7941f80d-21ad-43df-8650-44808d0de444",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eabff21-5964-4f01-944a-ba3ab1d9ade7",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        },
        {
            "id": "6f100278-6055-44b0-b370-15e2e67f3da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "b9e11c97-62e7-42d7-8b8a-538131904036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f100278-6055-44b0-b370-15e2e67f3da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e739e1-52af-4efb-987c-ba2c8f7848e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f100278-6055-44b0-b370-15e2e67f3da7",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        },
        {
            "id": "a2b9f5fe-fa8d-4f8b-8b2c-a451c129b05e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "4baa1649-2a54-4642-b6c3-4623012b0b9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2b9f5fe-fa8d-4f8b-8b2c-a451c129b05e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7d93064-988d-486a-a646-af6b29daa3ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2b9f5fe-fa8d-4f8b-8b2c-a451c129b05e",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        },
        {
            "id": "51bee4b0-3a01-4760-89ba-e1af23f3ba12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "91cddebf-1bf9-4791-90e2-4b6341679609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51bee4b0-3a01-4760-89ba-e1af23f3ba12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd370129-bc65-4e0e-9be2-57d3dd730a9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51bee4b0-3a01-4760-89ba-e1af23f3ba12",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        },
        {
            "id": "6076c2ed-c279-41bb-8fbe-8f8f8f79fdb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "compositeImage": {
                "id": "c0b45afc-1d02-4a67-a73b-c04e88dc0d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6076c2ed-c279-41bb-8fbe-8f8f8f79fdb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81b6a6bc-e7bd-4d19-8ea4-5674a81d8227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6076c2ed-c279-41bb-8fbe-8f8f8f79fdb0",
                    "LayerId": "2188ebf8-75e4-493c-9370-da8aa6e6ea91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2188ebf8-75e4-493c-9370-da8aa6e6ea91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13be7a6c-018c-4c1b-926d-be3ccfae1422",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}