{
    "id": "1fbe4c53-84da-42ce-8e4f-c56c52e92de9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponEnergySmallOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae673de8-2645-43b1-a5d9-9a1c35d75ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbe4c53-84da-42ce-8e4f-c56c52e92de9",
            "compositeImage": {
                "id": "876b724a-9345-4bdd-9db5-29707ff36d51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae673de8-2645-43b1-a5d9-9a1c35d75ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e4d3f0-2640-4dc6-9aab-859c29670d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae673de8-2645-43b1-a5d9-9a1c35d75ae6",
                    "LayerId": "951ff6c3-9757-424b-9574-7dbb00763d7b"
                }
            ]
        },
        {
            "id": "0d6419aa-4c27-4f1c-b1cf-34cbde743e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fbe4c53-84da-42ce-8e4f-c56c52e92de9",
            "compositeImage": {
                "id": "bbdfcf37-2867-4a21-9a2b-c4ce26cda2be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6419aa-4c27-4f1c-b1cf-34cbde743e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb174c10-ede1-4585-9e56-a43a60d56192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6419aa-4c27-4f1c-b1cf-34cbde743e3e",
                    "LayerId": "951ff6c3-9757-424b-9574-7dbb00763d7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "951ff6c3-9757-424b-9574-7dbb00763d7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fbe4c53-84da-42ce-8e4f-c56c52e92de9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": -4,
    "yorig": -8
}