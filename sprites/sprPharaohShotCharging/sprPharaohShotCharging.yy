{
    "id": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohShotCharging",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 2,
    "bbox_right": 17,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b54b34e-0f06-4e8b-abe9-2e78586053c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "compositeImage": {
                "id": "24fe56f5-faf2-49a1-8404-7ee037f2d4a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b54b34e-0f06-4e8b-abe9-2e78586053c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8ec297d-08d5-4d9c-8dfb-d3c5932d9757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b54b34e-0f06-4e8b-abe9-2e78586053c8",
                    "LayerId": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64"
                }
            ]
        },
        {
            "id": "5eb73d25-ece2-406c-a076-57dc294c0b56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "compositeImage": {
                "id": "1d81da91-c77c-4621-afc5-af5bfe89d6a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb73d25-ece2-406c-a076-57dc294c0b56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ffa56cf-9fb6-44b9-b99b-062f28fad8ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb73d25-ece2-406c-a076-57dc294c0b56",
                    "LayerId": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64"
                }
            ]
        },
        {
            "id": "f3f2ee33-93f2-43c0-9357-5b9f4dad3f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "compositeImage": {
                "id": "453e0547-905a-4f79-a0e1-4b4cf8a4eeaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f2ee33-93f2-43c0-9357-5b9f4dad3f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9992aa78-07e0-413d-8edf-5df16b2c09f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f2ee33-93f2-43c0-9357-5b9f4dad3f2f",
                    "LayerId": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64"
                }
            ]
        },
        {
            "id": "3db243c4-619b-4532-9ec5-817dbcb3fe92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "compositeImage": {
                "id": "8b37b226-62ee-4913-8815-77178b6e6ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db243c4-619b-4532-9ec5-817dbcb3fe92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53db84e2-ebe4-430d-9af6-77bfe9915f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db243c4-619b-4532-9ec5-817dbcb3fe92",
                    "LayerId": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64"
                }
            ]
        },
        {
            "id": "b190a88d-89c0-49b1-8a35-4760631793a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "compositeImage": {
                "id": "e5ef1b8e-2bd2-4c70-9243-9c9acb92c0e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b190a88d-89c0-49b1-8a35-4760631793a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb419d3f-57d9-48f7-8338-2703d8eac8c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b190a88d-89c0-49b1-8a35-4760631793a4",
                    "LayerId": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64"
                }
            ]
        },
        {
            "id": "1659fdef-a564-456d-97c9-c1ab10d1620e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "compositeImage": {
                "id": "fb222582-6367-4aad-a6c5-812380477424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1659fdef-a564-456d-97c9-c1ab10d1620e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b7d057b-48c2-4416-8996-cba8b7ee6ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1659fdef-a564-456d-97c9-c1ab10d1620e",
                    "LayerId": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64"
                }
            ]
        },
        {
            "id": "555cbb80-11c4-4fa8-81f7-c9052b7ec685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "compositeImage": {
                "id": "b00b2f87-ad0b-4ce3-921d-6d9d67f20158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "555cbb80-11c4-4fa8-81f7-c9052b7ec685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9665760-e8cb-45be-9c43-4665fb1ad4c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "555cbb80-11c4-4fa8-81f7-c9052b7ec685",
                    "LayerId": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64"
                }
            ]
        },
        {
            "id": "3f8d286f-ec5d-4510-8525-a8b4bbe68f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "compositeImage": {
                "id": "437c43aa-eb88-4551-a74b-56271bfc57f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f8d286f-ec5d-4510-8525-a8b4bbe68f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1387facb-017e-4c6c-82b8-207a52be9a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f8d286f-ec5d-4510-8525-a8b4bbe68f84",
                    "LayerId": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "ea06d6a3-ddc2-4c2a-8aaa-381107830b64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a2855ff-c2aa-4ad1-acb9-7333630bbb03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}