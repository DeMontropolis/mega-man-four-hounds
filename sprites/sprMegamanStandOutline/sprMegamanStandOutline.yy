{
    "id": "dbffe8b1-1926-4611-a83b-d41edfc2f545",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e87e2b23-1d50-4231-ab46-fe7ddbfc90c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbffe8b1-1926-4611-a83b-d41edfc2f545",
            "compositeImage": {
                "id": "ced74a5c-cd71-481f-aea2-1ad872f68bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e87e2b23-1d50-4231-ab46-fe7ddbfc90c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc232e5-a99a-4b21-8c0a-aa265b859d53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e87e2b23-1d50-4231-ab46-fe7ddbfc90c5",
                    "LayerId": "2e09a866-2566-40b5-8d5f-cc1a49a14b29"
                }
            ]
        },
        {
            "id": "1fcd6b91-e7ea-4803-a1b4-23e358ee4891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbffe8b1-1926-4611-a83b-d41edfc2f545",
            "compositeImage": {
                "id": "95b835ae-76f9-4d7c-880b-21c35d82e1d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fcd6b91-e7ea-4803-a1b4-23e358ee4891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0817c157-5c9b-43ca-904d-9d6ddfc37d3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fcd6b91-e7ea-4803-a1b4-23e358ee4891",
                    "LayerId": "2e09a866-2566-40b5-8d5f-cc1a49a14b29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "2e09a866-2566-40b5-8d5f-cc1a49a14b29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbffe8b1-1926-4611-a83b-d41edfc2f545",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 7
}