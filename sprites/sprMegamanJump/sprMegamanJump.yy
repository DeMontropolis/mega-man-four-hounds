{
    "id": "b43c8455-a8ef-469b-bb0a-80b35af0c434",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59f28ab1-db52-4ad9-b9ee-19f5a587b0c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b43c8455-a8ef-469b-bb0a-80b35af0c434",
            "compositeImage": {
                "id": "ea2f2f98-4713-4c7f-b165-9bf70b71705b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f28ab1-db52-4ad9-b9ee-19f5a587b0c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bfc963f-cb8c-4659-be4b-931f2558a55e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f28ab1-db52-4ad9-b9ee-19f5a587b0c1",
                    "LayerId": "ac4420f5-57b8-40e9-b5c6-d2f42aca2148"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "ac4420f5-57b8-40e9-b5c6-d2f42aca2148",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b43c8455-a8ef-469b-bb0a-80b35af0c434",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 12,
    "yorig": 7
}