{
    "id": "826b5690-77b1-4fcf-bca5-83fc0469b937",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "efcc12f2-3788-4818-9b86-d2be869a312a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826b5690-77b1-4fcf-bca5-83fc0469b937",
            "compositeImage": {
                "id": "5ceb7d18-64dc-48be-9a67-96037dfce7aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efcc12f2-3788-4818-9b86-d2be869a312a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7854a660-f181-4ce5-85e5-229eec699b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efcc12f2-3788-4818-9b86-d2be869a312a",
                    "LayerId": "d94fbfd8-3f44-402d-9513-034241774cca"
                }
            ]
        },
        {
            "id": "7c49f308-30fc-4027-864e-5bdd56ad366a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826b5690-77b1-4fcf-bca5-83fc0469b937",
            "compositeImage": {
                "id": "12b1f8a8-5f80-4484-96e8-6f2f8747e815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c49f308-30fc-4027-864e-5bdd56ad366a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc6a47ec-c11f-4924-adc5-32b843396b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c49f308-30fc-4027-864e-5bdd56ad366a",
                    "LayerId": "d94fbfd8-3f44-402d-9513-034241774cca"
                }
            ]
        },
        {
            "id": "c29c86d9-9157-41ef-b31e-760f7780ad8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826b5690-77b1-4fcf-bca5-83fc0469b937",
            "compositeImage": {
                "id": "fb7c527a-9101-40ae-ae25-e4e62fe09172",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c29c86d9-9157-41ef-b31e-760f7780ad8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c881b550-c5a2-4feb-9dbf-9927dcc57011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c29c86d9-9157-41ef-b31e-760f7780ad8d",
                    "LayerId": "d94fbfd8-3f44-402d-9513-034241774cca"
                }
            ]
        },
        {
            "id": "2ca24317-9405-45e4-88af-c2f64b61255f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "826b5690-77b1-4fcf-bca5-83fc0469b937",
            "compositeImage": {
                "id": "7fe73022-d8ac-4aa3-9946-e05f291b3049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca24317-9405-45e4-88af-c2f64b61255f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c6ed47-4d75-4553-b15d-302940aef812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca24317-9405-45e4-88af-c2f64b61255f",
                    "LayerId": "d94fbfd8-3f44-402d-9513-034241774cca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d94fbfd8-3f44-402d-9513-034241774cca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "826b5690-77b1-4fcf-bca5-83fc0469b937",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0.1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 11,
    "yorig": 7
}