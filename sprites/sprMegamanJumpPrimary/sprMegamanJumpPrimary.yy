{
    "id": "3a5ee4e4-6a4d-46e0-8a63-46386720a4e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 24,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c43b9be-a745-424b-becf-8148a5641fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a5ee4e4-6a4d-46e0-8a63-46386720a4e4",
            "compositeImage": {
                "id": "dae8f160-ef30-4b28-b103-3a49f74d3f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c43b9be-a745-424b-becf-8148a5641fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "082cae7a-bf15-45d2-aa91-b974c342d3d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c43b9be-a745-424b-becf-8148a5641fcc",
                    "LayerId": "f5e946fb-39f1-4c26-bc7d-8c31cb3b6988"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "f5e946fb-39f1-4c26-bc7d-8c31cb3b6988",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a5ee4e4-6a4d-46e0-8a63-46386720a4e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 12,
    "yorig": 7
}