{
    "id": "1f938811-32df-4568-ae0c-c3a5abe465bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 1,
    "bbox_right": 34,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a608fa2-7f1d-49eb-8ead-2348efcf79d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f938811-32df-4568-ae0c-c3a5abe465bb",
            "compositeImage": {
                "id": "fe8bb928-ab73-42d9-94bd-8dc15d4bbf1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a608fa2-7f1d-49eb-8ead-2348efcf79d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "751de48d-a45a-4ef4-b8ec-d8b4b5d9967e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a608fa2-7f1d-49eb-8ead-2348efcf79d9",
                    "LayerId": "8c431fd4-51af-44cc-b889-bffa942a5dce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "8c431fd4-51af-44cc-b889-bffa942a5dce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f938811-32df-4568-ae0c-c3a5abe465bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 13,
    "yorig": 19
}