{
    "id": "bdb743ce-1e29-4b19-8f5a-8069fc714a55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohCharge3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 2,
    "bbox_right": 24,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "640e49a8-9fba-40cc-8faa-8b49a90389a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdb743ce-1e29-4b19-8f5a-8069fc714a55",
            "compositeImage": {
                "id": "d2f56e40-ad30-4c15-b355-6d7ff14c9388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "640e49a8-9fba-40cc-8faa-8b49a90389a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d316a80-0b81-4ee3-a73f-a6657705fa05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "640e49a8-9fba-40cc-8faa-8b49a90389a8",
                    "LayerId": "e397c280-7fd9-4442-acae-056937be14c5"
                }
            ]
        },
        {
            "id": "0b46c474-64dc-4dc2-831a-a0e98fbe15ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdb743ce-1e29-4b19-8f5a-8069fc714a55",
            "compositeImage": {
                "id": "a1758a5f-ccd8-4dc6-a1b4-feb84d995816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b46c474-64dc-4dc2-831a-a0e98fbe15ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0598331b-cf42-4e8b-8649-aeed148ab64c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b46c474-64dc-4dc2-831a-a0e98fbe15ea",
                    "LayerId": "e397c280-7fd9-4442-acae-056937be14c5"
                }
            ]
        },
        {
            "id": "be4d5c30-945b-4868-92d7-1bb46c2de085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdb743ce-1e29-4b19-8f5a-8069fc714a55",
            "compositeImage": {
                "id": "a66dfd33-a845-4745-9cff-0b83af9900ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4d5c30-945b-4868-92d7-1bb46c2de085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db60fff5-3c35-4ab6-b2cc-66f9c633b962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4d5c30-945b-4868-92d7-1bb46c2de085",
                    "LayerId": "e397c280-7fd9-4442-acae-056937be14c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "e397c280-7fd9-4442-acae-056937be14c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdb743ce-1e29-4b19-8f5a-8069fc714a55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 13,
    "yorig": 19
}