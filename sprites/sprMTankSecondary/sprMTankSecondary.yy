{
    "id": "6e39d914-974c-4d2f-ba08-38c1b8f13729",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMTankSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1248b8fb-d2e0-4d08-bb05-f9624c40bdb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e39d914-974c-4d2f-ba08-38c1b8f13729",
            "compositeImage": {
                "id": "40507507-3f67-48cc-aea9-5f7b9c2bdc23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1248b8fb-d2e0-4d08-bb05-f9624c40bdb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d548ac8-c299-40af-8752-727bc0ec47b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1248b8fb-d2e0-4d08-bb05-f9624c40bdb0",
                    "LayerId": "dcf91be1-0944-47fe-92a5-bae8cf5a725e"
                }
            ]
        },
        {
            "id": "72ab6f2a-5890-44be-a7d4-33a45bead928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e39d914-974c-4d2f-ba08-38c1b8f13729",
            "compositeImage": {
                "id": "9854f7a5-aa2f-4273-9e5b-a18178749e00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72ab6f2a-5890-44be-a7d4-33a45bead928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61d7e4e5-e9ea-4ee3-adeb-a2d6b8996166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ab6f2a-5890-44be-a7d4-33a45bead928",
                    "LayerId": "dcf91be1-0944-47fe-92a5-bae8cf5a725e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dcf91be1-0944-47fe-92a5-bae8cf5a725e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e39d914-974c-4d2f-ba08-38c1b8f13729",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}