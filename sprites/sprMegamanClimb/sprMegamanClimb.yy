{
    "id": "73d64315-c746-48bb-b904-8e6feb50fe2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f950d95-07a1-4755-93ba-469ff322ff41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73d64315-c746-48bb-b904-8e6feb50fe2f",
            "compositeImage": {
                "id": "2191ac31-bbb9-44e2-ab97-36547a8e0669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f950d95-07a1-4755-93ba-469ff322ff41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2f62f6f-5fa2-4966-ad4d-6464c0594492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f950d95-07a1-4755-93ba-469ff322ff41",
                    "LayerId": "64894438-375c-44e8-94d2-f8e16010c9f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "64894438-375c-44e8-94d2-f8e16010c9f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73d64315-c746-48bb-b904-8e6feb50fe2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 7
}