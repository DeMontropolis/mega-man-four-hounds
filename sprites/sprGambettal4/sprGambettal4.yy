{
    "id": "6dce1245-4da2-42b3-9ba9-ba1fd6e498cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGambettal4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4835f7e2-28d2-421c-a4b8-8a8f95535525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dce1245-4da2-42b3-9ba9-ba1fd6e498cd",
            "compositeImage": {
                "id": "1b10e259-4500-4326-91b5-a7495c4799f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4835f7e2-28d2-421c-a4b8-8a8f95535525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd7d7be-dd53-4a69-8b64-a3be4549662e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4835f7e2-28d2-421c-a4b8-8a8f95535525",
                    "LayerId": "37934b52-6cc1-4d00-abbc-2755232ea9c1"
                }
            ]
        },
        {
            "id": "270299f4-d62a-43b6-aa97-5c9b180473a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dce1245-4da2-42b3-9ba9-ba1fd6e498cd",
            "compositeImage": {
                "id": "e17a6610-10fc-4a89-ab1f-554200b9aa96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270299f4-d62a-43b6-aa97-5c9b180473a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e72d8ee-1d4c-4211-b776-97eb6e8feff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270299f4-d62a-43b6-aa97-5c9b180473a9",
                    "LayerId": "37934b52-6cc1-4d00-abbc-2755232ea9c1"
                }
            ]
        },
        {
            "id": "188acb20-cec2-48ec-83fc-b448f062e7e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dce1245-4da2-42b3-9ba9-ba1fd6e498cd",
            "compositeImage": {
                "id": "b1cccd9f-0fc0-4a88-9cce-9ceef73d76f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "188acb20-cec2-48ec-83fc-b448f062e7e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90607d64-c574-46cf-b59c-8e68ed11b7a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "188acb20-cec2-48ec-83fc-b448f062e7e1",
                    "LayerId": "37934b52-6cc1-4d00-abbc-2755232ea9c1"
                }
            ]
        },
        {
            "id": "97abe318-2ee4-4ea2-95bb-86716f516940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dce1245-4da2-42b3-9ba9-ba1fd6e498cd",
            "compositeImage": {
                "id": "b5862e4d-8843-4569-b776-cf9b58fefcbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97abe318-2ee4-4ea2-95bb-86716f516940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe5f6e8b-440f-4a56-9de9-0ab8d1d28463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97abe318-2ee4-4ea2-95bb-86716f516940",
                    "LayerId": "37934b52-6cc1-4d00-abbc-2755232ea9c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "37934b52-6cc1-4d00-abbc-2755232ea9c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6dce1245-4da2-42b3-9ba9-ba1fd6e498cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 8,
    "yorig": 11
}