{
    "id": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponIconsGray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d08964b-1328-4b68-9a8c-38cf77b5b489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "compositeImage": {
                "id": "3ee9ca51-098e-4c99-9122-42e6a4a8a6da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d08964b-1328-4b68-9a8c-38cf77b5b489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc0abca-5258-4958-ad27-1faed2d5abe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d08964b-1328-4b68-9a8c-38cf77b5b489",
                    "LayerId": "f6a7348a-c189-4334-b4c7-0b94aa37674a"
                }
            ]
        },
        {
            "id": "ad30ab46-640f-4410-a620-6c943300088a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "compositeImage": {
                "id": "3033d406-35fc-417f-a376-d44a0be5147e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad30ab46-640f-4410-a620-6c943300088a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac524ef-2880-41a1-bcd9-9ca2b4cd7eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad30ab46-640f-4410-a620-6c943300088a",
                    "LayerId": "f6a7348a-c189-4334-b4c7-0b94aa37674a"
                }
            ]
        },
        {
            "id": "ed7677ad-671c-4906-b9d7-1e71172b1c31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "compositeImage": {
                "id": "288439f0-b8ea-41ce-b6c2-1e6202669237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed7677ad-671c-4906-b9d7-1e71172b1c31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e626ae0-2991-4f7b-85bc-20c5da94a418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed7677ad-671c-4906-b9d7-1e71172b1c31",
                    "LayerId": "f6a7348a-c189-4334-b4c7-0b94aa37674a"
                }
            ]
        },
        {
            "id": "cda3d5e4-a3e9-4528-a679-49cb1a8d6338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "compositeImage": {
                "id": "191405f5-374f-4d74-9944-de5f9e0b00d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda3d5e4-a3e9-4528-a679-49cb1a8d6338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f093708-0d21-4e8c-bbd5-46f99ecb830b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda3d5e4-a3e9-4528-a679-49cb1a8d6338",
                    "LayerId": "f6a7348a-c189-4334-b4c7-0b94aa37674a"
                }
            ]
        },
        {
            "id": "dab307aa-3d1e-42cb-b0f1-f2192db04cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "compositeImage": {
                "id": "5fb84c0a-df95-4c83-a593-ac3ca4e73902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dab307aa-3d1e-42cb-b0f1-f2192db04cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbd0159e-17ab-48d0-8e9d-ed68bf4e0f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dab307aa-3d1e-42cb-b0f1-f2192db04cf4",
                    "LayerId": "f6a7348a-c189-4334-b4c7-0b94aa37674a"
                }
            ]
        },
        {
            "id": "263224cd-b63e-4b0d-a205-95024dc68e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "compositeImage": {
                "id": "5145cf42-3645-467f-9824-25e6c8fd8ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "263224cd-b63e-4b0d-a205-95024dc68e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b209422-2257-4a5c-b8b2-75dbc7d31248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "263224cd-b63e-4b0d-a205-95024dc68e2d",
                    "LayerId": "f6a7348a-c189-4334-b4c7-0b94aa37674a"
                }
            ]
        },
        {
            "id": "2f34c87e-e7d7-4987-aaa6-5f95bfa9cc3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "compositeImage": {
                "id": "d2121403-6f4a-4145-b604-2d44e8488e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f34c87e-e7d7-4987-aaa6-5f95bfa9cc3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85fb0042-dc6f-42f0-bf10-65ed052b0141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f34c87e-e7d7-4987-aaa6-5f95bfa9cc3c",
                    "LayerId": "f6a7348a-c189-4334-b4c7-0b94aa37674a"
                }
            ]
        },
        {
            "id": "5ca212d9-8783-4303-8927-491478a71390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "compositeImage": {
                "id": "a84f03fd-4e9f-43e6-b30a-4b7eebb9a6d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ca212d9-8783-4303-8927-491478a71390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2135123a-5b32-4942-befa-8f6ab7d074c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ca212d9-8783-4303-8927-491478a71390",
                    "LayerId": "f6a7348a-c189-4334-b4c7-0b94aa37674a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f6a7348a-c189-4334-b4c7-0b94aa37674a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0df6180e-8747-45bc-82b8-9e8c5aeb0065",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}