{
    "id": "133330a1-cb16-4eac-bc23-1c6127403332",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandShootOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43a0cfa5-03ff-4eff-b00f-a4bbee069629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "133330a1-cb16-4eac-bc23-1c6127403332",
            "compositeImage": {
                "id": "f170ef2f-e840-423b-87fd-d67c6abe5cfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a0cfa5-03ff-4eff-b00f-a4bbee069629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a390d5e4-1e1c-4619-b4d3-547bae0a6398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a0cfa5-03ff-4eff-b00f-a4bbee069629",
                    "LayerId": "fc638f80-cfd5-4fca-a29f-08dcd77ba265"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "fc638f80-cfd5-4fca-a29f-08dcd77ba265",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "133330a1-cb16-4eac-bc23-1c6127403332",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 10,
    "yorig": 7
}