{
    "id": "cbc54415-8d82-429f-91b6-e09a48e1a8a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgPass",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 224,
    "bbox_left": 0,
    "bbox_right": 256,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28cae722-53d7-4151-b0ea-ac97bb53e534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbc54415-8d82-429f-91b6-e09a48e1a8a7",
            "compositeImage": {
                "id": "e3029835-1efa-48df-a14b-1cc4e2a8d6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28cae722-53d7-4151-b0ea-ac97bb53e534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0525610b-9305-41cf-82e3-3aec758b9dfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28cae722-53d7-4151-b0ea-ac97bb53e534",
                    "LayerId": "bc2ec960-d8de-4ccf-b661-826bfb3432e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 225,
    "layers": [
        {
            "id": "bc2ec960-d8de-4ccf-b661-826bfb3432e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbc54415-8d82-429f-91b6-e09a48e1a8a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 257,
    "xorig": 0,
    "yorig": 0
}