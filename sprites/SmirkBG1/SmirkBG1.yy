{
    "id": "54db91cc-acb6-4ec9-bdbb-afbccd7c42b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SmirkBG1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8e117b0-c578-4184-bb73-a94e939b655b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54db91cc-acb6-4ec9-bdbb-afbccd7c42b4",
            "compositeImage": {
                "id": "f16374c5-574c-4f21-9113-edd46601064b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8e117b0-c578-4184-bb73-a94e939b655b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1402f5-1f0c-4eca-8dc3-2cb759670e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8e117b0-c578-4184-bb73-a94e939b655b",
                    "LayerId": "4453415f-9421-4bd5-a273-229b6fdbda61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "4453415f-9421-4bd5-a273-229b6fdbda61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54db91cc-acb6-4ec9-bdbb-afbccd7c42b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}