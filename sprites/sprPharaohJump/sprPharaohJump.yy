{
    "id": "46f26ff7-8405-40dc-9f22-49bbbce77a1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "339d1923-bb67-4ee8-8e9a-0cd188bef7de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46f26ff7-8405-40dc-9f22-49bbbce77a1f",
            "compositeImage": {
                "id": "08d67bdd-4b66-4e82-a94d-0b68a83fff25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "339d1923-bb67-4ee8-8e9a-0cd188bef7de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8248ece-93df-4a4c-a66e-001c21a574e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "339d1923-bb67-4ee8-8e9a-0cd188bef7de",
                    "LayerId": "9ee4fdef-7813-4617-b293-a1ebe7fcb4d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9ee4fdef-7813-4617-b293-a1ebe7fcb4d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46f26ff7-8405-40dc-9f22-49bbbce77a1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 16,
    "yorig": 16
}