{
    "id": "c7e18a1b-0564-4ec1-8fc5-952e91f83a16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBusterShotCharged",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e36e667e-0018-4c60-a6bc-4acf12b2cea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7e18a1b-0564-4ec1-8fc5-952e91f83a16",
            "compositeImage": {
                "id": "5b8796b1-f936-4599-9ebe-b6ed6d74216f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e36e667e-0018-4c60-a6bc-4acf12b2cea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec9363e-147b-4596-8280-5ba66d4d1b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e36e667e-0018-4c60-a6bc-4acf12b2cea0",
                    "LayerId": "16db582d-dd22-4f76-9d5d-487e0bd8c3a2"
                }
            ]
        },
        {
            "id": "c5dc0a64-d17e-4bd1-80ab-1d2e501a110b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7e18a1b-0564-4ec1-8fc5-952e91f83a16",
            "compositeImage": {
                "id": "acbd77aa-5a93-4d1d-b91e-c725352776e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5dc0a64-d17e-4bd1-80ab-1d2e501a110b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1c3558-b59b-4484-89a1-63db15b06423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5dc0a64-d17e-4bd1-80ab-1d2e501a110b",
                    "LayerId": "16db582d-dd22-4f76-9d5d-487e0bd8c3a2"
                }
            ]
        },
        {
            "id": "bae78a11-e3f5-41a3-a5d3-043a8ee097ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7e18a1b-0564-4ec1-8fc5-952e91f83a16",
            "compositeImage": {
                "id": "d3a04689-3364-47d0-98e1-6f7d62f2284f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae78a11-e3f5-41a3-a5d3-043a8ee097ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56564ed1-8018-4deb-9eb2-66aefd25b3b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae78a11-e3f5-41a3-a5d3-043a8ee097ac",
                    "LayerId": "16db582d-dd22-4f76-9d5d-487e0bd8c3a2"
                }
            ]
        },
        {
            "id": "ca6de6df-16b4-42a1-9f72-f89ac1cb2685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7e18a1b-0564-4ec1-8fc5-952e91f83a16",
            "compositeImage": {
                "id": "af9c1b18-9e96-458b-ad35-0135ff8dc817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6de6df-16b4-42a1-9f72-f89ac1cb2685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc4943f3-5f93-4337-b792-36635c78b3e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6de6df-16b4-42a1-9f72-f89ac1cb2685",
                    "LayerId": "16db582d-dd22-4f76-9d5d-487e0bd8c3a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "16db582d-dd22-4f76-9d5d-487e0bd8c3a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7e18a1b-0564-4ec1-8fc5-952e91f83a16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 0.3,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 10
}