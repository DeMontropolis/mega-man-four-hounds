{
    "id": "149de97c-787a-4b55-9362-3fad7eafa1f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkShootOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8110da8-79b5-4fac-9089-66b15838a848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149de97c-787a-4b55-9362-3fad7eafa1f1",
            "compositeImage": {
                "id": "8903ba56-d771-430f-bdf9-7539c19d3d01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8110da8-79b5-4fac-9089-66b15838a848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b8cfeb-b603-4412-85ec-7370644cce55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8110da8-79b5-4fac-9089-66b15838a848",
                    "LayerId": "6ac7a247-a62c-4051-b569-0309ac4fd495"
                }
            ]
        },
        {
            "id": "efe8960f-c529-46fb-94db-124aa5e2f2d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149de97c-787a-4b55-9362-3fad7eafa1f1",
            "compositeImage": {
                "id": "ee343699-1bc9-4078-963c-1bc493ce130c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe8960f-c529-46fb-94db-124aa5e2f2d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907ce0e5-c885-4496-b6af-f23e6c8a6041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe8960f-c529-46fb-94db-124aa5e2f2d2",
                    "LayerId": "6ac7a247-a62c-4051-b569-0309ac4fd495"
                }
            ]
        },
        {
            "id": "92af1e17-f973-4549-b388-55c52b738625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149de97c-787a-4b55-9362-3fad7eafa1f1",
            "compositeImage": {
                "id": "8c89a449-cbf1-4c6f-83a2-d0324eba0227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92af1e17-f973-4549-b388-55c52b738625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e51851f1-d107-4e01-b0d8-ea159ce7506c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92af1e17-f973-4549-b388-55c52b738625",
                    "LayerId": "6ac7a247-a62c-4051-b569-0309ac4fd495"
                }
            ]
        },
        {
            "id": "14f126f3-4632-4c6e-9df4-8d2bf9148b37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149de97c-787a-4b55-9362-3fad7eafa1f1",
            "compositeImage": {
                "id": "a331cf05-a604-4ebb-be7a-934a585f6b81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14f126f3-4632-4c6e-9df4-8d2bf9148b37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de1d313-900c-4f47-a673-8b22147f00e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14f126f3-4632-4c6e-9df4-8d2bf9148b37",
                    "LayerId": "6ac7a247-a62c-4051-b569-0309ac4fd495"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6ac7a247-a62c-4051-b569-0309ac4fd495",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "149de97c-787a-4b55-9362-3fad7eafa1f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 11,
    "yorig": 7
}