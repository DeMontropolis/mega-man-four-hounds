{
    "id": "dadffa24-9801-48dd-b75c-50686e65f1e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohCharge2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6872757c-378f-4149-95e3-18f1b63b1de0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dadffa24-9801-48dd-b75c-50686e65f1e8",
            "compositeImage": {
                "id": "b5c977ad-2d82-4f3f-b6ee-4c256592294b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6872757c-378f-4149-95e3-18f1b63b1de0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7575d02-e2be-4f3b-b425-0707c6190d44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6872757c-378f-4149-95e3-18f1b63b1de0",
                    "LayerId": "d501b67e-4310-436e-be96-78700c930352"
                }
            ]
        },
        {
            "id": "9f16c7b9-0b94-451e-b67d-b582474beb4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dadffa24-9801-48dd-b75c-50686e65f1e8",
            "compositeImage": {
                "id": "7cbdb117-1380-4200-aca3-2ce6e62a680f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f16c7b9-0b94-451e-b67d-b582474beb4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afbb4a49-7b91-4d61-a09b-c17ee8b5d161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f16c7b9-0b94-451e-b67d-b582474beb4a",
                    "LayerId": "d501b67e-4310-436e-be96-78700c930352"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "d501b67e-4310-436e-be96-78700c930352",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dadffa24-9801-48dd-b75c-50686e65f1e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 13,
    "yorig": 19
}