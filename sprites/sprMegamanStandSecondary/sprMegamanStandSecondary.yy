{
    "id": "7c7ca050-c7af-4ba7-857a-c7e2c03e1f45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 4,
    "bbox_right": 16,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ac1d758-3d9e-46a4-bb92-011e0507a19d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c7ca050-c7af-4ba7-857a-c7e2c03e1f45",
            "compositeImage": {
                "id": "13b3f5df-8aac-434c-a5c8-7b1682b1c9c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ac1d758-3d9e-46a4-bb92-011e0507a19d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ddbfec6-6fc2-4313-a926-e2afc149849a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ac1d758-3d9e-46a4-bb92-011e0507a19d",
                    "LayerId": "6354d04c-0f47-41b3-ade2-6526c1fe3ec9"
                }
            ]
        },
        {
            "id": "da3f7cac-d99f-4a6a-b97f-0f29bd0aac05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c7ca050-c7af-4ba7-857a-c7e2c03e1f45",
            "compositeImage": {
                "id": "9f359781-59c6-48c2-b34e-a8c0915e2d1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da3f7cac-d99f-4a6a-b97f-0f29bd0aac05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6dcf09e-2cc3-4e49-9f15-76c630164984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da3f7cac-d99f-4a6a-b97f-0f29bd0aac05",
                    "LayerId": "6354d04c-0f47-41b3-ade2-6526c1fe3ec9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6354d04c-0f47-41b3-ade2-6526c1fe3ec9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c7ca050-c7af-4ba7-857a-c7e2c03e1f45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 7
}