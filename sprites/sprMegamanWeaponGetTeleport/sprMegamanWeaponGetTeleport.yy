{
    "id": "0ede8cf2-64d2-4cd0-8ddc-808a9edfc29a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWeaponGetTeleport",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d379c44b-14fc-4986-847c-c49629b195dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ede8cf2-64d2-4cd0-8ddc-808a9edfc29a",
            "compositeImage": {
                "id": "2427091b-1c0d-47e8-9b1a-3b8f699c520f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d379c44b-14fc-4986-847c-c49629b195dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2123cd7d-a184-487d-8cf5-18a72f1cbffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d379c44b-14fc-4986-847c-c49629b195dd",
                    "LayerId": "2f66752a-6b35-42c2-b9aa-d2e8fa9956c6"
                }
            ]
        },
        {
            "id": "6b774f64-c395-4687-a9d8-c7ac7dfb240a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ede8cf2-64d2-4cd0-8ddc-808a9edfc29a",
            "compositeImage": {
                "id": "3b65f35c-af31-47cd-bc76-cafceab84c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b774f64-c395-4687-a9d8-c7ac7dfb240a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbd754c1-6908-4cf1-8daa-94dfa42d460d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b774f64-c395-4687-a9d8-c7ac7dfb240a",
                    "LayerId": "2f66752a-6b35-42c2-b9aa-d2e8fa9956c6"
                }
            ]
        },
        {
            "id": "e9bf1968-58f4-4ce4-8c8e-63f5b55fb5ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ede8cf2-64d2-4cd0-8ddc-808a9edfc29a",
            "compositeImage": {
                "id": "8616b36e-c76a-4c7a-8c9e-5f6413270490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9bf1968-58f4-4ce4-8c8e-63f5b55fb5ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d82ffb3-5096-4a85-8447-941d42538ef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9bf1968-58f4-4ce4-8c8e-63f5b55fb5ed",
                    "LayerId": "2f66752a-6b35-42c2-b9aa-d2e8fa9956c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "2f66752a-6b35-42c2-b9aa-d2e8fa9956c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ede8cf2-64d2-4cd0-8ddc-808a9edfc29a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 40
}