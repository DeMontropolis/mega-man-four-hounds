{
    "id": "e5723155-f2e7-4f4d-a8cc-2eb49507728c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionArrowLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f25e596-4d24-429c-b93b-85339fc48fd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5723155-f2e7-4f4d-a8cc-2eb49507728c",
            "compositeImage": {
                "id": "c15a73de-f603-4481-8858-16fe98536575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f25e596-4d24-429c-b93b-85339fc48fd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb8ab0e3-7226-4fb2-948a-e9c7df516df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f25e596-4d24-429c-b93b-85339fc48fd2",
                    "LayerId": "fa4c0c78-32db-4b1b-959c-6c772cd9aea2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fa4c0c78-32db-4b1b-959c-6c772cd9aea2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5723155-f2e7-4f4d-a8cc-2eb49507728c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}