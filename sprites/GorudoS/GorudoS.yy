{
    "id": "aa81a503-6eac-469b-8472-b25309954a85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "GorudoS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13a32cd0-3925-4086-bb88-cb54bff2a797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa81a503-6eac-469b-8472-b25309954a85",
            "compositeImage": {
                "id": "ffedcc85-2c0a-4db2-858c-be14db593b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a32cd0-3925-4086-bb88-cb54bff2a797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b210ae-60c6-48c3-87a7-b4df96867738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a32cd0-3925-4086-bb88-cb54bff2a797",
                    "LayerId": "4c415231-40d1-4270-9861-da9f9edd6702"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "4c415231-40d1-4270-9861-da9f9edd6702",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa81a503-6eac-469b-8472-b25309954a85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 16
}