{
    "id": "f4f22a64-ebc0-4033-b141-4b094296430a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "001d7d62-4215-45dc-b109-0255732efbdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f22a64-ebc0-4033-b141-4b094296430a",
            "compositeImage": {
                "id": "410f1cd3-cab3-4f39-8910-4a958bbb427b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "001d7d62-4215-45dc-b109-0255732efbdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c99b18da-308b-4f7f-8e35-f890f273f1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "001d7d62-4215-45dc-b109-0255732efbdf",
                    "LayerId": "ab9f2aa4-b2f2-4f71-a7dd-4d12c4a391e7"
                }
            ]
        },
        {
            "id": "1c329b42-8e41-4be6-806a-0de201a0a67c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f22a64-ebc0-4033-b141-4b094296430a",
            "compositeImage": {
                "id": "cd4a05c7-26ee-4130-a9fb-aba7387538fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c329b42-8e41-4be6-806a-0de201a0a67c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eede71e6-539d-4873-9a39-0a19140c03ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c329b42-8e41-4be6-806a-0de201a0a67c",
                    "LayerId": "ab9f2aa4-b2f2-4f71-a7dd-4d12c4a391e7"
                }
            ]
        },
        {
            "id": "6b6796e6-7982-47ae-b2ed-f76a55b95ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f22a64-ebc0-4033-b141-4b094296430a",
            "compositeImage": {
                "id": "18acd5f6-fa13-4589-9d83-e581f6630f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b6796e6-7982-47ae-b2ed-f76a55b95ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc24b6d7-0e72-40bd-9bea-edbd33e12cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b6796e6-7982-47ae-b2ed-f76a55b95ad0",
                    "LayerId": "ab9f2aa4-b2f2-4f71-a7dd-4d12c4a391e7"
                }
            ]
        },
        {
            "id": "7d2cd7ae-2000-49a9-903d-67640ea035f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f22a64-ebc0-4033-b141-4b094296430a",
            "compositeImage": {
                "id": "9f9614bc-9836-4c51-801f-6b0809012e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2cd7ae-2000-49a9-903d-67640ea035f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a077e5e-2635-489c-8ea5-0678c030d322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2cd7ae-2000-49a9-903d-67640ea035f5",
                    "LayerId": "ab9f2aa4-b2f2-4f71-a7dd-4d12c4a391e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ab9f2aa4-b2f2-4f71-a7dd-4d12c4a391e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4f22a64-ebc0-4033-b141-4b094296430a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 11,
    "yorig": 7
}