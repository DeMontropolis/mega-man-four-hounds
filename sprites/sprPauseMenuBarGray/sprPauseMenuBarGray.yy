{
    "id": "2c231143-6e30-4674-8ea9-9706a82d6b59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPauseMenuBarGray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd9cac7b-b49c-46d1-9666-042e2f2a8f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "7711baf3-6fb1-4aeb-9eb4-bc4058661824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd9cac7b-b49c-46d1-9666-042e2f2a8f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "594786d6-c665-4391-a4f7-fcbac724123f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd9cac7b-b49c-46d1-9666-042e2f2a8f36",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "a9db0194-913b-46d8-8b8d-57dab601fd48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "1e0a8a7c-bde3-4f02-9fe0-8ab45bd53736",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9db0194-913b-46d8-8b8d-57dab601fd48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2feba0ef-f812-4339-b797-0d7e207d5208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9db0194-913b-46d8-8b8d-57dab601fd48",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "09ae91ca-2924-4e46-9bab-5cb83d0969f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "9db8146f-9261-4ba3-a593-a668c3a0142c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ae91ca-2924-4e46-9bab-5cb83d0969f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67fb74cb-8914-45ce-8e61-c2e5ef2b0231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ae91ca-2924-4e46-9bab-5cb83d0969f7",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "579b48be-e109-49e8-9279-b0b7668e660f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "da946c0b-7a98-4dbd-92f1-71de15fc6779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "579b48be-e109-49e8-9279-b0b7668e660f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4d25add-4633-47ce-8f00-4286732ab96d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "579b48be-e109-49e8-9279-b0b7668e660f",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "88e88aee-1a6a-479c-a0b1-21de816ce55e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "2c78ecb1-820e-4992-b24e-4eb7f3c345e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88e88aee-1a6a-479c-a0b1-21de816ce55e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5036d062-d18c-4ddd-b049-46d4d2b13ae8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88e88aee-1a6a-479c-a0b1-21de816ce55e",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "6f996730-8484-4f9b-b48b-dbf901f6e5a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "2b9abdb1-31ec-4308-b654-7052ce4b5b62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f996730-8484-4f9b-b48b-dbf901f6e5a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "188e07b7-ea74-432d-aafa-16d8664eb206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f996730-8484-4f9b-b48b-dbf901f6e5a7",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "3c3bdc57-7482-4c01-a778-89ca91993d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "d293ca4d-fe1c-41ca-be97-2e36e31382b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c3bdc57-7482-4c01-a778-89ca91993d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba76290a-dad8-4f4f-9a8d-dea95d6d716c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c3bdc57-7482-4c01-a778-89ca91993d90",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "73a2e320-b6b4-484f-ae72-76eaf9674a45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "830dd3cb-937a-47e3-bdf2-9691dbbc847c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a2e320-b6b4-484f-ae72-76eaf9674a45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0856ee1c-a1ca-4824-9461-0d8743de0cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a2e320-b6b4-484f-ae72-76eaf9674a45",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "224e3793-5691-4614-a55d-8d14afd60480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "9b4bb807-4b1c-4f67-8647-b5a556827331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "224e3793-5691-4614-a55d-8d14afd60480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4838c0b7-cfbc-4657-80ab-3c085bbb3616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "224e3793-5691-4614-a55d-8d14afd60480",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "50900242-6934-4f98-b1ce-fcdbc4d0251a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "8cb6eb40-1f13-4bd7-b087-8f9caa3d4ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50900242-6934-4f98-b1ce-fcdbc4d0251a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd280c4-803e-4d28-a2f2-5d6380fbc5e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50900242-6934-4f98-b1ce-fcdbc4d0251a",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "5ad8815c-de90-4760-a7b3-92e69356c4e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "d962019b-7090-4342-86ac-1e2e7009d1e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad8815c-de90-4760-a7b3-92e69356c4e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3553ac91-3be6-497b-a87c-4213ee456667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad8815c-de90-4760-a7b3-92e69356c4e5",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "a78a86b7-3b13-4b8c-b4ed-61696ac9508f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "0a293b48-7490-47e1-b4a8-709560ecec34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a78a86b7-3b13-4b8c-b4ed-61696ac9508f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7129a452-871c-407f-bff5-66b166abce65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a78a86b7-3b13-4b8c-b4ed-61696ac9508f",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "b7329eb3-2b87-4fd1-a232-811b40b5cdb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "a0a6400d-93d1-486a-a3c5-520b69451b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7329eb3-2b87-4fd1-a232-811b40b5cdb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c0b328-e664-4d88-ab22-4029b2abc473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7329eb3-2b87-4fd1-a232-811b40b5cdb5",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "9f99a2c6-3adc-4b4e-b458-86985bc4fbb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "8885ebb9-afcf-4393-94c5-8a122b7a595a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f99a2c6-3adc-4b4e-b458-86985bc4fbb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4284a512-33e8-4b17-86f5-369954499eb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f99a2c6-3adc-4b4e-b458-86985bc4fbb0",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "23628d42-468b-4d9b-ad6c-75aabeb15500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "94f0a06d-582a-4d74-9dae-07ff31e05e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23628d42-468b-4d9b-ad6c-75aabeb15500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1a1a0e6-7ea7-453a-be79-eb40ac1568e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23628d42-468b-4d9b-ad6c-75aabeb15500",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "d2fb6c8e-5adf-4519-b3b9-11123a38e5b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "af4cf8bf-6d33-4c45-a7ca-ad75555e41af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2fb6c8e-5adf-4519-b3b9-11123a38e5b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9afb47d4-0d42-4192-a2f0-db874a25825c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2fb6c8e-5adf-4519-b3b9-11123a38e5b5",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "a07841e2-65ca-417f-b89f-ea48c4932ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "03cc26b3-aead-4fc6-a09d-827cce9ad5c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07841e2-65ca-417f-b89f-ea48c4932ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bde082d-7959-4264-8473-01f3ca3f09d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07841e2-65ca-417f-b89f-ea48c4932ab7",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "39886364-b149-4d98-98c9-d3c46ab6bb1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "e0a7c69c-758a-4a29-9268-654a35de3b2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39886364-b149-4d98-98c9-d3c46ab6bb1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f470a0c-cc78-4d8d-be7d-61cef99608bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39886364-b149-4d98-98c9-d3c46ab6bb1b",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "909945da-6912-49a7-aa5d-c14853179384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "24df7d58-0ef3-43e6-b27c-6dc993367fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909945da-6912-49a7-aa5d-c14853179384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef58b65f-f305-4b6b-bf9d-be7001d5989a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909945da-6912-49a7-aa5d-c14853179384",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "a3323285-56c9-43cc-9b66-0f6a9e9eccd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "24aebbcf-589f-4adf-95a3-b35da502bb70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3323285-56c9-43cc-9b66-0f6a9e9eccd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77df1e54-2624-4eca-96c9-ddb4dc0343ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3323285-56c9-43cc-9b66-0f6a9e9eccd6",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "8e044716-389d-48d6-bc7e-15897b2fd3d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "a435dbc9-349d-4067-b2cf-548d956aecda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e044716-389d-48d6-bc7e-15897b2fd3d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4248ee39-c86c-4521-b5ef-08a8d4edcd4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e044716-389d-48d6-bc7e-15897b2fd3d0",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "c6799d90-ea0b-4fee-8e79-52d9c41c4d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "e49a6d2f-0630-4a6a-b48b-3ad788805764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6799d90-ea0b-4fee-8e79-52d9c41c4d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de149331-31ca-43d7-a744-39a89cf9ff08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6799d90-ea0b-4fee-8e79-52d9c41c4d2e",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "04966470-66a7-4c7e-a2aa-8175793b074f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "e2905c54-98c8-495f-b641-33032f21eaa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04966470-66a7-4c7e-a2aa-8175793b074f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3acff596-5da2-47de-81d7-046134dfe3c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04966470-66a7-4c7e-a2aa-8175793b074f",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "4350ff61-d26b-4ced-9df2-f5296dc16444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "5a1bc807-240b-45d9-8813-8203ca1ffbaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4350ff61-d26b-4ced-9df2-f5296dc16444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86c789e4-00ef-4262-aee2-04178a915fd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4350ff61-d26b-4ced-9df2-f5296dc16444",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "81b768ad-4750-4649-adb0-ea2a14cbf7e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "c17e9eb3-c551-42c1-8b75-765e96d636d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b768ad-4750-4649-adb0-ea2a14cbf7e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f218ab1-9ef2-419f-8a6a-1738a52f95ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b768ad-4750-4649-adb0-ea2a14cbf7e5",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "faabe93b-7112-4467-930d-ac367958d2fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "664b2365-8572-4792-b030-0e894c819dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faabe93b-7112-4467-930d-ac367958d2fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "583e4349-b9d6-4da2-bf0e-1bb9bc3bd9ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faabe93b-7112-4467-930d-ac367958d2fe",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "6caed737-008a-40d2-b1b9-bd37e3130626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "b190c2a4-1815-42c3-b994-b8afc050d77a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6caed737-008a-40d2-b1b9-bd37e3130626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10416b70-0368-425a-9774-7bdde863e1e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6caed737-008a-40d2-b1b9-bd37e3130626",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "bda7cdc9-854f-4168-b777-90c58a79c2ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "ee4875f6-5d17-4d01-8ce9-afda1cec68bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bda7cdc9-854f-4168-b777-90c58a79c2ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "392ddb4b-2b06-4408-becd-0496dbf8577f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bda7cdc9-854f-4168-b777-90c58a79c2ac",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        },
        {
            "id": "222d3679-6bf7-4e73-9558-f0301b39e6ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "compositeImage": {
                "id": "b2e1c7e5-635c-4c0b-8ed6-01d129f84512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "222d3679-6bf7-4e73-9558-f0301b39e6ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2736d70a-0639-4692-809d-4eaa6d555b65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "222d3679-6bf7-4e73-9558-f0301b39e6ab",
                    "LayerId": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "2d5fa8fe-164f-4ebe-9eda-3562ceccd014",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c231143-6e30-4674-8ea9-9706a82d6b59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}