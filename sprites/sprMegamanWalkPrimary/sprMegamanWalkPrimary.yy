{
    "id": "afd47375-f91a-4106-88f1-ab2b302181c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "330a305c-4a38-452e-a2f3-a7580b0e382a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd47375-f91a-4106-88f1-ab2b302181c3",
            "compositeImage": {
                "id": "bf4a7d76-129f-4a39-9c3e-5856f82e4642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330a305c-4a38-452e-a2f3-a7580b0e382a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6ee52ed-697a-4447-a1f6-56a2d4cc73ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330a305c-4a38-452e-a2f3-a7580b0e382a",
                    "LayerId": "e674575b-a5a4-4ffc-ad80-c6d952ef31a8"
                }
            ]
        },
        {
            "id": "8ec10aef-03e5-4c42-80e0-5d308707754d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd47375-f91a-4106-88f1-ab2b302181c3",
            "compositeImage": {
                "id": "8d86b5c8-a31f-48f4-af24-a7abfb89d328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec10aef-03e5-4c42-80e0-5d308707754d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "621e6c53-97e4-4206-bc8b-d7984056c2d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec10aef-03e5-4c42-80e0-5d308707754d",
                    "LayerId": "e674575b-a5a4-4ffc-ad80-c6d952ef31a8"
                }
            ]
        },
        {
            "id": "c4bc9b5c-c32c-404c-ade5-221db6bf5bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd47375-f91a-4106-88f1-ab2b302181c3",
            "compositeImage": {
                "id": "bfb3029c-ca4e-4514-b297-aff056b6861f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4bc9b5c-c32c-404c-ade5-221db6bf5bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78aa201b-dbbf-4f2e-9040-7b8644d347db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4bc9b5c-c32c-404c-ade5-221db6bf5bbc",
                    "LayerId": "e674575b-a5a4-4ffc-ad80-c6d952ef31a8"
                }
            ]
        },
        {
            "id": "295062bb-665f-4cb6-a6b0-9aa216fdc67b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd47375-f91a-4106-88f1-ab2b302181c3",
            "compositeImage": {
                "id": "18b37e34-a491-44d3-8c3e-eed2e2365f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "295062bb-665f-4cb6-a6b0-9aa216fdc67b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f46b90-c5c4-4e06-be6f-0eb979d92bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "295062bb-665f-4cb6-a6b0-9aa216fdc67b",
                    "LayerId": "e674575b-a5a4-4ffc-ad80-c6d952ef31a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "e674575b-a5a4-4ffc-ad80-c6d952ef31a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afd47375-f91a-4106-88f1-ab2b302181c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 11,
    "yorig": 7
}