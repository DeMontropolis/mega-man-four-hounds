{
    "id": "d313797e-ba07-4362-b231-06fa7a6498f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SmirkTiles_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "330f4c44-d450-470a-9d82-6fe7b95a909c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d313797e-ba07-4362-b231-06fa7a6498f9",
            "compositeImage": {
                "id": "ba85e58d-e68d-4d5a-9d9c-0873552095bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330f4c44-d450-470a-9d82-6fe7b95a909c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b7924e-e76b-47dc-b79f-82c4c4b07d3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330f4c44-d450-470a-9d82-6fe7b95a909c",
                    "LayerId": "cb595c74-3362-4432-bba7-8116cc993fd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "cb595c74-3362-4432-bba7-8116cc993fd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d313797e-ba07-4362-b231-06fa7a6498f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}