{
    "id": "305fa195-e1b1-4067-af50-377003bc2339",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGambettal3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46f6539e-7c92-47a5-85d2-209b8cfcef4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "305fa195-e1b1-4067-af50-377003bc2339",
            "compositeImage": {
                "id": "b6cdaea8-8cc3-402a-8c4d-937e08708889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46f6539e-7c92-47a5-85d2-209b8cfcef4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc784d16-63a3-4c0d-885a-43ea6390cbd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46f6539e-7c92-47a5-85d2-209b8cfcef4c",
                    "LayerId": "eb115443-7684-4c55-bd4d-7048333d0e86"
                }
            ]
        },
        {
            "id": "25161a54-d94c-4a34-b7b1-b54d1ed6b588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "305fa195-e1b1-4067-af50-377003bc2339",
            "compositeImage": {
                "id": "5cf31c98-aa32-4aa8-837e-e34f3e97b2a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25161a54-d94c-4a34-b7b1-b54d1ed6b588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab03629-f85c-4440-9016-945c4c67bf8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25161a54-d94c-4a34-b7b1-b54d1ed6b588",
                    "LayerId": "eb115443-7684-4c55-bd4d-7048333d0e86"
                }
            ]
        },
        {
            "id": "75876fad-1c8f-402c-ae48-2836bb0013f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "305fa195-e1b1-4067-af50-377003bc2339",
            "compositeImage": {
                "id": "2ef4bebf-7a06-4522-97be-1c8a3f8e69ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75876fad-1c8f-402c-ae48-2836bb0013f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d294327-7e53-45f7-af2e-fabcbf53a055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75876fad-1c8f-402c-ae48-2836bb0013f3",
                    "LayerId": "eb115443-7684-4c55-bd4d-7048333d0e86"
                }
            ]
        },
        {
            "id": "ad3776e0-0496-4a0c-a8f9-5385a4654d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "305fa195-e1b1-4067-af50-377003bc2339",
            "compositeImage": {
                "id": "5ef8f699-e0b3-4780-b50e-d78745a060a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad3776e0-0496-4a0c-a8f9-5385a4654d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62b795c2-759b-451d-9a99-ef1b996f93dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad3776e0-0496-4a0c-a8f9-5385a4654d52",
                    "LayerId": "eb115443-7684-4c55-bd4d-7048333d0e86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "eb115443-7684-4c55-bd4d-7048333d0e86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "305fa195-e1b1-4067-af50-377003bc2339",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 8,
    "yorig": 11
}