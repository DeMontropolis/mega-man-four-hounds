{
    "id": "c5dffe83-d80e-455f-98b7-e18430c17322",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPharaohJumpShootBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0a66eb0-2840-4ac5-9c9b-7d4a52211825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5dffe83-d80e-455f-98b7-e18430c17322",
            "compositeImage": {
                "id": "f730294e-ef30-49e2-9199-5215300680c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0a66eb0-2840-4ac5-9c9b-7d4a52211825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c07e500-2381-4d35-a017-1ae39d89d417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0a66eb0-2840-4ac5-9c9b-7d4a52211825",
                    "LayerId": "21aaaad3-6878-4541-8db6-855a60bf437f"
                }
            ]
        },
        {
            "id": "9c98168b-f7a9-4ce4-aaee-6130abc36131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5dffe83-d80e-455f-98b7-e18430c17322",
            "compositeImage": {
                "id": "d5f11bc7-5b2d-4352-bc3b-520e4727bd5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c98168b-f7a9-4ce4-aaee-6130abc36131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "975927fd-84b1-4a5f-823e-ce4420197d10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c98168b-f7a9-4ce4-aaee-6130abc36131",
                    "LayerId": "21aaaad3-6878-4541-8db6-855a60bf437f"
                }
            ]
        },
        {
            "id": "446790aa-ed17-47d4-8d62-e119a1662278",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5dffe83-d80e-455f-98b7-e18430c17322",
            "compositeImage": {
                "id": "e66ad5a5-897c-49c1-b259-f0469c88a586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "446790aa-ed17-47d4-8d62-e119a1662278",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74edcf29-5d82-4dde-a806-55baa44c40e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "446790aa-ed17-47d4-8d62-e119a1662278",
                    "LayerId": "21aaaad3-6878-4541-8db6-855a60bf437f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "21aaaad3-6878-4541-8db6-855a60bf437f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5dffe83-d80e-455f-98b7-e18430c17322",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 16,
    "yorig": 24
}