{
    "id": "2b3f8727-244b-438a-9efb-38f386ab3efd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPassArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1eca8da5-eeb8-4060-9fb3-ee163f41aa07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b3f8727-244b-438a-9efb-38f386ab3efd",
            "compositeImage": {
                "id": "20a01e6b-ab2a-4d47-b1d5-517558ff83ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eca8da5-eeb8-4060-9fb3-ee163f41aa07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa7058cf-ec85-46b9-b003-226be2ff7491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eca8da5-eeb8-4060-9fb3-ee163f41aa07",
                    "LayerId": "2506698c-ba7f-444c-b396-79fbf8a2c720"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "2506698c-ba7f-444c-b396-79fbf8a2c720",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b3f8727-244b-438a-9efb-38f386ab3efd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 4,
    "yorig": 3
}