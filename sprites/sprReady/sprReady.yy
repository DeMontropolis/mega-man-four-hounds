{
    "id": "56c74b03-8517-483f-9c27-da9e3c9091b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprReady",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5471e43f-f482-4901-86a6-5089e4a378e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56c74b03-8517-483f-9c27-da9e3c9091b5",
            "compositeImage": {
                "id": "4be96244-3d64-4ca9-b52f-dd93959c71f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5471e43f-f482-4901-86a6-5089e4a378e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cbc3df9-2277-4cc6-b584-1ef258f0e111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5471e43f-f482-4901-86a6-5089e4a378e4",
                    "LayerId": "0a39b939-4628-4038-9d21-79a2d6ddedff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0a39b939-4628-4038-9d21-79a2d6ddedff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56c74b03-8517-483f-9c27-da9e3c9091b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 4
}