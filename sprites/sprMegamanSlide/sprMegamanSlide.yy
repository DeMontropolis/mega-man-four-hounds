{
    "id": "0c8d6ac9-ab1f-411e-97ac-494fe118126d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanSlide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d665fda-4f80-4f43-9eb6-5c0aac940024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c8d6ac9-ab1f-411e-97ac-494fe118126d",
            "compositeImage": {
                "id": "683e2885-dc65-495d-a74f-33225543b921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d665fda-4f80-4f43-9eb6-5c0aac940024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6893df83-d666-439b-acce-ed952f9a3ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d665fda-4f80-4f43-9eb6-5c0aac940024",
                    "LayerId": "49e1caa3-5a31-4f46-a2d8-b16f90770129"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "49e1caa3-5a31-4f46-a2d8-b16f90770129",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c8d6ac9-ab1f-411e-97ac-494fe118126d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 11,
    "yorig": 2
}