{
    "id": "2d619873-d537-415f-9bba-5cb6e732088c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanTeleportPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81b3d6f7-4047-4a4a-ba92-e37f7b346894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d619873-d537-415f-9bba-5cb6e732088c",
            "compositeImage": {
                "id": "d1e1003e-6eb5-4c41-895a-2c9e6ccb54bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b3d6f7-4047-4a4a-ba92-e37f7b346894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6937b5ca-b0ea-44dd-a0ea-7118661db763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b3d6f7-4047-4a4a-ba92-e37f7b346894",
                    "LayerId": "4fd64b60-73f7-4397-88ae-e70756ab2e29"
                }
            ]
        },
        {
            "id": "671aed59-e56a-4503-b108-60e61de5575f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d619873-d537-415f-9bba-5cb6e732088c",
            "compositeImage": {
                "id": "4c039394-0aed-42b8-be93-72c579c7dc89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "671aed59-e56a-4503-b108-60e61de5575f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85f3396b-f58e-48ce-b063-5906f365377b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "671aed59-e56a-4503-b108-60e61de5575f",
                    "LayerId": "4fd64b60-73f7-4397-88ae-e70756ab2e29"
                }
            ]
        },
        {
            "id": "3f9aec46-6d33-459c-afb9-c8cf5cee8ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d619873-d537-415f-9bba-5cb6e732088c",
            "compositeImage": {
                "id": "6f7b7153-b32f-4316-8f28-4fcbb4cd8aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f9aec46-6d33-459c-afb9-c8cf5cee8ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c3ccc4-a21d-4900-8c11-bc4414e81bf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f9aec46-6d33-459c-afb9-c8cf5cee8ec1",
                    "LayerId": "4fd64b60-73f7-4397-88ae-e70756ab2e29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4fd64b60-73f7-4397-88ae-e70756ab2e29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d619873-d537-415f-9bba-5cb6e732088c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 15
}