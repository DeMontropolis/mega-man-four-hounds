{
    "id": "fd637dac-9de6-4741-a973-482357cea16a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBoozeUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 335,
    "bbox_left": 276,
    "bbox_right": 803,
    "bbox_top": 208,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ff69b26-24a1-4cba-9ef7-d9af74464e24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd637dac-9de6-4741-a973-482357cea16a",
            "compositeImage": {
                "id": "1d283732-cf37-437d-a25d-dc314a988bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff69b26-24a1-4cba-9ef7-d9af74464e24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "936bdcdc-3d13-433b-80a1-90438fec1fb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff69b26-24a1-4cba-9ef7-d9af74464e24",
                    "LayerId": "b1f37d99-b4ea-4c6c-9d30-efbe02fc1029"
                }
            ]
        },
        {
            "id": "1cac7e99-1735-4ad5-82a2-80698933bf35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd637dac-9de6-4741-a973-482357cea16a",
            "compositeImage": {
                "id": "48270e0b-25d8-45cc-8d1a-29067cdba193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cac7e99-1735-4ad5-82a2-80698933bf35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f8fafe-b984-4d71-be8d-131379d95f70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cac7e99-1735-4ad5-82a2-80698933bf35",
                    "LayerId": "b1f37d99-b4ea-4c6c-9d30-efbe02fc1029"
                }
            ]
        },
        {
            "id": "1deaff09-76c7-46ce-b799-bfa7eca59634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd637dac-9de6-4741-a973-482357cea16a",
            "compositeImage": {
                "id": "c5a9690a-39f7-4338-b0c0-713501a64fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1deaff09-76c7-46ce-b799-bfa7eca59634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "541d477d-ffc0-4347-8fd8-4349e38c2146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1deaff09-76c7-46ce-b799-bfa7eca59634",
                    "LayerId": "b1f37d99-b4ea-4c6c-9d30-efbe02fc1029"
                }
            ]
        },
        {
            "id": "db9cf79e-9a78-4d72-b710-eabf11249280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd637dac-9de6-4741-a973-482357cea16a",
            "compositeImage": {
                "id": "8d426599-c126-4e45-b8cc-7d3c315f84b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db9cf79e-9a78-4d72-b710-eabf11249280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef857b2e-0552-4437-a55b-f5c09001d9f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db9cf79e-9a78-4d72-b710-eabf11249280",
                    "LayerId": "b1f37d99-b4ea-4c6c-9d30-efbe02fc1029"
                }
            ]
        },
        {
            "id": "75e85625-78ca-4d39-af6c-48370d75d61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd637dac-9de6-4741-a973-482357cea16a",
            "compositeImage": {
                "id": "e9a613ee-4ffe-448e-97dd-873133187e1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75e85625-78ca-4d39-af6c-48370d75d61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8650ba-7760-4039-9af9-8eaacdd360cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75e85625-78ca-4d39-af6c-48370d75d61a",
                    "LayerId": "b1f37d99-b4ea-4c6c-9d30-efbe02fc1029"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 340,
    "layers": [
        {
            "id": "b1f37d99-b4ea-4c6c-9d30-efbe02fc1029",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd637dac-9de6-4741-a973-482357cea16a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1084,
    "xorig": 0,
    "yorig": 0
}