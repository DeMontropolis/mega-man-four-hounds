{
    "id": "a38404fe-a5cd-4f51-a0b2-834ff0dc8c8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkThrowOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29c2ae1b-21cc-4ae4-8a4e-aca4cb947b4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38404fe-a5cd-4f51-a0b2-834ff0dc8c8b",
            "compositeImage": {
                "id": "65026cc3-55b6-458a-af21-c79bc5076468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c2ae1b-21cc-4ae4-8a4e-aca4cb947b4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a829a329-53e7-4418-9995-0770cec02814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c2ae1b-21cc-4ae4-8a4e-aca4cb947b4e",
                    "LayerId": "4499ae01-4eef-4c5e-a3d8-d61fd706fe4a"
                }
            ]
        },
        {
            "id": "06312190-52ef-4d5e-9401-f881a1e09074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38404fe-a5cd-4f51-a0b2-834ff0dc8c8b",
            "compositeImage": {
                "id": "7a1cf1fa-70d4-4a93-abaa-8e69055c4b30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06312190-52ef-4d5e-9401-f881a1e09074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb0486c-e4be-44aa-ac75-85aa6095efe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06312190-52ef-4d5e-9401-f881a1e09074",
                    "LayerId": "4499ae01-4eef-4c5e-a3d8-d61fd706fe4a"
                }
            ]
        },
        {
            "id": "164fb0fe-0009-40bf-bcdc-1bd3c94fa70a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38404fe-a5cd-4f51-a0b2-834ff0dc8c8b",
            "compositeImage": {
                "id": "1546c7ae-23c9-4789-a9b6-cb302760adda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164fb0fe-0009-40bf-bcdc-1bd3c94fa70a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01672d9f-90e6-4be5-86af-1b4503d3bbed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164fb0fe-0009-40bf-bcdc-1bd3c94fa70a",
                    "LayerId": "4499ae01-4eef-4c5e-a3d8-d61fd706fe4a"
                }
            ]
        },
        {
            "id": "e458dec0-16d9-4a01-bea0-582bebdc6897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38404fe-a5cd-4f51-a0b2-834ff0dc8c8b",
            "compositeImage": {
                "id": "b5872b64-493a-4680-aa2e-42ba6b991355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e458dec0-16d9-4a01-bea0-582bebdc6897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fd1e9c2-fc3f-442d-9d1d-17b3db2ea8ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e458dec0-16d9-4a01-bea0-582bebdc6897",
                    "LayerId": "4499ae01-4eef-4c5e-a3d8-d61fd706fe4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4499ae01-4eef-4c5e-a3d8-d61fd706fe4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a38404fe-a5cd-4f51-a0b2-834ff0dc8c8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 11,
    "yorig": 7
}