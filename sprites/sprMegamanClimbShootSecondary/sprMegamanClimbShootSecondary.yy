{
    "id": "bf7c5437-f3be-4ec0-ab41-b1c75e2f6ee2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbShootSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b88317b-dc56-4302-96c7-0d942062afdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf7c5437-f3be-4ec0-ab41-b1c75e2f6ee2",
            "compositeImage": {
                "id": "afe57fdd-44be-4019-ac80-984e253b773b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b88317b-dc56-4302-96c7-0d942062afdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59483c1e-7fb8-4770-89fb-13396c31453a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b88317b-dc56-4302-96c7-0d942062afdf",
                    "LayerId": "be4913ec-e327-4610-8966-1fbc7444df1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "be4913ec-e327-4610-8966-1fbc7444df1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf7c5437-f3be-4ec0-ab41-b1c75e2f6ee2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 8,
    "yorig": 7
}