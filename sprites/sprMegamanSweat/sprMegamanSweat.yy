{
    "id": "ba54e7d7-ec58-4894-9b55-09b2b1cd875e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanSweat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49eb6745-8c9a-4702-9e66-3c40f32023fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba54e7d7-ec58-4894-9b55-09b2b1cd875e",
            "compositeImage": {
                "id": "6ce4141f-1185-4bed-9008-f3bb20066d57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49eb6745-8c9a-4702-9e66-3c40f32023fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c496723-5083-49c6-9937-836033503ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49eb6745-8c9a-4702-9e66-3c40f32023fd",
                    "LayerId": "0c01db77-99e4-4c06-9821-2e37a9e88bcf"
                }
            ]
        },
        {
            "id": "f96d358d-5ea2-4087-8438-07e25fae0dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba54e7d7-ec58-4894-9b55-09b2b1cd875e",
            "compositeImage": {
                "id": "45ff1a15-a526-4139-b02e-22e2c2a3767f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f96d358d-5ea2-4087-8438-07e25fae0dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f3ec8fe-70f7-4738-9077-0794b397f6f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f96d358d-5ea2-4087-8438-07e25fae0dbe",
                    "LayerId": "0c01db77-99e4-4c06-9821-2e37a9e88bcf"
                }
            ]
        },
        {
            "id": "1c54469f-d40f-49fb-abf2-31c9f43b8a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba54e7d7-ec58-4894-9b55-09b2b1cd875e",
            "compositeImage": {
                "id": "9b8a96f8-fcc1-4605-a7d9-b7246bdfd92c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c54469f-d40f-49fb-abf2-31c9f43b8a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ac6677-f5ca-4f39-8e5e-1fd7d56472f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c54469f-d40f-49fb-abf2-31c9f43b8a40",
                    "LayerId": "0c01db77-99e4-4c06-9821-2e37a9e88bcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0c01db77-99e4-4c06-9821-2e37a9e88bcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba54e7d7-ec58-4894-9b55-09b2b1cd875e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 4
}