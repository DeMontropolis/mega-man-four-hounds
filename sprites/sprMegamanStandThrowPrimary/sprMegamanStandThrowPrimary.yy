{
    "id": "4985a2a4-81a4-4ca5-9a32-c1e56db7b8be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStandThrowPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2674203b-10de-4ddd-aeed-be208ff6ca36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4985a2a4-81a4-4ca5-9a32-c1e56db7b8be",
            "compositeImage": {
                "id": "89f92a99-4727-4093-a1d7-dc294bf7a9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2674203b-10de-4ddd-aeed-be208ff6ca36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b774206-6909-4db4-98e9-8c34680e779a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2674203b-10de-4ddd-aeed-be208ff6ca36",
                    "LayerId": "4c3e448f-a8ff-45b3-b76a-edb364589ae9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4c3e448f-a8ff-45b3-b76a-edb364589ae9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4985a2a4-81a4-4ca5-9a32-c1e56db7b8be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 10,
    "yorig": 7
}