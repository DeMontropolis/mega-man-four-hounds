{
    "id": "719b9e6e-47a1-4014-8312-1f5217858307",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionBorderRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "792dbe6e-51b9-4c3d-b612-34dd0f6439a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "719b9e6e-47a1-4014-8312-1f5217858307",
            "compositeImage": {
                "id": "e10fd36d-b800-4e1a-908c-cbfdcbefce4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "792dbe6e-51b9-4c3d-b612-34dd0f6439a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb658ab8-fdcc-497c-83d9-36c2755b3d5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "792dbe6e-51b9-4c3d-b612-34dd0f6439a3",
                    "LayerId": "86df20db-7172-445d-8dfd-3ecea9bdcf03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "86df20db-7172-445d-8dfd-3ecea9bdcf03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "719b9e6e-47a1-4014-8312-1f5217858307",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}