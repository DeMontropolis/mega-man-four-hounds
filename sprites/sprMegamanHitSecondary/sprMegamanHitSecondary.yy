{
    "id": "8e6e010b-7d38-47b6-847e-985b80072a27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanHitSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d328d7d0-b79f-4e60-a96e-e7be70b5a57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e6e010b-7d38-47b6-847e-985b80072a27",
            "compositeImage": {
                "id": "6c7f2682-dca4-442e-9e02-3607337ac393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d328d7d0-b79f-4e60-a96e-e7be70b5a57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bde5b8-2421-43bc-babf-7f6e275a16c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d328d7d0-b79f-4e60-a96e-e7be70b5a57b",
                    "LayerId": "2809f242-6537-45dd-a36e-bebf03527c74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "2809f242-6537-45dd-a36e-bebf03527c74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e6e010b-7d38-47b6-847e-985b80072a27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 7
}