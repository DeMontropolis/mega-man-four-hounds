{
    "id": "70b58572-f854-4807-999e-10af0a548954",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprAirBubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f042450b-9131-4e86-b37b-85a36c6c09d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "16ea3fd1-9d9f-4534-96cf-2fcc804935c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f042450b-9131-4e86-b37b-85a36c6c09d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66e3da6-fd34-4506-b8d3-f870e511404e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f042450b-9131-4e86-b37b-85a36c6c09d8",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        },
        {
            "id": "ec63ebf0-b5ca-4277-93ee-ac30f3a4d4d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "876ff300-50a3-4b55-a511-c7f9c261b568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec63ebf0-b5ca-4277-93ee-ac30f3a4d4d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "373a3d81-6b0e-4223-b343-8ed1e189e43e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec63ebf0-b5ca-4277-93ee-ac30f3a4d4d2",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        },
        {
            "id": "d5c39817-31da-451d-addf-23565734cb0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "8cebbfe8-b14c-474c-b2a4-7d801e5cc036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5c39817-31da-451d-addf-23565734cb0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "953fff76-6184-4b36-96f4-b4e64baacee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c39817-31da-451d-addf-23565734cb0d",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        },
        {
            "id": "9103468b-1008-46c1-8deb-3f2a271e8933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "c856dfb5-5299-4297-ae13-af6800d1ea22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9103468b-1008-46c1-8deb-3f2a271e8933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea96689a-07d6-4cd0-a536-d76fc1b738de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9103468b-1008-46c1-8deb-3f2a271e8933",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        },
        {
            "id": "78c55ac7-6aec-4fad-a0f1-94fac4acccee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "4e26e326-e536-4f9f-954e-ba8183b4557d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78c55ac7-6aec-4fad-a0f1-94fac4acccee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1cef3c-95fb-407c-8093-dac6b7b53a7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78c55ac7-6aec-4fad-a0f1-94fac4acccee",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        },
        {
            "id": "85bdb0c2-b78f-480d-98a8-f145ef534466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "93cc69a1-96d0-40fb-a22e-51607cafb0be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bdb0c2-b78f-480d-98a8-f145ef534466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a20fe7b-7730-42aa-9f9d-cba342587ae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bdb0c2-b78f-480d-98a8-f145ef534466",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        },
        {
            "id": "42c03edd-50fd-489c-b70a-572fc3242bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "d2d49ad7-4a50-40c1-a242-1a45a07818ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42c03edd-50fd-489c-b70a-572fc3242bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "663fbcfe-8aef-40ce-bba5-2b8383ff3a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42c03edd-50fd-489c-b70a-572fc3242bfd",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        },
        {
            "id": "1f2622f0-b1f4-4ce0-ae53-ac5197a50081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "1df351d3-3246-4b9c-b2bf-9721a471ae6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2622f0-b1f4-4ce0-ae53-ac5197a50081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf08f0cb-fe16-43f2-90e7-958687f7da5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2622f0-b1f4-4ce0-ae53-ac5197a50081",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        },
        {
            "id": "67b76caf-cc12-465a-8672-9143ea41680a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "compositeImage": {
                "id": "94aff75b-0c5d-45a7-9f8c-29052b61d755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67b76caf-cc12-465a-8672-9143ea41680a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "703361b1-ad25-427b-94d2-b71bbe6fc4e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67b76caf-cc12-465a-8672-9143ea41680a",
                    "LayerId": "3c30ac0a-02cb-4078-8825-3c98389d8e06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "3c30ac0a-02cb-4078-8825-3c98389d8e06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70b58572-f854-4807-999e-10af0a548954",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 2
}