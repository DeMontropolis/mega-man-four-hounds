{
    "id": "5894a27d-559c-4f47-abd4-5cd0214c7d10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbGetup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5946d4ee-d4f1-4e05-a5cc-92a3bb38a744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5894a27d-559c-4f47-abd4-5cd0214c7d10",
            "compositeImage": {
                "id": "3bb5c96b-b2e8-41dd-bc80-6fc25f23245a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5946d4ee-d4f1-4e05-a5cc-92a3bb38a744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78cec31e-0229-4ca2-b099-d7796e7c814b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5946d4ee-d4f1-4e05-a5cc-92a3bb38a744",
                    "LayerId": "54e37a0e-4197-4d5f-952d-41f601dc3b04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "54e37a0e-4197-4d5f-952d-41f601dc3b04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5894a27d-559c-4f47-abd4-5cd0214c7d10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 5
}