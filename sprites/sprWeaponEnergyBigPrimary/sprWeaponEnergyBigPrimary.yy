{
    "id": "a78499b6-ebf8-46f7-a852-b7eea11fa82b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponEnergyBigPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fac83334-94ca-4cc1-8f17-20b8322df2f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a78499b6-ebf8-46f7-a852-b7eea11fa82b",
            "compositeImage": {
                "id": "48f3ea0f-ea4f-46e6-820f-590e6e74eaeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fac83334-94ca-4cc1-8f17-20b8322df2f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93456899-6141-435d-b675-e9f215f69c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fac83334-94ca-4cc1-8f17-20b8322df2f6",
                    "LayerId": "84cd7f29-9218-44cb-8bab-bf2415f77f30"
                }
            ]
        },
        {
            "id": "a0cc3133-263f-4a43-88b9-bc6c8e58a14d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a78499b6-ebf8-46f7-a852-b7eea11fa82b",
            "compositeImage": {
                "id": "7d277c88-cec6-4fcf-a21d-e4bf58e538f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0cc3133-263f-4a43-88b9-bc6c8e58a14d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4060647-95f1-47eb-af12-6302f6d1b713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0cc3133-263f-4a43-88b9-bc6c8e58a14d",
                    "LayerId": "84cd7f29-9218-44cb-8bab-bf2415f77f30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "84cd7f29-9218-44cb-8bab-bf2415f77f30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a78499b6-ebf8-46f7-a852-b7eea11fa82b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": -4
}