{
    "id": "99c096db-0056-4991-a66a-0e4ef605b8d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLifeSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 7,
    "bbox_right": 8,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e814c5f-0385-4a46-b08c-c7ac67830cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99c096db-0056-4991-a66a-0e4ef605b8d9",
            "compositeImage": {
                "id": "1cc80c30-fcb8-4113-beb3-ff2e6af73caa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e814c5f-0385-4a46-b08c-c7ac67830cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b732779d-26a5-45e0-8566-26255ea178b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e814c5f-0385-4a46-b08c-c7ac67830cf3",
                    "LayerId": "0e107547-ee95-4592-a681-f6272d199988"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "0e107547-ee95-4592-a681-f6272d199988",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99c096db-0056-4991-a66a-0e4ef605b8d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": -1
}