{
    "id": "bb0b6b19-3631-4891-8332-5f645611d8e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SmirkTile_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32e14623-294e-4d12-827d-8d8e35d54360",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb0b6b19-3631-4891-8332-5f645611d8e8",
            "compositeImage": {
                "id": "843a00c6-ec9a-419b-b410-22a7185492c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32e14623-294e-4d12-827d-8d8e35d54360",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1acbce-36ab-4185-a39a-6fa9b002a2be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32e14623-294e-4d12-827d-8d8e35d54360",
                    "LayerId": "1c252771-79be-49d1-84e8-6966defab5ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1c252771-79be-49d1-84e8-6966defab5ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb0b6b19-3631-4891-8332-5f645611d8e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}