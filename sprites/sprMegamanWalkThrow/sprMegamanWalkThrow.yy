{
    "id": "ca5ace5a-6cfb-4943-9ff4-b663303e898c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkThrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dae53458-97dd-4c2f-abe4-176eb4fc57d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5ace5a-6cfb-4943-9ff4-b663303e898c",
            "compositeImage": {
                "id": "94b804a6-ddd6-42ac-8dc1-9253105a98af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dae53458-97dd-4c2f-abe4-176eb4fc57d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed17fb9-851d-4c59-9b79-6888eec2ec85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dae53458-97dd-4c2f-abe4-176eb4fc57d8",
                    "LayerId": "fb76c70c-aa9b-49d0-8fe5-ceb7bfe32df7"
                }
            ]
        },
        {
            "id": "cf20815b-8c4a-4554-bfa0-0a4560b81b14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5ace5a-6cfb-4943-9ff4-b663303e898c",
            "compositeImage": {
                "id": "7130c737-7a72-4a47-a704-87e8870d6611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf20815b-8c4a-4554-bfa0-0a4560b81b14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f9e0d0-78ac-4afa-8a97-3b924ac66705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf20815b-8c4a-4554-bfa0-0a4560b81b14",
                    "LayerId": "fb76c70c-aa9b-49d0-8fe5-ceb7bfe32df7"
                }
            ]
        },
        {
            "id": "34f026cc-cd0b-4a30-93b3-f66891b17340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5ace5a-6cfb-4943-9ff4-b663303e898c",
            "compositeImage": {
                "id": "dfecc42e-2914-4d3b-bf00-6a8ff523cdf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34f026cc-cd0b-4a30-93b3-f66891b17340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd0f61eb-f0fb-4cdf-8115-40bce782010f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34f026cc-cd0b-4a30-93b3-f66891b17340",
                    "LayerId": "fb76c70c-aa9b-49d0-8fe5-ceb7bfe32df7"
                }
            ]
        },
        {
            "id": "1ff422d8-a0fe-431f-b1ba-9f1ee19cc1e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5ace5a-6cfb-4943-9ff4-b663303e898c",
            "compositeImage": {
                "id": "2b7eadc8-5552-4168-bb1a-9bf69b2de3dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ff422d8-a0fe-431f-b1ba-9f1ee19cc1e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d05edf-6249-4805-85fa-4a38502cfb21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ff422d8-a0fe-431f-b1ba-9f1ee19cc1e2",
                    "LayerId": "fb76c70c-aa9b-49d0-8fe5-ceb7bfe32df7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "fb76c70c-aa9b-49d0-8fe5-ceb7bfe32df7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca5ace5a-6cfb-4943-9ff4-b663303e898c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 11,
    "yorig": 7
}