{
    "id": "01af6260-f315-4a83-bdae-3d55375d99ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpThrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e29aef84-8597-4715-a447-ca7a1c3418ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01af6260-f315-4a83-bdae-3d55375d99ed",
            "compositeImage": {
                "id": "2ffacfaa-67e1-474e-8e36-75f30c2ea98c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e29aef84-8597-4715-a447-ca7a1c3418ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a3be4f2-f915-4b7e-95d4-db6877153942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e29aef84-8597-4715-a447-ca7a1c3418ca",
                    "LayerId": "585bafa0-f461-44c7-a792-cc0dc481e63e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "585bafa0-f461-44c7-a792-cc0dc481e63e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01af6260-f315-4a83-bdae-3d55375d99ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 12,
    "yorig": 7
}