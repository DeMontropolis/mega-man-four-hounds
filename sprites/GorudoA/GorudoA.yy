{
    "id": "ddae51d6-1587-40c2-b994-044670fb6712",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "GorudoA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73352ebf-5b00-4ed7-ae8d-4a30784eb4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddae51d6-1587-40c2-b994-044670fb6712",
            "compositeImage": {
                "id": "7ebd5fc5-2ab4-48fb-92f4-521323f4d929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73352ebf-5b00-4ed7-ae8d-4a30784eb4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6fc75f5-89cd-4fdd-948e-1c40fbfd8710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73352ebf-5b00-4ed7-ae8d-4a30784eb4c9",
                    "LayerId": "4bb829f4-be61-4ab1-a38b-6581d80e6e63"
                }
            ]
        },
        {
            "id": "e41dfd3d-2d78-4d59-bc57-bb6bddacd72d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddae51d6-1587-40c2-b994-044670fb6712",
            "compositeImage": {
                "id": "69be0114-8374-416c-a01c-853afa9d358b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e41dfd3d-2d78-4d59-bc57-bb6bddacd72d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f249511-25ad-40a0-acc5-21565bb71a06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e41dfd3d-2d78-4d59-bc57-bb6bddacd72d",
                    "LayerId": "4bb829f4-be61-4ab1-a38b-6581d80e6e63"
                }
            ]
        },
        {
            "id": "5e96a87c-37bb-49cd-a99a-d6e8a5371371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddae51d6-1587-40c2-b994-044670fb6712",
            "compositeImage": {
                "id": "b51685d1-f881-48d2-9631-3eda2e3d4d6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e96a87c-37bb-49cd-a99a-d6e8a5371371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a74658-2502-4223-9bcd-0b1cb1669e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e96a87c-37bb-49cd-a99a-d6e8a5371371",
                    "LayerId": "4bb829f4-be61-4ab1-a38b-6581d80e6e63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "4bb829f4-be61-4ab1-a38b-6581d80e6e63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddae51d6-1587-40c2-b994-044670fb6712",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 16
}