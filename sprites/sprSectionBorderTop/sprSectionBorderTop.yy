{
    "id": "efc4fa17-46fe-42a2-ae90-a41192d4663d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionBorderTop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3002e1ad-ab55-4314-8cb1-91e492ddf405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc4fa17-46fe-42a2-ae90-a41192d4663d",
            "compositeImage": {
                "id": "8e895b4d-76aa-4c2c-9425-8735a808ea37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3002e1ad-ab55-4314-8cb1-91e492ddf405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "461acb28-89e3-4517-9e90-236df04674f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3002e1ad-ab55-4314-8cb1-91e492ddf405",
                    "LayerId": "84274cae-d6c2-4464-a656-fdfabd1e009b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "84274cae-d6c2-4464-a656-fdfabd1e009b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efc4fa17-46fe-42a2-ae90-a41192d4663d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}