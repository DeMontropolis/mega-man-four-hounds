{
    "id": "fce0226f-328a-4fc1-979c-0dc8807ab7fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanSlideSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 4,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49ecbb7a-96ab-4482-98d1-19a7d6434106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fce0226f-328a-4fc1-979c-0dc8807ab7fd",
            "compositeImage": {
                "id": "f3971915-f45e-4a7e-9ec3-c91f64afa3f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ecbb7a-96ab-4482-98d1-19a7d6434106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49a22b8-4aa5-4b02-90e3-9a872c25ac09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ecbb7a-96ab-4482-98d1-19a7d6434106",
                    "LayerId": "cd6d3240-a0bc-4993-a282-9408de2d8629"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "cd6d3240-a0bc-4993-a282-9408de2d8629",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fce0226f-328a-4fc1-979c-0dc8807ab7fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 11,
    "yorig": 2
}