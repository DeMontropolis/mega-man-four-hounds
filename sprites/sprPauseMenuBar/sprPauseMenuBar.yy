{
    "id": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPauseMenuBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b76a65c6-7fdf-405b-a8e4-c4886de834ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "a58827d0-0f85-4e45-8714-e951c2080eac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b76a65c6-7fdf-405b-a8e4-c4886de834ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838d1efb-76f0-4e21-af36-4651af00170d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76a65c6-7fdf-405b-a8e4-c4886de834ab",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "e6bc1094-b93e-4b6f-97f1-92d8cf12e97d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "00159144-3c8a-49d7-9370-3f94eef94342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6bc1094-b93e-4b6f-97f1-92d8cf12e97d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd3272b-d3cb-4262-8ce9-aee6b681947a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bc1094-b93e-4b6f-97f1-92d8cf12e97d",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "57b963bc-ca51-4e45-96a2-5528ecf5e225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "020c5ed8-f268-4c22-8333-4873e066d8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b963bc-ca51-4e45-96a2-5528ecf5e225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1210f29a-ca82-4d8d-8949-1462b270ec81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b963bc-ca51-4e45-96a2-5528ecf5e225",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "9a2b67fd-ccc5-479d-b605-bb3269a8301d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "b9d9a741-bb3f-48a1-ba2f-c562c01e1b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a2b67fd-ccc5-479d-b605-bb3269a8301d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fccab1c-6067-4ab2-94aa-7281b7d43147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a2b67fd-ccc5-479d-b605-bb3269a8301d",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "bbbb3f7b-0dff-436f-bbf7-864021bd9c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "9a036c29-ca33-4520-aded-f60ed3554e14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbbb3f7b-0dff-436f-bbf7-864021bd9c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27a83868-dcbb-4024-8312-496acbb8d812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbbb3f7b-0dff-436f-bbf7-864021bd9c87",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "592672a2-674f-4fc2-980f-fc8f562df9bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "5d99513c-f7e1-4494-af2c-dab7ac69729b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "592672a2-674f-4fc2-980f-fc8f562df9bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c822f25f-6d95-434f-864f-4f0922aa0cb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "592672a2-674f-4fc2-980f-fc8f562df9bb",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "29ae9891-4199-4564-9f5b-25b17b787860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "dae3065d-a911-4e99-a9d6-55a65d751747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ae9891-4199-4564-9f5b-25b17b787860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63546a4-c980-4fd7-b61f-a76912cbd760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ae9891-4199-4564-9f5b-25b17b787860",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "9e7d55e9-fa40-481a-8ea8-76f214ae5ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "fed8f9ae-dd9e-4993-8834-6d7381ecd840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e7d55e9-fa40-481a-8ea8-76f214ae5ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29036d9b-2b7b-41bd-a335-9ee0fcd23d92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e7d55e9-fa40-481a-8ea8-76f214ae5ecf",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "a24518bc-9d95-4d7f-a099-6425121d11fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "40529666-8fab-4dc4-adfa-871501c0d258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a24518bc-9d95-4d7f-a099-6425121d11fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f4eb964-3e47-43d3-897c-a1541470124e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a24518bc-9d95-4d7f-a099-6425121d11fc",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "df7fcf93-f99c-48b2-b83c-8ae951555b03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "8d5fd78a-0e28-49fb-9f16-ea8805e03267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df7fcf93-f99c-48b2-b83c-8ae951555b03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7328d473-e6c7-4de2-86f7-6c7b46080356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df7fcf93-f99c-48b2-b83c-8ae951555b03",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "901d37ff-a54e-461f-9811-427deae11081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "4591eb46-aa93-42a1-abed-f245fdf3fd71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "901d37ff-a54e-461f-9811-427deae11081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d4e897-0982-4fdc-9ca1-131c1381ecaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "901d37ff-a54e-461f-9811-427deae11081",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "fa50086f-5d25-4cf1-a2bf-b95ccadec5c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "d4174349-790d-44f6-b99a-972a12f582d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa50086f-5d25-4cf1-a2bf-b95ccadec5c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0a19df-d866-449c-b5f5-976d2ca20596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa50086f-5d25-4cf1-a2bf-b95ccadec5c1",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "dd205641-67f2-431f-8a92-cbf95fa9a326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "0eca4ffe-c736-449b-a5bb-f5b22236bf2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd205641-67f2-431f-8a92-cbf95fa9a326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33c78ebd-111e-4e23-8f92-664fde987964",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd205641-67f2-431f-8a92-cbf95fa9a326",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "508d4bf8-b865-443c-8efb-af42c727b4a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "17acc75b-8d36-4ade-97a7-e06539b60f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "508d4bf8-b865-443c-8efb-af42c727b4a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa89c128-6e02-4adc-8d7b-dd6b04185358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "508d4bf8-b865-443c-8efb-af42c727b4a2",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "b8cfea58-7ed7-4723-912d-7a7729feb9f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "e470643d-e2b2-41a4-9c46-efdad6c15f79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8cfea58-7ed7-4723-912d-7a7729feb9f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "939e02b2-18e3-4799-b09e-33456c632a61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8cfea58-7ed7-4723-912d-7a7729feb9f8",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "8ba0c4d1-1e8f-41e2-8eaa-d5301d06a345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "8fe3a382-a229-437a-9036-aec2003b1906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba0c4d1-1e8f-41e2-8eaa-d5301d06a345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ff49821-dca6-492f-a817-21b8836dc96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba0c4d1-1e8f-41e2-8eaa-d5301d06a345",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "3cfde5f8-a329-44c8-81e4-2978e1a1ae42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "c9fefb3b-798d-47a2-852a-fb3b1b16f6bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cfde5f8-a329-44c8-81e4-2978e1a1ae42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e824d604-6ecc-482b-8ae4-0450ad9ef560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cfde5f8-a329-44c8-81e4-2978e1a1ae42",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "7d8b13b5-d65b-4d6f-a236-36bc3a5d4dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "2da91557-aa42-4621-a2fc-97f3ab0bda14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8b13b5-d65b-4d6f-a236-36bc3a5d4dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a110a560-a02c-41d5-ae1f-7dcc14fbb00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8b13b5-d65b-4d6f-a236-36bc3a5d4dbb",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "62c9717a-b6a7-4ebb-9c13-a1f2e9c6a066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "c4906b25-74d8-4002-9b49-10d50917464f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62c9717a-b6a7-4ebb-9c13-a1f2e9c6a066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f92942-fd88-4061-8e6e-a065e12e1436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62c9717a-b6a7-4ebb-9c13-a1f2e9c6a066",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "ca2b13a8-f4dc-4e64-8e51-0680d78e054a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "52542444-3fb0-49da-a714-727475d825c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca2b13a8-f4dc-4e64-8e51-0680d78e054a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a393c262-a012-4710-9f35-be70f1e78e11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca2b13a8-f4dc-4e64-8e51-0680d78e054a",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "6359913a-e3ea-4b1b-a416-545d1b1d2165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "172ef12d-3931-44b0-835c-d67552cb050c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6359913a-e3ea-4b1b-a416-545d1b1d2165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba89ef80-d5dd-4de6-b123-37aca59e3dbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6359913a-e3ea-4b1b-a416-545d1b1d2165",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "435c375d-6287-4c6f-8828-c677cb5ba526",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "d42eb2b6-7b72-4ae1-8ef6-5cc7a825f700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435c375d-6287-4c6f-8828-c677cb5ba526",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef0ce37-e781-4b16-8094-a06104113339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435c375d-6287-4c6f-8828-c677cb5ba526",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "86eecde0-ca04-4752-abb4-a4cba3e9be2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "03c3772e-9584-49a4-bdec-89bb028db369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86eecde0-ca04-4752-abb4-a4cba3e9be2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7e58c7-f799-4019-aac0-829097486ca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86eecde0-ca04-4752-abb4-a4cba3e9be2f",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "1d6eed75-b3ce-4877-a6b4-2632fd645364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "14ffaec7-f6da-4c85-b37e-21e17fcddb12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d6eed75-b3ce-4877-a6b4-2632fd645364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1741d57-3471-4bb7-aae8-1da5c5c004fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d6eed75-b3ce-4877-a6b4-2632fd645364",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "7240fc87-b560-4960-bb8e-89139542ca6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "ffc44f9e-a02d-443f-a3c3-6b3a0c24343a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7240fc87-b560-4960-bb8e-89139542ca6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa43af8a-aad3-4eaa-b630-bf9d0872c73d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7240fc87-b560-4960-bb8e-89139542ca6e",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "88511794-4f23-4503-9c67-ceb1a4c68ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "078a40a0-00a0-475b-b370-67efe8696566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88511794-4f23-4503-9c67-ceb1a4c68ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "889b4b88-790d-4c63-a24a-63a06910bce8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88511794-4f23-4503-9c67-ceb1a4c68ff6",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "4ec8b17b-abc3-4fc4-a9a8-e3ad0865fa36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "410823d2-22f5-4204-9a4f-35472948fb11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec8b17b-abc3-4fc4-a9a8-e3ad0865fa36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d14722-9765-4ae5-a97a-4d86c076b5cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec8b17b-abc3-4fc4-a9a8-e3ad0865fa36",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "673dd4ac-80ec-4959-b1ba-15e882288b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "6b0d6726-8c5e-4203-83c7-856b398dca37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "673dd4ac-80ec-4959-b1ba-15e882288b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba5d84b6-a4a9-410e-b261-4b9508811326",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "673dd4ac-80ec-4959-b1ba-15e882288b93",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        },
        {
            "id": "b394827c-2f30-4181-a500-3079185c67e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "compositeImage": {
                "id": "aa085e02-2291-4dfd-acd7-d60141bfb41b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b394827c-2f30-4181-a500-3079185c67e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2991edc7-ac9f-4f14-b3eb-7df15cd2090a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b394827c-2f30-4181-a500-3079185c67e8",
                    "LayerId": "fca15ebc-65b7-4e02-a645-eadbc508fe4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fca15ebc-65b7-4e02-a645-eadbc508fe4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6925de9-4fb9-4d8b-a1d7-e24c3f939cd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}