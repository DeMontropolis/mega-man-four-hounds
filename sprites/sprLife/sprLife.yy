{
    "id": "11d6b46e-e366-44e1-b177-a70c9c50a22d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a202d73e-b567-40a5-9508-c2e744dbf7b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11d6b46e-e366-44e1-b177-a70c9c50a22d",
            "compositeImage": {
                "id": "81f3f50b-5786-480b-8e4c-d738cc41b993",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a202d73e-b567-40a5-9508-c2e744dbf7b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b748b55c-1fe7-44a6-9bfa-ab5dceb0aa1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a202d73e-b567-40a5-9508-c2e744dbf7b0",
                    "LayerId": "afaf7a9b-c15a-42b6-a4cc-e0b6e38a03d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "afaf7a9b-c15a-42b6-a4cc-e0b6e38a03d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11d6b46e-e366-44e1-b177-a70c9c50a22d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": -1
}