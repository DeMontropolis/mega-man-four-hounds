{
    "id": "9e50b755-3696-4f5f-997a-63e111ea7a57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbThrowPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 21,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f00ec9cb-62bc-4c3a-b5d8-fbbc8974b5c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e50b755-3696-4f5f-997a-63e111ea7a57",
            "compositeImage": {
                "id": "713160df-e464-4a0a-a81d-9eb88664bd23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f00ec9cb-62bc-4c3a-b5d8-fbbc8974b5c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b1234a-3845-4a4d-abe3-158e67210d73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f00ec9cb-62bc-4c3a-b5d8-fbbc8974b5c6",
                    "LayerId": "5935d9de-6d3c-4839-a888-81638ebebe8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "5935d9de-6d3c-4839-a888-81638ebebe8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e50b755-3696-4f5f-997a-63e111ea7a57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 8,
    "yorig": 7
}