{
    "id": "44a5f415-1006-46f1-a25b-dee8d02145bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCheckpoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a728132a-fbc9-4239-a521-fa6c1904985d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a5f415-1006-46f1-a25b-dee8d02145bc",
            "compositeImage": {
                "id": "feb8aca0-b990-4776-98b3-ef35daeabae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a728132a-fbc9-4239-a521-fa6c1904985d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd126425-c02b-4ef4-9891-ca66e937b1c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a728132a-fbc9-4239-a521-fa6c1904985d",
                    "LayerId": "a9d43150-38b7-4559-a6b1-4596b9e8d265"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "a9d43150-38b7-4559-a6b1-4596b9e8d265",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44a5f415-1006-46f1-a25b-dee8d02145bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}