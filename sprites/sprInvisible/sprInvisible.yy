{
    "id": "b80ace3a-b415-463c-bf9b-0b5de3c42fbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprInvisible",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d02a9ae-9534-4f0e-a979-4475c7d7437e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b80ace3a-b415-463c-bf9b-0b5de3c42fbb",
            "compositeImage": {
                "id": "c75bcd01-7b73-4dbb-9c34-dd78b338edcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d02a9ae-9534-4f0e-a979-4475c7d7437e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "471f90c9-abe1-4335-8248-496ef7d864ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d02a9ae-9534-4f0e-a979-4475c7d7437e",
                    "LayerId": "6bcb54a2-fba8-466f-9751-cda39484e33b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6bcb54a2-fba8-466f-9751-cda39484e33b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b80ace3a-b415-463c-bf9b-0b5de3c42fbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}