{
    "id": "3f1aaabb-f5b5-4b89-98c7-d3295f409e87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpShootSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 6,
    "bbox_right": 27,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e238fb51-a758-40cf-8d1c-cb4dc6f04082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1aaabb-f5b5-4b89-98c7-d3295f409e87",
            "compositeImage": {
                "id": "4a9b0c42-9cf1-4224-9060-bfa38f55b768",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e238fb51-a758-40cf-8d1c-cb4dc6f04082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f5aa560-6d76-48bb-b2e7-3bc2dab66dd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e238fb51-a758-40cf-8d1c-cb4dc6f04082",
                    "LayerId": "bd1d03c2-976d-4e63-a1e4-c50ee4376760"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "bd1d03c2-976d-4e63-a1e4-c50ee4376760",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f1aaabb-f5b5-4b89-98c7-d3295f409e87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 12,
    "yorig": 7
}