{
    "id": "ca4143f5-9fa7-4410-a41b-0d0b27584181",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 178,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "270071b6-e98e-4bd1-ab47-d244ea9609f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca4143f5-9fa7-4410-a41b-0d0b27584181",
            "compositeImage": {
                "id": "83561050-0d30-4e75-af6d-4c259c725091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270071b6-e98e-4bd1-ab47-d244ea9609f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844fded3-ae66-4502-b690-9c3f5c7db039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270071b6-e98e-4bd1-ab47-d244ea9609f2",
                    "LayerId": "ccc5a3c4-fc45-46a5-b645-92e0f25900b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "ccc5a3c4-fc45-46a5-b645-92e0f25900b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca4143f5-9fa7-4410-a41b-0d0b27584181",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 179,
    "xorig": 89,
    "yorig": 28
}