{
    "id": "de9249a0-e38f-4a70-9c61-5ae5ab7ff792",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionBorderRightScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5f32107-ba19-41da-b653-c19b61eb1503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de9249a0-e38f-4a70-9c61-5ae5ab7ff792",
            "compositeImage": {
                "id": "f9589d09-5202-4ae2-8bfb-28a73d72086f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5f32107-ba19-41da-b653-c19b61eb1503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ce9f19-3b9b-4b3c-83c5-88855c5bb03a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5f32107-ba19-41da-b653-c19b61eb1503",
                    "LayerId": "72ee08b3-da00-48ed-8573-6178ead6f94f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "72ee08b3-da00-48ed-8573-6178ead6f94f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de9249a0-e38f-4a70-9c61-5ae5ab7ff792",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}