{
    "id": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBoozeCannonUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a93ccfe6-bb2a-44ce-8dab-cac405fc5e52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
            "compositeImage": {
                "id": "99342988-0257-4c8f-a5c4-47e16a3701d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a93ccfe6-bb2a-44ce-8dab-cac405fc5e52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf11646-98c9-481d-8ab5-fcc41499ed25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a93ccfe6-bb2a-44ce-8dab-cac405fc5e52",
                    "LayerId": "6d2f440a-8261-41d9-83cc-d7353617b2f1"
                }
            ]
        },
        {
            "id": "8b1bc357-442d-449f-b390-a2f6636d51be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
            "compositeImage": {
                "id": "1536f0ca-73b1-4da8-b04a-a6b39712412b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b1bc357-442d-449f-b390-a2f6636d51be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2b84fb8-0a3a-4793-a06b-7824539bd4e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b1bc357-442d-449f-b390-a2f6636d51be",
                    "LayerId": "6d2f440a-8261-41d9-83cc-d7353617b2f1"
                }
            ]
        },
        {
            "id": "1a4d63c1-430f-4d34-b2ac-0a7ee22cb398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
            "compositeImage": {
                "id": "5f351a5a-bd27-4612-bf7a-89e5c3b14d39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4d63c1-430f-4d34-b2ac-0a7ee22cb398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b634211c-a82d-4ca5-9f50-16cbd8ef84f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4d63c1-430f-4d34-b2ac-0a7ee22cb398",
                    "LayerId": "6d2f440a-8261-41d9-83cc-d7353617b2f1"
                }
            ]
        },
        {
            "id": "e1477587-bf27-4d76-82b6-a2845ea029d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
            "compositeImage": {
                "id": "77e932ca-1635-4eb6-a6a8-ddd1707f6902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1477587-bf27-4d76-82b6-a2845ea029d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b68823b7-8e04-48ed-85c8-4b032460fa16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1477587-bf27-4d76-82b6-a2845ea029d7",
                    "LayerId": "6d2f440a-8261-41d9-83cc-d7353617b2f1"
                }
            ]
        },
        {
            "id": "8cf010b3-e19f-462f-8956-324aba29b4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
            "compositeImage": {
                "id": "a0fd34c0-e751-4e75-abd2-f8bf80457ff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cf010b3-e19f-462f-8956-324aba29b4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f832feb9-0621-41a5-ad4b-2f995ded0a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cf010b3-e19f-462f-8956-324aba29b4ad",
                    "LayerId": "6d2f440a-8261-41d9-83cc-d7353617b2f1"
                }
            ]
        },
        {
            "id": "9e849c2c-1f57-412e-98e7-def8e68cf2f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
            "compositeImage": {
                "id": "f34ea9e8-8845-43d8-87c4-b6bca1757af6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e849c2c-1f57-412e-98e7-def8e68cf2f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09b7bb2f-d360-488c-aad7-f69decae46cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e849c2c-1f57-412e-98e7-def8e68cf2f0",
                    "LayerId": "6d2f440a-8261-41d9-83cc-d7353617b2f1"
                }
            ]
        },
        {
            "id": "f30c4edc-3039-43fb-aad4-66fee0a2281b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
            "compositeImage": {
                "id": "fab07ff9-1846-42d8-afcd-3c1fc59a4478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f30c4edc-3039-43fb-aad4-66fee0a2281b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c9b320-6e2e-4dd7-99cf-1713bbda228a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f30c4edc-3039-43fb-aad4-66fee0a2281b",
                    "LayerId": "6d2f440a-8261-41d9-83cc-d7353617b2f1"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 128,
    "layers": [
        {
            "id": "6d2f440a-8261-41d9-83cc-d7353617b2f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0bf2ef7-8ff9-46c4-9020-3b47fa0e5a5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0.1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}