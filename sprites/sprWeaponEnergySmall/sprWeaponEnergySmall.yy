{
    "id": "d77876f4-997a-49ac-ac87-59651c5fb5d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponEnergySmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c1fa595-d4ef-4257-b7d7-f8013cc81653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d77876f4-997a-49ac-ac87-59651c5fb5d9",
            "compositeImage": {
                "id": "8333e20e-3975-435b-869c-fed038412cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c1fa595-d4ef-4257-b7d7-f8013cc81653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe38e23d-fd5e-4972-846e-4788cd3b23fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c1fa595-d4ef-4257-b7d7-f8013cc81653",
                    "LayerId": "79f7a32f-0883-41fa-828f-672771a804fc"
                }
            ]
        },
        {
            "id": "32c393bf-61ca-412b-b4fd-091c2ecfcacb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d77876f4-997a-49ac-ac87-59651c5fb5d9",
            "compositeImage": {
                "id": "c3797720-e19e-4422-9f5c-d760407ad59d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c393bf-61ca-412b-b4fd-091c2ecfcacb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4927efc2-1665-4032-95ce-c788113a43a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c393bf-61ca-412b-b4fd-091c2ecfcacb",
                    "LayerId": "79f7a32f-0883-41fa-828f-672771a804fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "79f7a32f-0883-41fa-828f-672771a804fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d77876f4-997a-49ac-ac87-59651c5fb5d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": -4,
    "yorig": -8
}