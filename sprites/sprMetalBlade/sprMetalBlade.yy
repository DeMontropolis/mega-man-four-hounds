{
    "id": "3089b003-bc2a-4352-9781-38602c92f761",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMetalBlade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14ecdb3a-36a9-40d3-a1e4-74f0c2d79ba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3089b003-bc2a-4352-9781-38602c92f761",
            "compositeImage": {
                "id": "8bfa8554-ab86-4c69-b56e-a823e242ee47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ecdb3a-36a9-40d3-a1e4-74f0c2d79ba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb54d1ed-889f-471a-b395-19809e8052db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ecdb3a-36a9-40d3-a1e4-74f0c2d79ba0",
                    "LayerId": "21739b70-7d4a-416f-9a33-3adcebc8f672"
                }
            ]
        },
        {
            "id": "4cb4a9be-e3dc-408d-977b-33c29d2ee415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3089b003-bc2a-4352-9781-38602c92f761",
            "compositeImage": {
                "id": "b5b9e71f-2b53-4afc-9931-1937f457f393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cb4a9be-e3dc-408d-977b-33c29d2ee415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26391c21-1be1-4d6d-85f6-af4662690406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cb4a9be-e3dc-408d-977b-33c29d2ee415",
                    "LayerId": "21739b70-7d4a-416f-9a33-3adcebc8f672"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "21739b70-7d4a-416f-9a33-3adcebc8f672",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3089b003-bc2a-4352-9781-38602c92f761",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}