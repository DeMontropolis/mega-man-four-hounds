{
    "id": "ce32eaf9-e249-48d0-8762-7211c257a6a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkShootSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e1eb4b4-9b9a-4df0-878f-64da2386df61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce32eaf9-e249-48d0-8762-7211c257a6a3",
            "compositeImage": {
                "id": "9f52a93e-8821-4353-bc34-7b1e4232151d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e1eb4b4-9b9a-4df0-878f-64da2386df61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ba0005-734b-4974-92d0-e8fd9255815e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e1eb4b4-9b9a-4df0-878f-64da2386df61",
                    "LayerId": "89d16a8e-bfb9-4777-b637-6a84098bb4b7"
                }
            ]
        },
        {
            "id": "1959d8be-6a38-481f-a641-dd0d6e585d88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce32eaf9-e249-48d0-8762-7211c257a6a3",
            "compositeImage": {
                "id": "776b67c8-143c-4b59-bc05-2e02f22c2ca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1959d8be-6a38-481f-a641-dd0d6e585d88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "518a8739-5b02-40f5-9fab-d086c2835216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1959d8be-6a38-481f-a641-dd0d6e585d88",
                    "LayerId": "89d16a8e-bfb9-4777-b637-6a84098bb4b7"
                }
            ]
        },
        {
            "id": "7e617115-7eae-46dd-8e59-936cfba65de3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce32eaf9-e249-48d0-8762-7211c257a6a3",
            "compositeImage": {
                "id": "f5f19daf-f67f-4c7e-b73f-fa4481f54840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e617115-7eae-46dd-8e59-936cfba65de3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e461fd28-51f4-4ec2-9596-32c7e4ce39cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e617115-7eae-46dd-8e59-936cfba65de3",
                    "LayerId": "89d16a8e-bfb9-4777-b637-6a84098bb4b7"
                }
            ]
        },
        {
            "id": "07e2a810-4a5c-48ee-9404-629fab0b3dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce32eaf9-e249-48d0-8762-7211c257a6a3",
            "compositeImage": {
                "id": "ca75b4ee-5ef5-45bd-ac8c-18c0b16f4b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07e2a810-4a5c-48ee-9404-629fab0b3dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9f33a3-510f-4c99-a20e-918b1645ecd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07e2a810-4a5c-48ee-9404-629fab0b3dc7",
                    "LayerId": "89d16a8e-bfb9-4777-b637-6a84098bb4b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "89d16a8e-bfb9-4777-b637-6a84098bb4b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce32eaf9-e249-48d0-8762-7211c257a6a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 11,
    "yorig": 7
}