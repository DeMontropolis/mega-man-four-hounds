{
    "id": "8db17531-7320-4d40-9298-aa4d3fca0562",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanSlidePrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 1,
    "bbox_right": 26,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0d711fe-019d-4a36-a17f-eb9088db3379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8db17531-7320-4d40-9298-aa4d3fca0562",
            "compositeImage": {
                "id": "2564d0c0-e867-4ca6-8357-5e4aa8037c82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0d711fe-019d-4a36-a17f-eb9088db3379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aa20440-8305-47ee-8705-6a1a6f471801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0d711fe-019d-4a36-a17f-eb9088db3379",
                    "LayerId": "60a0d871-18d4-4137-9b08-7da6d2ada4f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "60a0d871-18d4-4137-9b08-7da6d2ada4f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8db17531-7320-4d40-9298-aa4d3fca0562",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 11,
    "yorig": 2
}