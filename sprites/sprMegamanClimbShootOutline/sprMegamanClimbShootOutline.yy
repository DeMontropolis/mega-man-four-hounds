{
    "id": "27a11969-fad1-4702-8e4d-704eeaba2049",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbShootOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2246d74-877b-4927-837b-2050aad92b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a11969-fad1-4702-8e4d-704eeaba2049",
            "compositeImage": {
                "id": "40a07aec-f076-432f-a18b-cc70fea3e773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2246d74-877b-4927-837b-2050aad92b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3215432c-5077-4506-80b4-4bcd209901cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2246d74-877b-4927-837b-2050aad92b0d",
                    "LayerId": "bd30d6cc-15f7-41be-aa46-a8e4b60c5779"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "bd30d6cc-15f7-41be-aa46-a8e4b60c5779",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27a11969-fad1-4702-8e4d-704eeaba2049",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 8,
    "yorig": 7
}