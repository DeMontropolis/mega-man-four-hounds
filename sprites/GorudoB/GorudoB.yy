{
    "id": "d68ec239-23c0-4913-9dd1-0ae21b2f4149",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "GorudoB",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bef186d-3e94-42d3-ba09-d36f05cf3b68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d68ec239-23c0-4913-9dd1-0ae21b2f4149",
            "compositeImage": {
                "id": "c850019c-20b8-439c-85c3-2a4ac46c88e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bef186d-3e94-42d3-ba09-d36f05cf3b68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f69d355c-405a-43e4-a0a2-119fe41e89b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bef186d-3e94-42d3-ba09-d36f05cf3b68",
                    "LayerId": "24ba2ae5-82e1-4bb9-9594-f6a3c8ea55a8"
                }
            ]
        },
        {
            "id": "b0f1d33f-7819-40c4-869c-999bff7b5dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d68ec239-23c0-4913-9dd1-0ae21b2f4149",
            "compositeImage": {
                "id": "c25669a2-ddd7-406c-9320-5f574628500c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0f1d33f-7819-40c4-869c-999bff7b5dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d850d50e-9961-48c8-835f-ecef2a1ff8de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0f1d33f-7819-40c4-869c-999bff7b5dad",
                    "LayerId": "24ba2ae5-82e1-4bb9-9594-f6a3c8ea55a8"
                }
            ]
        },
        {
            "id": "c27cdcf8-18f4-45eb-9440-745a4a8969ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d68ec239-23c0-4913-9dd1-0ae21b2f4149",
            "compositeImage": {
                "id": "a249ab4d-09f0-4595-91da-3f1753b6ad74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c27cdcf8-18f4-45eb-9440-745a4a8969ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ae5c43a-f4d5-4ead-911b-49a49c804dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c27cdcf8-18f4-45eb-9440-745a4a8969ca",
                    "LayerId": "24ba2ae5-82e1-4bb9-9594-f6a3c8ea55a8"
                }
            ]
        },
        {
            "id": "af3fbb34-b503-49ed-9eaa-1bc915d750c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d68ec239-23c0-4913-9dd1-0ae21b2f4149",
            "compositeImage": {
                "id": "1d7d05fe-667a-4f0d-969d-0c8d7521c2ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af3fbb34-b503-49ed-9eaa-1bc915d750c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dda9b24-8f5b-467b-b39e-935c329ec229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af3fbb34-b503-49ed-9eaa-1bc915d750c1",
                    "LayerId": "24ba2ae5-82e1-4bb9-9594-f6a3c8ea55a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "24ba2ae5-82e1-4bb9-9594-f6a3c8ea55a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d68ec239-23c0-4913-9dd1-0ae21b2f4149",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0.13,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 16
}