{
    "id": "ec8ee048-a293-4569-92af-fdb1165da28e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbThrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "135674a4-c3ef-4f56-83bf-0c53d390fa62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec8ee048-a293-4569-92af-fdb1165da28e",
            "compositeImage": {
                "id": "0893fbe0-311c-4aee-af68-c293b3fd43c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "135674a4-c3ef-4f56-83bf-0c53d390fa62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b41d29-150d-4e04-b963-c1e0fc33d62b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "135674a4-c3ef-4f56-83bf-0c53d390fa62",
                    "LayerId": "d2c27916-218f-45d4-ab33-6909d17658a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "d2c27916-218f-45d4-ab33-6909d17658a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec8ee048-a293-4569-92af-fdb1165da28e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 8,
    "yorig": 7
}