{
    "id": "9002d6f2-605e-43e6-b641-2ee50d8b5603",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgWeaponGet",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 224,
    "bbox_left": 0,
    "bbox_right": 256,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be888102-5bd3-4746-a223-e548c87c06de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9002d6f2-605e-43e6-b641-2ee50d8b5603",
            "compositeImage": {
                "id": "bfd5c3c9-97f4-442a-85d1-c98161436e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be888102-5bd3-4746-a223-e548c87c06de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f7c2a8c-c1aa-4ea3-9320-cef5aa4de497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be888102-5bd3-4746-a223-e548c87c06de",
                    "LayerId": "3e9550f0-89af-4a7b-bec4-67a0086b8027"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 225,
    "layers": [
        {
            "id": "3e9550f0-89af-4a7b-bec4-67a0086b8027",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9002d6f2-605e-43e6-b641-2ee50d8b5603",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 257,
    "xorig": 0,
    "yorig": 0
}