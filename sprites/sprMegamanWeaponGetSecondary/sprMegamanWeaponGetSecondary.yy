{
    "id": "6a44bd97-c6fa-4ba1-ae1b-77bce97821bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWeaponGetSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 1,
    "bbox_right": 37,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "010bff29-69a2-46ab-ade7-545a7b082d39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a44bd97-c6fa-4ba1-ae1b-77bce97821bf",
            "compositeImage": {
                "id": "00687fdd-abe6-4b38-b8f4-c71f54d1e94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "010bff29-69a2-46ab-ade7-545a7b082d39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "136cc8d7-c521-4901-9ccb-a3080f282e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "010bff29-69a2-46ab-ade7-545a7b082d39",
                    "LayerId": "d24457c8-5d5f-41ba-8160-df4bb687453c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 93,
    "layers": [
        {
            "id": "d24457c8-5d5f-41ba-8160-df4bb687453c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a44bd97-c6fa-4ba1-ae1b-77bce97821bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 46
}