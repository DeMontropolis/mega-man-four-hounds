{
    "id": "9da5912a-5407-4c21-ab34-6f1735019056",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanJumpThrowOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63f13491-1ac5-4ab1-bdcd-1ed8787ad9dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9da5912a-5407-4c21-ab34-6f1735019056",
            "compositeImage": {
                "id": "03207b16-dca7-4cc1-b521-fc6c0fada37e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63f13491-1ac5-4ab1-bdcd-1ed8787ad9dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bda3224-b9c9-4b92-87fe-52db442a0954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63f13491-1ac5-4ab1-bdcd-1ed8787ad9dd",
                    "LayerId": "ffb7e0a1-a306-45aa-a752-ca7eda8c642e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "ffb7e0a1-a306-45aa-a752-ca7eda8c642e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9da5912a-5407-4c21-ab34-6f1735019056",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 12,
    "yorig": 7
}