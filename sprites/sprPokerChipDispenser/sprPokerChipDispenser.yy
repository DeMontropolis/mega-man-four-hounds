{
    "id": "609a4b9d-719b-4368-b442-0e1dd1e34034",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPokerChipDispenser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9eae6957-d7c4-4950-bf08-30f2169fcbc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609a4b9d-719b-4368-b442-0e1dd1e34034",
            "compositeImage": {
                "id": "e9a44570-8594-4c14-8241-0f6de12d7075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eae6957-d7c4-4950-bf08-30f2169fcbc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41bd6aa8-5dc3-4af9-9781-519d899da3b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eae6957-d7c4-4950-bf08-30f2169fcbc3",
                    "LayerId": "6251689b-cf66-42e7-b636-fec4ff52047c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6251689b-cf66-42e7-b636-fec4ff52047c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "609a4b9d-719b-4368-b442-0e1dd1e34034",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}