{
    "id": "b1f05876-6dac-4b7d-9bbd-fbff05eab2b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWalkThrowPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43899428-c312-4230-b943-3c209edc2ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f05876-6dac-4b7d-9bbd-fbff05eab2b9",
            "compositeImage": {
                "id": "e0584474-a14a-44ac-bbf3-de0046511471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43899428-c312-4230-b943-3c209edc2ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1058b60e-80c9-4258-a663-4d02b2f9dffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43899428-c312-4230-b943-3c209edc2ae7",
                    "LayerId": "640adf91-469b-4e0c-896d-8ede782506c1"
                }
            ]
        },
        {
            "id": "24395a06-2efd-49da-86df-e25c02f26ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f05876-6dac-4b7d-9bbd-fbff05eab2b9",
            "compositeImage": {
                "id": "4a9dfeaf-de9a-41cc-a781-0989101225f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24395a06-2efd-49da-86df-e25c02f26ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01773c5b-17dc-4e43-8a89-cbd32288f8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24395a06-2efd-49da-86df-e25c02f26ffb",
                    "LayerId": "640adf91-469b-4e0c-896d-8ede782506c1"
                }
            ]
        },
        {
            "id": "9ec10274-701e-4206-9847-47d281ce3089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f05876-6dac-4b7d-9bbd-fbff05eab2b9",
            "compositeImage": {
                "id": "8da8d635-0985-437f-a16b-90ec5401c874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec10274-701e-4206-9847-47d281ce3089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faa1e81c-de57-4943-94d4-3ce8ef52b06e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec10274-701e-4206-9847-47d281ce3089",
                    "LayerId": "640adf91-469b-4e0c-896d-8ede782506c1"
                }
            ]
        },
        {
            "id": "6cef2fc3-5b04-4afb-94ad-194da53dbf82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1f05876-6dac-4b7d-9bbd-fbff05eab2b9",
            "compositeImage": {
                "id": "b2c24112-0b46-409e-a3ef-85fc868bbe02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cef2fc3-5b04-4afb-94ad-194da53dbf82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b21c297-f245-4944-9b38-d2a1a3ef9073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cef2fc3-5b04-4afb-94ad-194da53dbf82",
                    "LayerId": "640adf91-469b-4e0c-896d-8ede782506c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "640adf91-469b-4e0c-896d-8ede782506c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1f05876-6dac-4b7d-9bbd-fbff05eab2b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 11,
    "yorig": 7
}