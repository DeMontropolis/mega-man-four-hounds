{
    "id": "7cc5626d-fed4-4947-b140-17c8a506af28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHitspark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22ae4aeb-182e-43b5-b22a-374a46f8142b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cc5626d-fed4-4947-b140-17c8a506af28",
            "compositeImage": {
                "id": "92385910-f231-48cf-8045-69f354797533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ae4aeb-182e-43b5-b22a-374a46f8142b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da32ca38-7a9c-4fa2-a737-7deaecd73144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ae4aeb-182e-43b5-b22a-374a46f8142b",
                    "LayerId": "0762264d-54eb-48a4-808d-4ab048f2ed6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0762264d-54eb-48a4-808d-4ab048f2ed6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cc5626d-fed4-4947-b140-17c8a506af28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}