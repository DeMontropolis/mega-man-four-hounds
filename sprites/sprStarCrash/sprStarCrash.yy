{
    "id": "c3747ba8-821b-43d5-ae81-daf9f22c12e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStarCrash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc3698ac-36cf-4b1a-b13c-0b119ae98481",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3747ba8-821b-43d5-ae81-daf9f22c12e8",
            "compositeImage": {
                "id": "c0ecd0b1-2393-486d-bdd1-3c7fa533d864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3698ac-36cf-4b1a-b13c-0b119ae98481",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6873f3ef-0260-4345-86e2-7b2908a33661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3698ac-36cf-4b1a-b13c-0b119ae98481",
                    "LayerId": "dc744bd7-7b0b-4585-b3ba-228992343909"
                }
            ]
        },
        {
            "id": "f3652452-82aa-42e6-bd96-fbfe19e16d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3747ba8-821b-43d5-ae81-daf9f22c12e8",
            "compositeImage": {
                "id": "6ef99641-aab6-4d60-a9f9-ef4f2ee323cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3652452-82aa-42e6-bd96-fbfe19e16d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb6322b-6d14-4212-8692-c33dd32fd156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3652452-82aa-42e6-bd96-fbfe19e16d1f",
                    "LayerId": "dc744bd7-7b0b-4585-b3ba-228992343909"
                }
            ]
        },
        {
            "id": "3ef9bdf0-dea0-4134-b4f4-bfb08309b97a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3747ba8-821b-43d5-ae81-daf9f22c12e8",
            "compositeImage": {
                "id": "277c9d1f-a904-409f-ad28-0a9a982a518b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ef9bdf0-dea0-4134-b4f4-bfb08309b97a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b081240c-99b3-4142-a958-6ed3197439af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ef9bdf0-dea0-4134-b4f4-bfb08309b97a",
                    "LayerId": "dc744bd7-7b0b-4585-b3ba-228992343909"
                }
            ]
        },
        {
            "id": "868cd0d3-4892-448c-8047-e19ed099ee6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3747ba8-821b-43d5-ae81-daf9f22c12e8",
            "compositeImage": {
                "id": "df044463-108f-4983-93a5-b5be52f68103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "868cd0d3-4892-448c-8047-e19ed099ee6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "125b90dc-764b-4b0f-9d0e-22dd01ed42d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "868cd0d3-4892-448c-8047-e19ed099ee6e",
                    "LayerId": "dc744bd7-7b0b-4585-b3ba-228992343909"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "dc744bd7-7b0b-4585-b3ba-228992343909",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3747ba8-821b-43d5-ae81-daf9f22c12e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 51,
    "xorig": 25,
    "yorig": 26
}