{
    "id": "815e0db2-5052-48cb-b58b-049c7450ecea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanWeaponGetPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 3,
    "bbox_right": 54,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bdb9c8c-952b-49f4-8452-921285bb994a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "815e0db2-5052-48cb-b58b-049c7450ecea",
            "compositeImage": {
                "id": "5ac8eafc-51de-42a5-97ad-eb77654da935",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bdb9c8c-952b-49f4-8452-921285bb994a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "681adaf2-3999-4f7a-a4bc-b2f127ed58d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bdb9c8c-952b-49f4-8452-921285bb994a",
                    "LayerId": "e9fd9d78-634b-498d-bd60-41b7192a7e87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 93,
    "layers": [
        {
            "id": "e9fd9d78-634b-498d-bd60-41b7192a7e87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "815e0db2-5052-48cb-b58b-049c7450ecea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 46
}