{
    "id": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMM3Font",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84882376-2959-46de-adee-2b3b1394acac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "d20f51a6-cc0b-48a8-9e29-00166e10d69c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84882376-2959-46de-adee-2b3b1394acac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b21841-8654-47e0-b69a-30027e352606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84882376-2959-46de-adee-2b3b1394acac",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "49b36269-c26d-4e5e-814e-fe09a7321a77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "d7a47021-8ae6-40a2-8e47-3571efcbf3ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b36269-c26d-4e5e-814e-fe09a7321a77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb3009fb-0f2f-4ff3-952f-afa84f6fb9b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b36269-c26d-4e5e-814e-fe09a7321a77",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "77419565-236e-4f95-9519-437aa4569b6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "1b895af7-5a97-44b6-8135-327fed6770f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77419565-236e-4f95-9519-437aa4569b6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee3cc3e4-164b-4380-9152-9046d235ca75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77419565-236e-4f95-9519-437aa4569b6a",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "604ce909-fcf2-4f7c-bd7f-bb0b7f39d29d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "937f39ef-b83b-4b37-be44-4c0c8575e860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "604ce909-fcf2-4f7c-bd7f-bb0b7f39d29d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93118624-9813-4f6a-92bb-265c9b647e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "604ce909-fcf2-4f7c-bd7f-bb0b7f39d29d",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "6f4eade4-6048-45f7-867b-953955a86b45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "521bbf72-5178-4840-829c-009823200548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f4eade4-6048-45f7-867b-953955a86b45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a3efcb4-b35e-436e-9b29-4c7a9efa385e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f4eade4-6048-45f7-867b-953955a86b45",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "96d6b29b-a199-49dc-9059-ca66cead75d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "cd8792d6-65de-4baa-9e9f-066b368264a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d6b29b-a199-49dc-9059-ca66cead75d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97c512cb-8522-4ca1-86c3-00bb5a8fd805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d6b29b-a199-49dc-9059-ca66cead75d6",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "43064107-423e-476a-b80a-fb067ad6cd82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "590eb09c-f6fa-4a15-b5bc-ec4fbc1d55e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43064107-423e-476a-b80a-fb067ad6cd82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd49282e-aab9-4d13-9cea-6b0fb06a140f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43064107-423e-476a-b80a-fb067ad6cd82",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "edb78ee2-deea-4c75-ae19-16979ab43c46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "f3b32229-49bb-4b73-820c-6eb5ffa9af4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edb78ee2-deea-4c75-ae19-16979ab43c46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff328b31-724a-4e51-a0ba-f7c44c29383d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edb78ee2-deea-4c75-ae19-16979ab43c46",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "ff5118b2-0f6c-4c7f-8be8-fa61a741eb67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "4e1ede5d-bc30-48ba-a62d-c26625a11af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5118b2-0f6c-4c7f-8be8-fa61a741eb67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71cbee5f-2726-451d-97cc-5c143f68054f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5118b2-0f6c-4c7f-8be8-fa61a741eb67",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "e33d9c43-dab1-4491-a80f-af1682d7d8a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "97a5e685-6a21-451d-a938-df9e306bd12d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e33d9c43-dab1-4491-a80f-af1682d7d8a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b4e8854-0326-4bdc-883a-1414be9e742c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e33d9c43-dab1-4491-a80f-af1682d7d8a0",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "3b0b257f-9f7a-4c8a-a87b-59a053617da8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "4b4c159b-a6fa-4d64-9f35-1e7753bac3dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b0b257f-9f7a-4c8a-a87b-59a053617da8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e11a65a7-94f0-40be-9e43-e01b087fb75b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b0b257f-9f7a-4c8a-a87b-59a053617da8",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "532636a2-050a-4153-98e2-c4a752659536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "ee20d32a-7909-4c29-b41e-e8b422712aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "532636a2-050a-4153-98e2-c4a752659536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddde7055-4860-482e-a9ce-3b9c4d5efea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "532636a2-050a-4153-98e2-c4a752659536",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "020ebb17-5628-49f6-b817-c9b64ed0f945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "c393e868-125b-4376-960a-de8fda60900d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "020ebb17-5628-49f6-b817-c9b64ed0f945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0af64c30-e8fa-4b22-bcd1-c5cfc4d7d19c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "020ebb17-5628-49f6-b817-c9b64ed0f945",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "3d30236f-604c-468d-951f-d63dac51910e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "da7ca1f9-93d3-4ac7-a316-ec75782f29c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d30236f-604c-468d-951f-d63dac51910e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc4706b-b972-458f-b62f-3e47d0bf013c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d30236f-604c-468d-951f-d63dac51910e",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "f84195bd-39be-4203-a8ba-d41ccf81ff5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "5d765af0-1a2d-4b4e-8600-7baf4f51a5fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f84195bd-39be-4203-a8ba-d41ccf81ff5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0540e0cb-6fed-41fa-b49c-a8773d9bc1fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f84195bd-39be-4203-a8ba-d41ccf81ff5a",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "2655feac-95d6-4a61-bc2e-0e542286daa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "dd63a936-203c-4515-b25f-07d89f2047a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2655feac-95d6-4a61-bc2e-0e542286daa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57eb15a2-4e18-4e8b-be4c-2a83311bc8d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2655feac-95d6-4a61-bc2e-0e542286daa2",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "beeaf9be-bf11-445f-8fa4-6cd8a085f2b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "c99c7019-ebb3-438a-8a6c-7fb16fb8f360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beeaf9be-bf11-445f-8fa4-6cd8a085f2b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2da6d8e9-407d-4795-8183-a8e5c3bb93c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beeaf9be-bf11-445f-8fa4-6cd8a085f2b9",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "f4261a5d-8f2d-46a3-9af6-b83caee84650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "95042704-b8e7-4b37-b65c-0782bd509583",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4261a5d-8f2d-46a3-9af6-b83caee84650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3ca4d51-3ab7-4266-be52-40de82d6b450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4261a5d-8f2d-46a3-9af6-b83caee84650",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "c96d1187-842a-495b-bb45-ca927b250736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "8401b810-0339-4416-a144-154758b5225d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c96d1187-842a-495b-bb45-ca927b250736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e3515d-1983-451b-b6a2-10fdc7b224ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c96d1187-842a-495b-bb45-ca927b250736",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "d22bd0a7-a0ab-4f26-b0f7-b14817f92a1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "07ee7a1b-0a1f-4d19-b570-8e2e4413aff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d22bd0a7-a0ab-4f26-b0f7-b14817f92a1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b52575d-b844-4d26-bc1f-392977247c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d22bd0a7-a0ab-4f26-b0f7-b14817f92a1b",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "03209e37-6c26-44af-9bd7-932b24626c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "44d3fdf7-30cf-4be0-92c3-65d3a5960cc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03209e37-6c26-44af-9bd7-932b24626c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28c9aed8-8884-44e7-8db8-86c4c994ea11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03209e37-6c26-44af-9bd7-932b24626c47",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "f017192e-f371-402d-b261-4f052e47b79a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "7074abf4-853d-4108-84ca-cfd1a1512c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f017192e-f371-402d-b261-4f052e47b79a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a8b1b9d-b7b7-47e5-ae2a-bb6787cc08ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f017192e-f371-402d-b261-4f052e47b79a",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "77df3ae3-c9f4-4868-80b5-6898a02415c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "208753c6-c996-4072-8646-c4ff644f53ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77df3ae3-c9f4-4868-80b5-6898a02415c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d17efb-538c-483d-99f7-a336acb34e8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77df3ae3-c9f4-4868-80b5-6898a02415c7",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "c333dd38-79fe-45cb-90d0-49c4b59148c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "f17227e5-4062-419d-a354-6008538cb47b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c333dd38-79fe-45cb-90d0-49c4b59148c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca7f341c-eb57-4312-abdc-f5dbcdddee7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c333dd38-79fe-45cb-90d0-49c4b59148c9",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "7b40ba6c-db2a-473c-b384-a994d059aaf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "c0329c56-b259-4cb7-8536-5a51142041a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b40ba6c-db2a-473c-b384-a994d059aaf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4de3f912-311a-4d1a-aeb9-b5f33c45dafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b40ba6c-db2a-473c-b384-a994d059aaf8",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "925aa4a3-924e-42e2-82ac-3df4957c3f99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "3a939810-fd00-4155-805f-50ba6adf0534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "925aa4a3-924e-42e2-82ac-3df4957c3f99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97959231-0a45-4f41-85a1-5dcd52bf5b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "925aa4a3-924e-42e2-82ac-3df4957c3f99",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "e95741ed-b38c-4157-b492-d3ed241dcab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "7f19066d-7a3e-4e40-b0be-d26eb9b6b908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e95741ed-b38c-4157-b492-d3ed241dcab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acfc4380-f9aa-4bbe-a942-39faff60283d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95741ed-b38c-4157-b492-d3ed241dcab4",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "815761b0-9cd1-468f-b141-bf7ab920fd53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "31510d0f-b4bc-4bdf-9404-20df53711487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815761b0-9cd1-468f-b141-bf7ab920fd53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2536968d-38af-4e3f-814a-275ded67c124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815761b0-9cd1-468f-b141-bf7ab920fd53",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "909d25df-0e7d-44a7-9ad7-0dd825f81ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "da960453-3a54-4c1d-b0ba-f780b7ba7ac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909d25df-0e7d-44a7-9ad7-0dd825f81ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa12f49b-835a-48f5-a569-0213810a6a88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909d25df-0e7d-44a7-9ad7-0dd825f81ea4",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "2033204c-f210-402a-b260-6db798734f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "ad86582d-e090-41de-8e5d-ae1c61c4f9a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2033204c-f210-402a-b260-6db798734f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4a73fb0-a1ca-4a3c-abf2-a2d10ed1ef19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2033204c-f210-402a-b260-6db798734f8b",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "1320de30-ca2c-4f1d-b54c-311a085784ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "204eb639-96e5-4c26-a483-01b1500e79f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1320de30-ca2c-4f1d-b54c-311a085784ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7a5796-7c3e-40e1-a112-658251b49808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1320de30-ca2c-4f1d-b54c-311a085784ee",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "375170fe-4dbd-4c2c-9d38-f598e5fed793",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "9f74a8f2-ace9-4f71-9380-26102ce37b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375170fe-4dbd-4c2c-9d38-f598e5fed793",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b7d8d3c-0787-4490-b4b1-9709e74ba936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375170fe-4dbd-4c2c-9d38-f598e5fed793",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "b469a3bb-e349-448e-9dd1-c54874a3c76b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "12fe2c3e-eeb6-4959-9310-7bc55823977b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b469a3bb-e349-448e-9dd1-c54874a3c76b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5fe98ab-d4f5-4d70-b3f4-7e3c39ca0fd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b469a3bb-e349-448e-9dd1-c54874a3c76b",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "fff585a0-c425-479b-bbb0-b1e9a106bc8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "3ec282bd-1eb3-43a9-a05e-00c4452be8b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fff585a0-c425-479b-bbb0-b1e9a106bc8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63ecaa17-c6a1-4bd7-a0c3-26dd6caf8bb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fff585a0-c425-479b-bbb0-b1e9a106bc8e",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "c191d2a1-7937-4030-9a52-b5f858d78308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "2681778f-f1ea-4b30-8748-a089debed26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c191d2a1-7937-4030-9a52-b5f858d78308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cd9a7c3-e262-48d1-b955-3bffb0b7f5ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c191d2a1-7937-4030-9a52-b5f858d78308",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "6ec5f02d-4bd7-41f5-8ce9-7493fa164cf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "3f96ee3b-4abf-4b3b-8b48-c88cf8742e2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec5f02d-4bd7-41f5-8ce9-7493fa164cf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d3b90ca-3dd6-4a2f-a612-41cf3751e891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec5f02d-4bd7-41f5-8ce9-7493fa164cf1",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "a2c429af-7a99-4159-b62c-c7e62575d7f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "82b7ed9b-d68a-49e9-9eb3-de4849ebe3a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2c429af-7a99-4159-b62c-c7e62575d7f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7cf99d-6ab3-4b4f-8d08-8056a4112bcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2c429af-7a99-4159-b62c-c7e62575d7f9",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "7f2b6f52-e185-401c-b075-315e91055c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "c962801b-c0c1-4abd-a656-9bfdc34211cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f2b6f52-e185-401c-b075-315e91055c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9967bdf-b8de-4edb-8460-220af6235e66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f2b6f52-e185-401c-b075-315e91055c5a",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "914c454a-4e48-4f45-866d-5391939e8ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "348b7c65-039f-41be-a1b5-e9b6298501eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "914c454a-4e48-4f45-866d-5391939e8ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ff00bb-b535-4d8f-91ef-79644d5ed102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "914c454a-4e48-4f45-866d-5391939e8ef4",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "d3a3e226-8be6-4aa3-ac09-a0d10bc5f8f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "666b93f2-aef0-467b-9564-7e117ed8e70b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a3e226-8be6-4aa3-ac09-a0d10bc5f8f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0f2377c-96bf-424f-ae14-79a04598329f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a3e226-8be6-4aa3-ac09-a0d10bc5f8f7",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "203e7b2b-9068-4b42-a28d-fb6ca41e01d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "60be7c28-b01f-4da1-98a9-bfb6055d9d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "203e7b2b-9068-4b42-a28d-fb6ca41e01d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dad9125-2482-4f36-8c53-c6332f0c4230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "203e7b2b-9068-4b42-a28d-fb6ca41e01d8",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "d3e445ac-50dd-4bce-af34-ff67c5f1be61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "4804ada9-ee99-4397-b4d2-684ce744c083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3e445ac-50dd-4bce-af34-ff67c5f1be61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d431f5bc-894b-4107-9bb1-51dabbeb3309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3e445ac-50dd-4bce-af34-ff67c5f1be61",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "0444bce8-c8a9-439c-bc21-5d8d0bc53bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "7653096b-4281-4356-b4f4-1cc4b6396778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0444bce8-c8a9-439c-bc21-5d8d0bc53bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4103bea7-a79b-48d3-b448-0cd2fc6c430d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0444bce8-c8a9-439c-bc21-5d8d0bc53bb1",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "ae192e2a-b9a5-49f9-9ac3-1317180cc9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "ecbe9f28-b6aa-4c35-ad5d-24ac9a584dce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae192e2a-b9a5-49f9-9ac3-1317180cc9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e316eec7-4e49-43f6-b6df-fe3825c917d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae192e2a-b9a5-49f9-9ac3-1317180cc9c8",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "940fd098-4cdd-430d-9333-eeaf6f32060d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "d801faa3-3d41-4ee6-b91c-1a248714d9b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940fd098-4cdd-430d-9333-eeaf6f32060d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3766c4f-8ff6-467b-83de-01475d904786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940fd098-4cdd-430d-9333-eeaf6f32060d",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "8b8d0e45-cd77-47af-aee0-ce802cbe6d2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "a98c5a43-20a7-43d3-874c-9a97974e5ba7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b8d0e45-cd77-47af-aee0-ce802cbe6d2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8afa5fd-bff8-47f4-9693-4dee62f6649b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b8d0e45-cd77-47af-aee0-ce802cbe6d2c",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "10f431a1-6612-4961-9cfd-153780a18cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "b804c589-614e-4815-89fe-e86997121358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f431a1-6612-4961-9cfd-153780a18cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34ebcad-d2df-4918-96ac-984fef85b41e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f431a1-6612-4961-9cfd-153780a18cd3",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "e5568810-ef5f-4af2-a055-01c3d612db80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "f5f2a783-988e-49ae-9a21-a29c7daba674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5568810-ef5f-4af2-a055-01c3d612db80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74cd570e-f4b9-4d7f-9c98-dd9edb6a408d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5568810-ef5f-4af2-a055-01c3d612db80",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "733a7237-aff6-48bd-99b4-f7df2273af1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "5dff96b8-ef8c-430c-baea-cafdbf300902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "733a7237-aff6-48bd-99b4-f7df2273af1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2417ac2-77b6-456b-95b7-092146f7bf66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "733a7237-aff6-48bd-99b4-f7df2273af1c",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "8433b181-9af2-49ec-b827-4b0d5154e336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "868d2984-ec97-4e75-abf6-3b72cc49fab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8433b181-9af2-49ec-b827-4b0d5154e336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2509c10b-1b14-464d-a889-9886ae47fc26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8433b181-9af2-49ec-b827-4b0d5154e336",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "aa80fc66-382e-46d9-a8ee-5e6e445addf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "9336b841-aebe-4752-aeaa-d10568cb9b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa80fc66-382e-46d9-a8ee-5e6e445addf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce1d311f-5bf9-42ee-a235-6b98f2e6eb60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa80fc66-382e-46d9-a8ee-5e6e445addf8",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "bbbbf210-7309-4c2f-873a-f276d67e0088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "129a93cb-5964-4f95-a250-976a857785b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbbbf210-7309-4c2f-873a-f276d67e0088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a4a5a0-2186-4f66-8d92-21ca3bafde6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbbbf210-7309-4c2f-873a-f276d67e0088",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "233771ad-d8c1-4b22-913d-b9edcf8acf25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "659d2760-de35-45aa-b7f4-18242dfda365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233771ad-d8c1-4b22-913d-b9edcf8acf25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a9b0fd-c168-445a-9a56-0015d58df7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233771ad-d8c1-4b22-913d-b9edcf8acf25",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "e8c821a3-e1ef-4b27-881d-4befe4cfb757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "d7d42d3e-f987-4308-b62c-836b439296de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8c821a3-e1ef-4b27-881d-4befe4cfb757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f922093-5429-469e-9b0a-1f72066aea82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c821a3-e1ef-4b27-881d-4befe4cfb757",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "e02d15ad-2ec1-4e76-8a03-eca7157a6a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "61e906b7-b148-42be-ae15-c0f999ce6a30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e02d15ad-2ec1-4e76-8a03-eca7157a6a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e2ed337-407d-423a-87e8-a6c60765072f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e02d15ad-2ec1-4e76-8a03-eca7157a6a32",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "3a58c349-5a97-4ae7-a816-e41a4fce3100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "def20e34-e93a-44af-a917-d74ad66c871b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a58c349-5a97-4ae7-a816-e41a4fce3100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16db708a-8026-4b56-aa32-671fca5fd2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a58c349-5a97-4ae7-a816-e41a4fce3100",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "6059e02f-d592-400c-a396-e5e017e8c311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "90a0b3ca-ba5a-44de-84d9-1a9805c53b27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6059e02f-d592-400c-a396-e5e017e8c311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e81fd375-15ec-4251-88dc-61e8822981c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6059e02f-d592-400c-a396-e5e017e8c311",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        },
        {
            "id": "c6d9b557-a23b-438a-92fe-2bc846a0f849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "compositeImage": {
                "id": "3f740d37-9d19-4070-9e6c-fa4cad584779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d9b557-a23b-438a-92fe-2bc846a0f849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b86d314-ee19-4507-b64c-d6b7a5062548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d9b557-a23b-438a-92fe-2bc846a0f849",
                    "LayerId": "f1620242-50b0-42b7-9b4a-a63004af5122"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f1620242-50b0-42b7-9b4a-a63004af5122",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "241fdfe3-03e6-44c6-82e0-9c1524fb105f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}