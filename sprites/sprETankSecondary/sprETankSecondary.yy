{
    "id": "27f0ab7f-c10b-4c35-a136-0cb76972d8ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprETankSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25e23929-d47a-47a5-a8e6-12e10fe6098b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27f0ab7f-c10b-4c35-a136-0cb76972d8ab",
            "compositeImage": {
                "id": "43bc4aca-4c59-46fa-a80e-ecd41187e0ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e23929-d47a-47a5-a8e6-12e10fe6098b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41606d49-75d2-4fd9-8af2-c88a3ee5fc80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e23929-d47a-47a5-a8e6-12e10fe6098b",
                    "LayerId": "33afb02a-d906-4645-b3cb-72186c7a9d98"
                }
            ]
        },
        {
            "id": "38d97668-b4df-4f0f-8c22-8eb839b168b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27f0ab7f-c10b-4c35-a136-0cb76972d8ab",
            "compositeImage": {
                "id": "34e4d055-0bfe-4929-a4a5-2983458b530e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d97668-b4df-4f0f-8c22-8eb839b168b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb99009-1291-45d5-ae67-e58f07bf889a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d97668-b4df-4f0f-8c22-8eb839b168b8",
                    "LayerId": "33afb02a-d906-4645-b3cb-72186c7a9d98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "33afb02a-d906-4645-b3cb-72186c7a9d98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27f0ab7f-c10b-4c35-a136-0cb76972d8ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}