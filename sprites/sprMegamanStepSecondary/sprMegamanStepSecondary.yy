{
    "id": "44965255-c5d2-4ae6-9e45-ddb11f73ce5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanStepSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 4,
    "bbox_right": 16,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0c83961-0252-4f7a-a75d-da8149a58f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44965255-c5d2-4ae6-9e45-ddb11f73ce5e",
            "compositeImage": {
                "id": "d14c7360-ed6b-41fb-bef6-0137c1ad939c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c83961-0252-4f7a-a75d-da8149a58f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d15ef7-708b-4551-86d1-53e72cccaddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c83961-0252-4f7a-a75d-da8149a58f8c",
                    "LayerId": "b3e6674d-3f96-400b-8788-443946e53100"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b3e6674d-3f96-400b-8788-443946e53100",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44965255-c5d2-4ae6-9e45-ddb11f73ce5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 7
}