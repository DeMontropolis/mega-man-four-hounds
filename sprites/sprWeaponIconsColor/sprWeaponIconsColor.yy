{
    "id": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWeaponIconsColor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8daa7b34-19a9-48ae-8b78-5666bf1398aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "3f1be80b-efad-4a37-8ee1-e3c895feabfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8daa7b34-19a9-48ae-8b78-5666bf1398aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "156d53d6-5f9b-4316-852a-bc00f5435473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8daa7b34-19a9-48ae-8b78-5666bf1398aa",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        },
        {
            "id": "dd832817-ce83-4554-8087-8c78e0e3ba25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "087de78d-d2ba-48c1-a6de-fba2609213a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd832817-ce83-4554-8087-8c78e0e3ba25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3dc82b4-75d7-4c2c-8211-305ff1808dcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd832817-ce83-4554-8087-8c78e0e3ba25",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        },
        {
            "id": "c91c3f2d-a8ee-4e70-ac0c-a5f0cd31a6be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "c642c79f-6fce-45ad-af0c-546c80016261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91c3f2d-a8ee-4e70-ac0c-a5f0cd31a6be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7092af5-63ac-4dcc-b862-eff18f3c8bba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91c3f2d-a8ee-4e70-ac0c-a5f0cd31a6be",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        },
        {
            "id": "12472597-eac8-418c-910f-ce64c09f3b07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "c9fb3f15-b9ae-4636-8cea-e359f078d4db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12472597-eac8-418c-910f-ce64c09f3b07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87c21017-c170-4ae1-a66f-ecfd21e955f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12472597-eac8-418c-910f-ce64c09f3b07",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        },
        {
            "id": "f75fd0b6-c2da-4ce0-abae-798c4c90b64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "a38e79f7-1e65-4060-8e51-d08d0e111297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f75fd0b6-c2da-4ce0-abae-798c4c90b64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b30dfa-1a75-4d49-8464-593439f44b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f75fd0b6-c2da-4ce0-abae-798c4c90b64b",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        },
        {
            "id": "37a87264-3790-4a1b-b7f1-fa2d11a68049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "d136b597-10e4-4664-a69e-35d1d00f5993",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a87264-3790-4a1b-b7f1-fa2d11a68049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a38f9cd-f075-4ab9-a099-e4f92894979c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a87264-3790-4a1b-b7f1-fa2d11a68049",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        },
        {
            "id": "e024bc20-4ec0-4da2-ae3d-52d022c5fe51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "766a14c5-171a-4003-ae22-1d04c81d1e53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e024bc20-4ec0-4da2-ae3d-52d022c5fe51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b7ba27-9e06-4124-a5d1-a73414462d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e024bc20-4ec0-4da2-ae3d-52d022c5fe51",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        },
        {
            "id": "ee012f36-5a5a-451d-8f0d-c914915e7c9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "c47289c0-8ba1-4dbe-8d2c-bcf3ccf5f5f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee012f36-5a5a-451d-8f0d-c914915e7c9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19421bc1-4edc-4512-8966-4aff364d4557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee012f36-5a5a-451d-8f0d-c914915e7c9f",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        },
        {
            "id": "e22a5e27-5105-4314-b279-4c2da83d1ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "compositeImage": {
                "id": "568a8ba7-82c2-4482-8b41-89d017431fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22a5e27-5105-4314-b279-4c2da83d1ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6e89ef-c87c-4d4d-b2f3-6dc1677752e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22a5e27-5105-4314-b279-4c2da83d1ed8",
                    "LayerId": "c8faf06a-612c-4ea7-bb52-0f7683751bae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c8faf06a-612c-4ea7-bb52-0f7683751bae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "738c2913-e5c8-45a4-a816-b7abdbc720d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}