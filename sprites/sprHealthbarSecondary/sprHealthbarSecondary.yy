{
    "id": "0d512c1d-683b-4c44-bc9a-9181caebc97f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHealthbarSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 2,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38342c83-2c47-4eaf-94fb-1e29ccd27bb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d512c1d-683b-4c44-bc9a-9181caebc97f",
            "compositeImage": {
                "id": "bf287d92-140a-46c8-8dcf-585d2f42def2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38342c83-2c47-4eaf-94fb-1e29ccd27bb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c6539d9-9d6a-46a4-8155-7b8e020e9180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38342c83-2c47-4eaf-94fb-1e29ccd27bb2",
                    "LayerId": "926dba3c-49b7-4fb5-a0b6-49f6f97a293b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "926dba3c-49b7-4fb5-a0b6-49f6f97a293b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d512c1d-683b-4c44-bc9a-9181caebc97f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}