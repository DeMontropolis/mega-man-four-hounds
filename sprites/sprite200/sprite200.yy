{
    "id": "7eec674c-b62c-42a2-bc0a-64fb0b3e51b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite200",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f47db57b-b1e1-4efa-b0eb-083f67660719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eec674c-b62c-42a2-bc0a-64fb0b3e51b6",
            "compositeImage": {
                "id": "5982f9fd-b82f-4ce6-bc85-7717690a4724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f47db57b-b1e1-4efa-b0eb-083f67660719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86337554-a3c4-436f-952c-df26cad24e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f47db57b-b1e1-4efa-b0eb-083f67660719",
                    "LayerId": "fc536e61-848f-4c60-bd03-d6ae8ebbfd8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fc536e61-848f-4c60-bd03-d6ae8ebbfd8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7eec674c-b62c-42a2-bc0a-64fb0b3e51b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}