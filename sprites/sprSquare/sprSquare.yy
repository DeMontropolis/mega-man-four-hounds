{
    "id": "9e9eba65-b824-4726-b796-2fa10fa013ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6ca9b14-0a0d-494b-89c2-a73adc260352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9eba65-b824-4726-b796-2fa10fa013ab",
            "compositeImage": {
                "id": "5079e4a9-9fea-46f8-9071-f5c6de0bad67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6ca9b14-0a0d-494b-89c2-a73adc260352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06621c94-66a2-4b95-b6b5-20ab3ceca07f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6ca9b14-0a0d-494b-89c2-a73adc260352",
                    "LayerId": "fd405858-71a2-4b10-8339-0e7f62806d7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fd405858-71a2-4b10-8339-0e7f62806d7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e9eba65-b824-4726-b796-2fa10fa013ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}