{
    "id": "cff436c6-6a36-48e4-9c10-904e92ece9c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSectionArrowDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a0bd824-e0d4-4f46-8302-33243005c8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cff436c6-6a36-48e4-9c10-904e92ece9c7",
            "compositeImage": {
                "id": "702ca4d3-81e6-4ee6-8772-b6ca3c35592d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a0bd824-e0d4-4f46-8302-33243005c8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e8d7cf-f453-4195-974b-f8b0d4319bc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a0bd824-e0d4-4f46-8302-33243005c8d4",
                    "LayerId": "064882a0-b9ba-4710-af44-8d0411ff4f9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "064882a0-b9ba-4710-af44-8d0411ff4f9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cff436c6-6a36-48e4-9c10-904e92ece9c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}