{
    "id": "fe8a5c54-f072-457c-9c0e-23e3706cccde",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLifeEnergyBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f3f13d1-7c23-4337-9071-ca9f93c043ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe8a5c54-f072-457c-9c0e-23e3706cccde",
            "compositeImage": {
                "id": "64525a85-a6ba-4d95-87cd-54db430dc70c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f3f13d1-7c23-4337-9071-ca9f93c043ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4601e6db-611a-4532-bec9-d327dcb7b434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f3f13d1-7c23-4337-9071-ca9f93c043ab",
                    "LayerId": "44a44834-823f-4a6f-86ed-724dc600f46b"
                }
            ]
        },
        {
            "id": "a870c0b1-23fc-4874-b3d8-31fcb0d884a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe8a5c54-f072-457c-9c0e-23e3706cccde",
            "compositeImage": {
                "id": "92e816b4-43df-40b9-88a9-12ca752d79ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a870c0b1-23fc-4874-b3d8-31fcb0d884a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa61d45-bb68-4a1d-9849-642905a5e5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a870c0b1-23fc-4874-b3d8-31fcb0d884a0",
                    "LayerId": "44a44834-823f-4a6f-86ed-724dc600f46b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "44a44834-823f-4a6f-86ed-724dc600f46b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe8a5c54-f072-457c-9c0e-23e3706cccde",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}