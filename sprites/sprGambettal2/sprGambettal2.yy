{
    "id": "3f58b309-7d59-4a19-99a8-e72638388c5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGambettal2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a89f0b51-787c-4cf1-b08e-809da72d3863",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f58b309-7d59-4a19-99a8-e72638388c5c",
            "compositeImage": {
                "id": "1d80b469-a875-44d8-819d-1c50a67a9cab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a89f0b51-787c-4cf1-b08e-809da72d3863",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c59f64-4428-4d8b-a1e6-97bee5398181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a89f0b51-787c-4cf1-b08e-809da72d3863",
                    "LayerId": "6f335e11-a136-4d8d-8648-5f13e4af1cda"
                }
            ]
        },
        {
            "id": "11277aeb-38f7-4e08-bfbc-1180054ea804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f58b309-7d59-4a19-99a8-e72638388c5c",
            "compositeImage": {
                "id": "8298e7fe-3e14-4c3a-98a7-254d68820771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11277aeb-38f7-4e08-bfbc-1180054ea804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66170d4e-3eb7-4e7a-ba67-868968a33fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11277aeb-38f7-4e08-bfbc-1180054ea804",
                    "LayerId": "6f335e11-a136-4d8d-8648-5f13e4af1cda"
                }
            ]
        },
        {
            "id": "241b9b26-004f-429f-a9aa-91adfe265f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f58b309-7d59-4a19-99a8-e72638388c5c",
            "compositeImage": {
                "id": "edd2bc09-a3f6-4f30-a353-65f29f0d682a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241b9b26-004f-429f-a9aa-91adfe265f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "923752c3-3e30-4155-b531-96c1a7dd4ba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241b9b26-004f-429f-a9aa-91adfe265f5a",
                    "LayerId": "6f335e11-a136-4d8d-8648-5f13e4af1cda"
                }
            ]
        },
        {
            "id": "0dad87c9-8df3-4ee1-8aa3-56383e968210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f58b309-7d59-4a19-99a8-e72638388c5c",
            "compositeImage": {
                "id": "01b8a822-edf7-4551-8183-f46d1071bb03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dad87c9-8df3-4ee1-8aa3-56383e968210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57fd3ef9-359a-41db-97e5-002299cbed87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dad87c9-8df3-4ee1-8aa3-56383e968210",
                    "LayerId": "6f335e11-a136-4d8d-8648-5f13e4af1cda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "6f335e11-a136-4d8d-8648-5f13e4af1cda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f58b309-7d59-4a19-99a8-e72638388c5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 8,
    "yorig": 11
}