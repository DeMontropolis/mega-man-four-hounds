{
    "id": "a00a32ca-803b-402a-b40c-3c1987743422",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMTankOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b3ef819-9ab8-4964-9626-f0c08a9a619d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a00a32ca-803b-402a-b40c-3c1987743422",
            "compositeImage": {
                "id": "036e4b34-651c-48ff-a48c-748268d9834d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b3ef819-9ab8-4964-9626-f0c08a9a619d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d04581-705f-41e6-8542-3b715a700123",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b3ef819-9ab8-4964-9626-f0c08a9a619d",
                    "LayerId": "9b5a49ce-38f3-4496-b7bb-17dd4025e059"
                }
            ]
        },
        {
            "id": "7fdec2af-a64f-46c1-84ac-bebc77e25964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a00a32ca-803b-402a-b40c-3c1987743422",
            "compositeImage": {
                "id": "0c7199f5-e960-4fc8-9a92-7860f936b9ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fdec2af-a64f-46c1-84ac-bebc77e25964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1631470d-f076-41b3-ba69-f9738f139a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fdec2af-a64f-46c1-84ac-bebc77e25964",
                    "LayerId": "9b5a49ce-38f3-4496-b7bb-17dd4025e059"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9b5a49ce-38f3-4496-b7bb-17dd4025e059",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a00a32ca-803b-402a-b40c-3c1987743422",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}