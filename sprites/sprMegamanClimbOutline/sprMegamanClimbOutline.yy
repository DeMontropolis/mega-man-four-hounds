{
    "id": "7f1b5294-beca-486e-b388-8f38a674702d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b95065d8-a264-4b23-a34d-557e5172731e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f1b5294-beca-486e-b388-8f38a674702d",
            "compositeImage": {
                "id": "88372b83-3471-471f-857d-afdca675a6b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b95065d8-a264-4b23-a34d-557e5172731e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daab09c0-4762-4386-850c-15963ab9e5a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b95065d8-a264-4b23-a34d-557e5172731e",
                    "LayerId": "a94f04de-49b8-4502-9b03-d41346134190"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "a94f04de-49b8-4502-9b03-d41346134190",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f1b5294-beca-486e-b388-8f38a674702d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 7
}