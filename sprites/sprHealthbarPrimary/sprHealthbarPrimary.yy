{
    "id": "476b2d83-ce6d-4800-88b0-e6fff5307017",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHealthbarPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "802e79fd-f9fb-4f38-aa4c-a2646e1c05b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "476b2d83-ce6d-4800-88b0-e6fff5307017",
            "compositeImage": {
                "id": "d37f1247-7c66-4839-91f6-fe583164f7c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "802e79fd-f9fb-4f38-aa4c-a2646e1c05b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "442e2476-bcd1-4bfe-9fb9-3b5337e12e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "802e79fd-f9fb-4f38-aa4c-a2646e1c05b7",
                    "LayerId": "7c5e6041-1c2f-401b-b799-7b81b84a1f21"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "7c5e6041-1c2f-401b-b799-7b81b84a1f21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "476b2d83-ce6d-4800-88b0-e6fff5307017",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}