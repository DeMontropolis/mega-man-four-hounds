{
    "id": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBluePokerTelly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c0a5ac4-7346-4c70-a92f-1fb3899b2ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "compositeImage": {
                "id": "e0b5d958-a9d4-4a89-8fdc-1593ff7a68ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0a5ac4-7346-4c70-a92f-1fb3899b2ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda12410-845a-4133-97b7-0148dd5bc1b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0a5ac4-7346-4c70-a92f-1fb3899b2ec2",
                    "LayerId": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5"
                }
            ]
        },
        {
            "id": "01923cfe-6321-4edd-acb9-95cfd17c13c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "compositeImage": {
                "id": "d4453b5f-6125-4e56-9e03-699830fb62ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01923cfe-6321-4edd-acb9-95cfd17c13c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82769890-bafd-41ce-a912-9ff04c47b934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01923cfe-6321-4edd-acb9-95cfd17c13c2",
                    "LayerId": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5"
                }
            ]
        },
        {
            "id": "a4853a57-5cee-4a9b-a072-ab2bbd8e68e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "compositeImage": {
                "id": "ef3f1d88-7e12-441a-98a9-eab4fcac71fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4853a57-5cee-4a9b-a072-ab2bbd8e68e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40100095-9b1e-4ff9-ae26-0b15a3b7610f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4853a57-5cee-4a9b-a072-ab2bbd8e68e4",
                    "LayerId": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5"
                }
            ]
        },
        {
            "id": "9d52cb7d-f19a-407a-85f9-edfcbfd66e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "compositeImage": {
                "id": "6f28c0b1-2c11-4228-af9e-58081fd9e2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d52cb7d-f19a-407a-85f9-edfcbfd66e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "239116c0-64f6-469d-aecd-1423fdff7aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d52cb7d-f19a-407a-85f9-edfcbfd66e7f",
                    "LayerId": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5"
                }
            ]
        },
        {
            "id": "f5200c8b-901c-4280-a8c6-d104e1c6a85d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "compositeImage": {
                "id": "452fb58a-c834-4839-ad29-00ecbbb52648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5200c8b-901c-4280-a8c6-d104e1c6a85d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8379c71b-9f34-47e5-8fc1-f15be8a37397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5200c8b-901c-4280-a8c6-d104e1c6a85d",
                    "LayerId": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5"
                }
            ]
        },
        {
            "id": "e19d5b3d-8cd3-409c-9e9f-baf692028fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "compositeImage": {
                "id": "7f48729d-94a6-4451-9c51-5cf58ca1c615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e19d5b3d-8cd3-409c-9e9f-baf692028fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3391054b-b7bd-404c-8bae-357dbdde7315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e19d5b3d-8cd3-409c-9e9f-baf692028fca",
                    "LayerId": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5"
                }
            ]
        },
        {
            "id": "5a480b48-eda6-46fd-b4d2-fba252518099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "compositeImage": {
                "id": "6876bac6-fa4e-4cf5-9b38-3597cab6d951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a480b48-eda6-46fd-b4d2-fba252518099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aedff8ae-1083-4771-84da-f9ae7373fb07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a480b48-eda6-46fd-b4d2-fba252518099",
                    "LayerId": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5"
                }
            ]
        },
        {
            "id": "7dd3561b-a65d-4cab-a2f1-a1f602c6f6f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "compositeImage": {
                "id": "3f642be2-4845-4af0-a71c-76aa80b671b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd3561b-a65d-4cab-a2f1-a1f602c6f6f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "668fcbcd-37e9-41d6-a0c3-7e2e9f89835c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd3561b-a65d-4cab-a2f1-a1f602c6f6f1",
                    "LayerId": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dacf23a0-7d9d-4601-96f0-7dd0f6ccdbb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcdf6213-0f77-4b1e-b51f-d99795e07ca6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0.06,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}