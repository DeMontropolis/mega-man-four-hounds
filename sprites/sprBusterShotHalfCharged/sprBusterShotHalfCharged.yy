{
    "id": "17e2d7b1-6be1-4163-b17d-1b1d329708b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBusterShotHalfCharged",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddb5282c-0600-4f72-b7ca-6b1e2b223f14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17e2d7b1-6be1-4163-b17d-1b1d329708b0",
            "compositeImage": {
                "id": "b685f33b-4330-4c31-ae66-477f564706e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddb5282c-0600-4f72-b7ca-6b1e2b223f14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b8a8c8-78c3-46f7-8b41-94b74bd4f0e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddb5282c-0600-4f72-b7ca-6b1e2b223f14",
                    "LayerId": "14cd4343-da31-4b02-b4dd-d19e04d5f400"
                }
            ]
        },
        {
            "id": "c658ad25-60e9-4e7b-a964-d811fb9dd151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17e2d7b1-6be1-4163-b17d-1b1d329708b0",
            "compositeImage": {
                "id": "6f5e67c8-fb02-4f3b-baa7-c20e8ba2b08b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c658ad25-60e9-4e7b-a964-d811fb9dd151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e693b5-1d45-43e3-9726-082707ea00e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c658ad25-60e9-4e7b-a964-d811fb9dd151",
                    "LayerId": "14cd4343-da31-4b02-b4dd-d19e04d5f400"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "14cd4343-da31-4b02-b4dd-d19e04d5f400",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17e2d7b1-6be1-4163-b17d-1b1d329708b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.2,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 4,
    "yorig": 4
}