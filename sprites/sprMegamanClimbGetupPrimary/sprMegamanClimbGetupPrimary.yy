{
    "id": "bd654c1d-f082-4159-a88b-131fec2ed96b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanClimbGetupPrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "626203d7-26fc-42e0-ab74-75047858950e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd654c1d-f082-4159-a88b-131fec2ed96b",
            "compositeImage": {
                "id": "a3b80842-6432-4420-9ca8-7897226eae2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "626203d7-26fc-42e0-ab74-75047858950e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9247ec7b-1d6e-46ab-a6e3-64359c140cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "626203d7-26fc-42e0-ab74-75047858950e",
                    "LayerId": "50aa262e-f426-4bd1-91a9-dc3baacaaf19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "50aa262e-f426-4bd1-91a9-dc3baacaaf19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd654c1d-f082-4159-a88b-131fec2ed96b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 5
}