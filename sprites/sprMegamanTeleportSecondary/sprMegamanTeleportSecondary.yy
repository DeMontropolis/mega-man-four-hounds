{
    "id": "b6b1bdf3-c1d7-4d01-b35b-5518492067ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMegamanTeleportSecondary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05dfb912-8759-4f1a-9015-8d3f397c9eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6b1bdf3-c1d7-4d01-b35b-5518492067ea",
            "compositeImage": {
                "id": "928c98b7-ea64-46ea-83a6-c3ec76816685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05dfb912-8759-4f1a-9015-8d3f397c9eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85cad9f7-a788-47a6-ac6b-5ed42c0e42d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05dfb912-8759-4f1a-9015-8d3f397c9eb3",
                    "LayerId": "405262f8-d9ee-4825-bded-a69f3a574a69"
                }
            ]
        },
        {
            "id": "31a09f00-2bd0-44d9-8f21-3c9cefea14fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6b1bdf3-c1d7-4d01-b35b-5518492067ea",
            "compositeImage": {
                "id": "c5e04ba2-3a35-4187-ac91-37437a276970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a09f00-2bd0-44d9-8f21-3c9cefea14fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98df5bc-385d-40e0-a8c1-47421f26a05e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a09f00-2bd0-44d9-8f21-3c9cefea14fb",
                    "LayerId": "405262f8-d9ee-4825-bded-a69f3a574a69"
                }
            ]
        },
        {
            "id": "972110d3-49c8-4d2d-999f-56af7b907fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6b1bdf3-c1d7-4d01-b35b-5518492067ea",
            "compositeImage": {
                "id": "43101c1c-c15c-484d-9b7b-811a59b80056",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "972110d3-49c8-4d2d-999f-56af7b907fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9035b99c-aede-4422-ab92-f2fe1791f875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972110d3-49c8-4d2d-999f-56af7b907fb9",
                    "LayerId": "405262f8-d9ee-4825-bded-a69f3a574a69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "405262f8-d9ee-4825-bded-a69f3a574a69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6b1bdf3-c1d7-4d01-b35b-5518492067ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 15
}