{
    "id": "bfbbcf32-7d0c-4225-89ba-8ada67edc64b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLifePrimary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92704234-c4f6-4222-85cc-281565a6a5c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfbbcf32-7d0c-4225-89ba-8ada67edc64b",
            "compositeImage": {
                "id": "f3187fc2-e320-4908-ad7d-946143a80ce5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92704234-c4f6-4222-85cc-281565a6a5c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e75ced-372d-48ed-8862-cc870b509c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92704234-c4f6-4222-85cc-281565a6a5c6",
                    "LayerId": "a2c10d44-81df-4f56-a293-92813be37580"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "a2c10d44-81df-4f56-a293-92813be37580",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfbbcf32-7d0c-4225-89ba-8ada67edc64b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": -1
}