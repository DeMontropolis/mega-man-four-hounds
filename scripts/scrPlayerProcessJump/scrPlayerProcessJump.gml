// Jumping
if not (tileCollision(x, y + 1)) exit;

if (keyboard_check_pressed(global.jumpKey)) {
	yspeed -= jumpPower;
	state = PlayerState.InAir;
}