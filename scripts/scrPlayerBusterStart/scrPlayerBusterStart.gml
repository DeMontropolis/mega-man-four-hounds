var _attackPressed = argument[0];

// Fire a single shot
if (_attackPressed && (instance_number(objBullet) < 3))
{
	// Charging to true
	shotCharging = true;
	
	// Create basic lemon
	var _bullet = instance_create_layer(x + 2, y + 4, layer_get_id("Effects"), objBullet);
	with (_bullet)
	{
		sprite_index = other.busterSpriteNoCharge;
		spd = other.busterSpeedNoCharge;
		if (sign(other.image_xscale) == 1)
		{
			dir = 0;
		}
		else
		{
			dir = 180;
			image_xscale = -1;
		}
	}
	
	// Switch to shooting state
	if (state == PlayerState.Idle)
	{
		state = PlayerState.ShootingIdle;
	}
	else if (state == PlayerState.Moving)
	{
		state = PlayerState.ShootingMoving;
	}
	else if (state == PlayerState.InAir)
	{
		state = PlayerState.ShootingInAir;	
	}
}

