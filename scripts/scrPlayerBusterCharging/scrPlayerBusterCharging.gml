var _attackReleased = argument[0];

if (shotCharging)
{
	++shotCharge;
	
	// Check released
	if (_attackReleased)
	{
		shotCharging = false;
		var _bullet = noone;
		if (shotCharge >= shotChargeMax)
		{
			_bullet = instance_create_layer(x + 2, y + 4, layer_get_id("Effects"), objBullet);
			_bullet.sprite_index = other.busterSpriteFullCharge;
			_bullet.damage = 3;
			_bullet.spd = other.busterSpeedFullCharge;
		}
		else if (shotCharge >= shotChargeStart)
		{
			_bullet = instance_create_layer(x + 2, y + 4, layer_get_id("Effects"), objBullet);
			_bullet.sprite_index = other.busterSpriteHalfCharge;
			_bullet.damage = 2;
			_bullet.spd = other.busterSpeedHalfCharge;
		}
		else
		{
			// show_debug_message("no shot fired!");	
		}
		
		// Handle bullet
		with (_bullet)
		{
			if (sign(other.image_xscale) == 1)
			{
				dir = 0;
			}
			else
			{
				dir = 180;
				image_xscale = -1;
			}
		}
		
		// Change state
		if (shotCharge >= shotChargeStart) {
		
			// Switch to shooting state
			if (state == PlayerState.Idle)
			{
				state = PlayerState.ShootingIdle;
			}
			else if (state == PlayerState.Moving)
			{
				state = PlayerState.ShootingMoving;
			}
			else if (state == PlayerState.InAir)
			{
				state = PlayerState.ShootingInAir;	
			}
		}
	
		// Reset
		shotCharge = 0;
	}
}
