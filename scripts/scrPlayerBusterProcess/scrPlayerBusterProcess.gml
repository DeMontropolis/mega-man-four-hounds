var _leftDown = argument[0];
var _rightDown = argument[1];

// Timer to reset
if (shootResetTimer++ >= shootResetTimerMax)
{
	if (state = PlayerState.ShootingInAir || state == PlayerState.InAir)
	{
		state = PlayerState.InAir;
	}
	else if (_leftDown || _rightDown)
	{
		state = PlayerState.Moving;
	}
	else
	{
		state = PlayerState.Idle;
	}
	shootResetTimer = 0;
}
else if (state == PlayerState.ShootingInAir || state == PlayerState.InAir)
{
	state = PlayerState.ShootingInAir;	
}
else if (_leftDown || _rightDown)
{
	state = PlayerState.ShootingMoving;
}
else
{
	state = PlayerState.ShootingIdle;
}