var _slideButton = keyboard_check_pressed(global.slideKey);
var _downJump = (keyboard_check(global.downKey) && keyboard_check_pressed(global.jumpKey));
if (_slideButton || _downJump) {
	state = PlayerState.Sliding;
	return true;
}
return false;