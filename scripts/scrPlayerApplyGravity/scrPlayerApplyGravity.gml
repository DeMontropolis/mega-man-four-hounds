// Gravity
var _jumpDown = argument[0];
yspeed = Approach(yspeed, grav, gravAccel);
if (yspeed < 0)
{
	if (not _jumpDown)
	{
		yspeed = 0;
	}
}