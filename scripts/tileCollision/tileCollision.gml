var _x = argument[0];
var _y = argument[1];
var _origX = x;
var _origY = y;
x = _x;
y = _y;
var coll = ((tile_get_index(tilemap_get_at_pixel(global.collisionTiles, bbox_right, bbox_top)) != 0) 
	|| (tile_get_index(tilemap_get_at_pixel(global.collisionTiles, bbox_right, bbox_bottom)) != 0) 
	|| (tile_get_index(tilemap_get_at_pixel(global.collisionTiles, bbox_left, bbox_top)) != 0) 
	|| (tile_get_index(tilemap_get_at_pixel(global.collisionTiles, bbox_left, bbox_bottom)) != 0));
x = _origX;
y = _origY;
return coll;