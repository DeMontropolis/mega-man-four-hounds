var _leftDown = argument[0];
var _rightDown = argument[1];

// Check for landing
if (tileCollision(x, y + 1)) {
	if (state = PlayerState.InAir) {
		if (_leftDown || _rightDown) {
			state = PlayerState.Moving;
		} else {
			state = PlayerState.Idle;
		}
	} else if (state = PlayerState.ShootingInAir) {
		if (_leftDown || _rightDown) {
			state = PlayerState.ShootingMoving;
		} else {
			state = PlayerState.ShootingIdle;
		}
	}
}